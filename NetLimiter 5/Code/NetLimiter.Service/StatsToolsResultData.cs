﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class StatsToolsResultData
{
	[DataMember]
	public ulong DataOut;

	[DataMember]
	public ulong DataIn;

	[DataMember]
	public ulong DataInOH;

	[DataMember]
	public ulong DataOutOH;

	[DataMember]
	public uint ErrFlags;

	[DataMember]
	public uint WinErrCode;

	[DataMember]
	public string ErrMessage;

	[DataMember]
	public ulong TotalRowNum;

	[DataMember]
	public ulong CurrentRowNum;

	[DataMember]
	public bool Finished;
}}
