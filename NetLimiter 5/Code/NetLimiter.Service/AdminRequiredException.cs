﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class AdminRequiredException : NLException
{
	public AdminRequiredException()
		: base(string.Format(Langs.MsgAdminRequired).Replace("\\n", "\n"))
	{
	}

	public AdminRequiredException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
