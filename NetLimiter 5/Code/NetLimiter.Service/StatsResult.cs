﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class StatsResult
{
	[DataMember]
	public StatsGroupByMethod GroupBy;

	[DataMember]
	public List<StatsResultValue> Values = new List<StatsResultValue>();

	public StatsResult(StatsGroupByMethod groupBy = StatsGroupByMethod.Time)
	{
		GroupBy = groupBy;
	}
}}
