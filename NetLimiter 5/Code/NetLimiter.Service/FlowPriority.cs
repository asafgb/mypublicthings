﻿namespace NetLimiter.Service{

public enum FlowPriority
{
	None,
	Low,
	Normal,
	High,
	Critical
}}
