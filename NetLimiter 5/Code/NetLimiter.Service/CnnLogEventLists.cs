﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class CnnLogEventLists
{
	[DataMember]
	public string Name { get; set; }

	[DataMember]
	public DateTime Time { get; set; }

	[DataMember]
	public List<List<CnnLogEvent>> EventLists { get; set; }
}}
