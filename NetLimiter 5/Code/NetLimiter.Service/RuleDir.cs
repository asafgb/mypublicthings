﻿using System;

namespace NetLimiter.Service{

[Serializable]
public enum RuleDir
{
	In,
	Out,
	Both
}}
