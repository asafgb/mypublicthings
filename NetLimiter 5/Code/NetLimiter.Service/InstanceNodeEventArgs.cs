﻿namespace NetLimiter.Service{

public class InstanceNodeEventArgs : NLNodeEventArgs
{
	public new InstanceNode Node
	{
		get
		{
			return base.Node as InstanceNode;
		}
		internal set
		{
			base.Node = value;
		}
	}
}}
