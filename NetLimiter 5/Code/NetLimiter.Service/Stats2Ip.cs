﻿using System.Runtime.Serialization;
using CoreLib.Net;

namespace NetLimiter.Service{

[DataContract]
public class Stats2Ip
{
	[DataMember]
	public uint IpV4;

	[DataMember]
	public IPAddress6 IpV6;

	[DataMember]
	public string Domain;
}}
