﻿using System;

namespace NetLimiter.Service{

public class FailedToConnectException : Exception
{
	public FailedToConnectException(string hostName, Exception inner)
		: base($"Failed to connect to {hostName}", inner)
	{
	}
}}
