﻿using System.Collections.Generic;

namespace NetLimiter.Service{

public class StatsResultsManager
{
	private Dictionary<string, LinkedList<StatsResultList>> _results = new Dictionary<string, LinkedList<StatsResultList>>();

	public void Put(string name, StatsResultList r)
	{
		lock (_results)
		{
			if (_results.ContainsKey(name))
			{
				_results[name].AddLast(r);
				return;
			}
			LinkedList<StatsResultList> linkedList = new LinkedList<StatsResultList>();
			linkedList.AddLast(r);
			_results.Add(name, linkedList);
		}
	}

	public StatsResultList Get(string name, bool remove = true)
	{
		lock (_results)
		{
			if (!_results.ContainsKey(name) || _results[name].Count == 0)
			{
				return null;
			}
			StatsResultList value = _results[name].First.Value;
			if (remove)
			{
				_results[name].RemoveFirst();
			}
			return value;
		}
	}

	private void Clear(string name = null)
	{
		lock (_results)
		{
			if (name == null)
			{
				_results.Clear();
			}
			else if (_results.ContainsKey(name))
			{
				_results[name].Clear();
			}
		}
	}
}}
