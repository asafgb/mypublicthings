﻿using System;

namespace NetLimiter.Service{

[Serializable]
public enum FilterPerType
{
	None,
	PerConnection,
	PerProcess,
	PerApp
}}
