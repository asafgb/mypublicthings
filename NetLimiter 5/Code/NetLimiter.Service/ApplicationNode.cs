﻿using System;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Logging;
using NLInterop;

namespace NetLimiter.Service{

public class ApplicationNode : NLNode
{
	private static readonly ILogger<ApplicationNode> _logger = Logging.LoggerFactory.CreateLogger<ApplicationNode>();

	public AppId AppId { get; protected set; }

	public string FilePath => AppId.Path;

	public bool IsForward { get; protected set; }

	public new TopNode Parent
	{
		get
		{
			return base.Parent as TopNode;
		}
		private set
		{
			base.Parent = value;
		}
	}

	public event EventHandler<ApplicationNodeEventArgs> Updated;

	public event EventHandler<ApplicationNodeEventArgs> Removed;

	internal ApplicationNode(NodeLoader ldr, AppNodeDataWrapper data)
		: base(data)
	{
		Parent = ldr.TopNode;
		try
		{
			string text = Marshal.PtrToStringUni(data.GetPath());
			Sid sid = Sid.FromBytes(data.GetSid());
			AppId = new AppId(text, sid);
			IsForward = text == "netlimiter\\forward";
		}
		catch (Exception exception)
		{
			AppId = new AppId("Error", null);
			_logger.LogError(exception, "Failed to create app node");
		}
		Update(ldr, data);
	}

	internal override void Update(NodeLoader ldr, NodeDataWrapper data)
	{
		Update(ldr, data, ldr.Applications.Nodes._args);
		ldr.Applications.Nodes.OnNodeUpdated();
		if (this.Updated != null)
		{
			this.Updated(this, ldr.Applications.Nodes._args);
		}
	}

	internal override void OnRemoved(NodeLoader ldr)
	{
		if (this.Removed != null)
		{
			this.Removed(this, ldr.Applications.Nodes._args);
		}
	}
}}
