﻿using System;

namespace NetLimiter.Service{

public class FilterUpdateEventArgs : EventArgs
{
	public Filter Filter { get; protected set; }

	public Filter OldFilter { get; protected set; }

	public FilterUpdateInfo Info { get; protected set; }

	public FilterUpdateEventArgs(FilterUpdateInfo info, Filter oldFilter)
	{
		Filter = info.Filter;
		OldFilter = oldFilter;
		Info = info;
	}
}}
