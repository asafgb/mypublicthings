﻿namespace NetLimiter.Service{

public class StatsQueryInProgress
{
	public string Name { get; set; }

	public bool StopRequest { get; set; }

	public StatsQueryInProgress(string name)
	{
		Name = name;
		StopRequest = false;
	}
}}
