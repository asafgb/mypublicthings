﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Service.AppList;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class SvcPackageFilter : PackageFilter
{
	[DataMember]
	public string SvcName { get; set; }

	[DataMember]
	public string DisplayName { get; set; }

	[DataMember]
	public string Description { get; set; }

	internal SvcPackageFilter(Sid sid)
		: base(sid)
	{
		if (!sid.IsServiceSid)
		{
			throw new ArgumentException("Invalid service sid", "sid");
		}
		UpdateInfo();
	}

	internal void UpdateInfo()
	{
		AppListSvcItem appListSvcItem = null;
		try
		{
			appListSvcItem = AppListCreator.GetSvcItem(base.Sid);
		}
		catch
		{
		}
		if (appListSvcItem != null)
		{
			try
			{
				SvcName = appListSvcItem.Name;
				base.Name = appListSvcItem.Name;
				DisplayName = appListSvcItem.DisplayName;
				Description = appListSvcItem.Description;
			}
			catch (Exception)
			{
			}
		}
		if (string.IsNullOrEmpty(base.Name))
		{
			base.Name = base.Sid.ToString();
		}
	}
}}
