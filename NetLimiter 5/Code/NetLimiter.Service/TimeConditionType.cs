﻿namespace NetLimiter.Service{

public enum TimeConditionType
{
	EveryDay,
	Days,
	Once,
	DayOfMonth
}}
