﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class WiFiNetwork : Network
{
	[DataMember]
	public bool IsAdHoc { get; set; }

	public WiFiNetwork(string systemId, ushort index, string ifcName, string cnnName, string mac, string routerMac, string wifiBssid, string wifiSsid, bool isAdHoc)
		: base(71u, systemId, index, ifcName, cnnName, mac, routerMac, wifiBssid, wifiSsid)
	{
		IsAdHoc = IsAdHoc;
	}
}}
