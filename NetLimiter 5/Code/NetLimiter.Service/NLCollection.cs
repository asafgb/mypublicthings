﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class NLCollection<T> : ICollection<T>, IEnumerable<T>, IEnumerable
{
	[DataMember(Name = "Items")]
	protected List<T> _items = new List<T>();

	public int Count => _items.Count;

	public bool IsReadOnly => false;

	public void Add(T item)
	{
		_items.Add(item);
	}

	public void Clear()
	{
		_items.Clear();
	}

	public bool Contains(T item)
	{
		return _items.Contains(item);
	}

	public void CopyTo(T[] array, int arrayIndex)
	{
		_items.CopyTo(array, arrayIndex);
	}

	public bool Remove(T item)
	{
		return _items.Remove(item);
	}

	public IEnumerator<T> GetEnumerator()
	{
		return _items.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return _items.GetEnumerator();
	}
}}
