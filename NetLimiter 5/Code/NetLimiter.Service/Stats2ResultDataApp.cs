﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2ResultDataApp
{
	[DataMember]
	public ulong In;

	[DataMember]
	public ulong Out;

	[DataMember]
	public string Path;

	[DataMember]
	public string Sid;

	[DataMember]
	public ushort InternalId;
}}
