﻿namespace NetLimiter.Service{

public class ApplicationNodeEventArgs : NLNodeEventArgs
{
	public new ApplicationNode Node
	{
		get
		{
			return base.Node as ApplicationNode;
		}
		internal set
		{
			base.Node = value;
		}
	}
}}
