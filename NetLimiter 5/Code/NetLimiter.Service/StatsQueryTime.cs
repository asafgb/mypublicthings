﻿using System;
using System.Collections.Generic;

namespace NetLimiter.Service{

public class StatsQueryTime
{
	public StatsTimeStep Step = StatsTimeStep.Day;

	public StatsTimeType Type;

	public DateTime Start = new DateTime(1970, 1, 1);

	public DateTime End = DateTime.Now;

	public List<StatsTimeInterval> TimeInvervals;
}}
