﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
[KnownType(typeof(SvcAppInfo))]
[KnownType(typeof(StoreAppInfo))]
public class AppInfo
{
	[DataMember]
	public AppId AppId { get; protected set; }

	[DataMember]
	public DateTime SysCreated { get; set; }

	[DataMember]
	public string CompanyName { get; set; }

	[DataMember]
	public string ProductName { get; set; }

	[DataMember]
	public string ProductVersion { get; set; }

	[DataMember]
	public string FileDescription { get; set; }

	[DataMember]
	public string FileVersion { get; set; }

	[DataMember]
	public List<string> Tags { get; set; }

	internal bool IsDead
	{
		get
		{
			try
			{
				return GetIsDead(AppId);
			}
			catch
			{
				return false;
			}
		}
	}

	public AppInfo(AppId appId)
	{
		AppId = appId ?? throw new ArgumentNullException("appId");
	}

	public virtual void UpdateInfo(AppInfoCtx ctx = null)
	{
		try
		{
			FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(AppId.Path);
			ProductName = FixAppString(versionInfo.ProductName);
			FileDescription = FixAppString(versionInfo.FileDescription);
			FileVersion = FixAppString(versionInfo.FileVersion);
			ProductVersion = FixAppString(versionInfo.ProductVersion);
			CompanyName = FixAppString(versionInfo.CompanyName);
		}
		catch
		{
		}
		try
		{
			DateTime creationTimeUtc = File.GetCreationTimeUtc(AppId.Path);
			DateTime lastWriteTimeUtc = File.GetLastWriteTimeUtc(AppId.Path);
			SysCreated = ((creationTimeUtc < lastWriteTimeUtc) ? lastWriteTimeUtc : creationTimeUtc);
		}
		catch
		{
		}
	}

	public bool CheckFileCreationTime()
	{
		try
		{
			return SysCreated != File.GetCreationTime(AppId.Path);
		}
		catch
		{
			return false;
		}
	}

	internal static string FixAppString(string str)
	{
		if (str != null)
		{
			return str.Trim().MakeXmlValid();
		}
		return "";
	}

	public static AppInfo Create(AppId appId, AppInfoCtx ctx = null)
	{
		Sid sid = appId.Sid;
		AppInfo appInfo;
		if ((object)sid != null && sid.IsServiceSid)
		{
			appInfo = new SvcAppInfo(appId);
		}
		else
		{
			Sid sid2 = appId.Sid;
			appInfo = (((object)sid2 == null || !sid2.IsStoreAppSid) ? new AppInfo(appId) : new StoreAppInfo(appId));
		}
		appInfo.UpdateInfo(ctx);
		return appInfo;
	}

	internal static bool GetIsDead(AppId appId)
	{
		if (appId.IsAppEmpty)
		{
			return true;
		}
		if (string.Equals(appId.Path, "system", StringComparison.InvariantCultureIgnoreCase))
		{
			return false;
		}
		if (!File.Exists(appId.Path))
		{
			return true;
		}
		if (!appId.IsSidEmpty && !PathHelper.ContainsAny(appId.Path, "\\windowsapps\\", "\\systemapps\\", "\\system32\\backgroundtaskhost.exe", "\\system32\\backgroundtransferhost.exe", "\\system32\\svchost.exe"))
		{
			return true;
		}
		return false;
	}
}}
