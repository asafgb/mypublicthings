﻿namespace NetLimiter.Service{

public enum RuleState
{
	Disabled,
	Inactive,
	Active
}}
