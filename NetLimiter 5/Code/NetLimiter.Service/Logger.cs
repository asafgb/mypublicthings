﻿using Microsoft.Extensions.Logging;

namespace NetLimiter.Service{

internal class Logger : ILogger
{
	private Microsoft.Extensions.Logging.ILogger _logger;

	public static Logger Create(string name)
	{
		return new Logger
		{
			_logger = Logging.LoggerFactory.CreateLogger(name)
		};
	}

	public void Trace(string msg)
	{
		_logger.LogTrace(msg);
	}

	public void Debug(string msg)
	{
		_logger.LogDebug(msg);
	}

	public void Info(string msg)
	{
		_logger.LogInformation(msg);
	}

	public void Warn(string msg)
	{
		_logger.LogWarning(msg);
	}

	public void Error(string msg)
	{
		_logger.LogError(msg);
	}

	public void Fatal(string msg)
	{
		_logger.LogCritical(msg);
	}
}}
