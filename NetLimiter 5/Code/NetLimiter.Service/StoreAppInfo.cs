﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Service.AppList;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class StoreAppInfo : AppInfo
{
	[DataMember]
	public string PackageId { get; set; }

	[DataMember]
	public string PackageName { get; set; }

	[DataMember]
	public string PackageDisplayName { get; set; }

	[DataMember]
	public string PackageFolder { get; set; }

	[DataMember]
	public string IconPath { get; set; }

	[DataMember]
	public string PackagePublisher { get; set; }

	[DataMember]
	public string PackageVersion { get; set; }

	public StoreAppInfo(AppId appId)
		: base(appId)
	{
	}

	public override void UpdateInfo(AppInfoCtx ctx = null)
	{
		ctx = ctx ?? new AppInfoCtx();
		base.UpdateInfo(ctx);
		if (ctx.Manifest == null)
		{
			try
			{
				ctx.Manifest = new StorePackageManifest(base.AppId);
			}
			catch
			{
				return;
			}
		}
		PackageId = AppInfo.FixAppString(ctx.Manifest.PackageId);
		PackageName = AppInfo.FixAppString(ctx.Manifest.Name);
		PackageFolder = AppInfo.FixAppString(ctx.Manifest.Folder);
		PackageDisplayName = AppInfo.FixAppString(ctx.Manifest.DisplayName);
		PackageVersion = AppInfo.FixAppString(ctx.Manifest.Version);
		PackagePublisher = AppInfo.FixAppString(ctx.Manifest.Publisher);
		IconPath = AppInfo.FixAppString(ctx.Manifest.AppIcon44);
	}

	public string TranslateIndirectStr(string str, string def)
	{
		return IndirectString.Translate(PackageId, PackageName, str, def);
	}
}}
