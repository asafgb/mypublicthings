﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class NtwInUseException : NLException
{
	public NtwInUseException(Filter flt)
		: base(string.Format(Langs.MsgNtwInFilterUse, flt.Name))
	{
	}

	public NtwInUseException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
