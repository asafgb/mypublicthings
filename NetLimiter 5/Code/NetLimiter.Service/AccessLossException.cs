﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;
using NetLimiter.Service.Security;

namespace NetLimiter.Service{

[Serializable]
public class AccessLossException : NLException
{
	internal AccessLossException(Rights right, string userName)
		: base(string.Format(Langs.MsgAccessLoss, right.ToString(), userName))
	{
	}

	public AccessLossException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
