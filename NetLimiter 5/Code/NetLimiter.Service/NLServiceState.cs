﻿namespace NetLimiter.Service{

public enum NLServiceState
{
	Initial,
	Connecting,
	Connected,
	Disconnecting,
	Disconnected
}}
