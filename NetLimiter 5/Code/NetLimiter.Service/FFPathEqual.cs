﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFPathEqual : FilterFunctionT<AppPathFilterValue>
{
	public FFPathEqual()
	{
	}

	public FFPathEqual(string path)
	{
		base.Values.Add(path);
	}
}}
