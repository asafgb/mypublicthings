﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceProcess;
using NLInterop;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class SvcAppInfo : AppInfo
{
	[DataMember]
	public string ServiceName { get; set; }

	[DataMember]
	public string ServiceDesc { get; set; }

	public SvcAppInfo(AppId appId)
		: base(appId)
	{
	}

	public override void UpdateInfo(AppInfoCtx ctx = null)
	{
		ctx = ctx ?? new AppInfoCtx();
		base.UpdateInfo(ctx);
		try
		{
			string svcName = AppIdHelper.GetSvcNameFromSid(base.AppId.Sid.Bytes);
			if (svcName != null)
			{
				ServiceName = AppInfo.FixAppString(svcName);
				ServiceController serviceController = ctx.Services?.FirstOrDefault((ServiceController x) => string.Compare(x.ServiceName, svcName, ignoreCase: true) == 0);
				if (serviceController != null)
				{
					ServiceDesc = AppInfo.FixAppString(serviceController.DisplayName);
				}
			}
		}
		catch
		{
		}
	}
}}
