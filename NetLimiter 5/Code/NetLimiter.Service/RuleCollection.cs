﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[CollectionDataContract(Name = "rules", ItemName = "rule")]
public class RuleCollection : List<Rule>
{
}}
