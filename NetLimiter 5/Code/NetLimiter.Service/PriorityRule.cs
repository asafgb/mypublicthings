﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class PriorityRule : Rule
{
	[DataMember]
	public FlowPriority Priority { get; set; }

	public PriorityRule()
		: base(RuleDir.Both)
	{
	}

	public PriorityRule(FlowPriority priority)
		: base(RuleDir.Both)
	{
		Priority = priority;
	}
}}
