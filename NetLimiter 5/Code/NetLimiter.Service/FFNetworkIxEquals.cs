﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFNetworkIxEquals : FilterFunctionT<ushort>
{
	public FFNetworkIxEquals()
	{
	}

	public FFNetworkIxEquals(ushort id)
	{
		base.Values.Add(id);
	}
}}
