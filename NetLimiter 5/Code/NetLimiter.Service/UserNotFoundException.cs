﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class UserNotFoundException : NLException
{
	internal UserNotFoundException(string userName, Exception inner = null)
		: base(string.Format(Langs.MsgUserNotFound, userName), inner)
	{
	}

	public UserNotFoundException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
