﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class ServiceVersionTooLow : NLException
{
	private string Version { get; }

	internal ServiceVersionTooLow(string version)
		: base(string.Format(Langs.MsgServiceVersionTooLow, version.ToString()))
	{
		Version = version;
	}

	public ServiceVersionTooLow(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
