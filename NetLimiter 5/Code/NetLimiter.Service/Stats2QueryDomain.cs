﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2QueryDomain
{
	[DataMember]
	public string Domain;

	[DataMember]
	public bool IsMatch = true;

	[DataMember]
	public bool IsFragment;
}}
