﻿using System;

namespace NetLimiter.Service{

public class OptionsChangeEventArgs : EventArgs
{
	public OptionsChangeType ChangeType { get; protected set; }

	public object Value { get; protected set; }

	public OptionsChangeEventArgs(OptionsChangeType changeType, object value)
	{
		ChangeType = changeType;
		Value = value;
	}
}}
