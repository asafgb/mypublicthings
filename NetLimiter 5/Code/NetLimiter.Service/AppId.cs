﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class AppId : IEquatable<AppId>
{
	public const string NullPath = "null-path";

	[DataMember]
	public string Path { get; protected set; }

	[DataMember]
	public Sid Sid { get; protected set; }

	public bool IsWild
	{
		get
		{
			if (!IsAppEmpty)
			{
				return Path.IndexOf('*') > -1;
			}
			return false;
		}
	}

	public bool IsAppEmpty => Path == "null-path";

	public bool IsSidEmpty => (object)Sid == null;

	public bool IsEmpty
	{
		get
		{
			if (IsAppEmpty)
			{
				return IsSidEmpty;
			}
			return false;
		}
	}

	public bool IsStorePackage
	{
		get
		{
			if (IsAppEmpty && Sid != null)
			{
				return Sid.IsStoreAppSid;
			}
			return false;
		}
	}

	public bool IsSvcPackage
	{
		get
		{
			if (IsAppEmpty && Sid != null)
			{
				return Sid.IsServiceSid;
			}
			return false;
		}
	}

	public AppId(string path, Sid sid)
	{
		Path = (string.IsNullOrEmpty(path) ? "null-path" : path);
		Sid = sid;
	}

	public AppId(string path)
	{
		Path = (string.IsNullOrEmpty(path) ? "null-path" : path);
		Sid = null;
	}

	public bool Equals(AppId other)
	{
		if (other == null)
		{
			return false;
		}
		if (string.Compare(Path, other.Path, ignoreCase: true) == 0)
		{
			return Sid.Equals(Sid, other.Sid);
		}
		return false;
	}

	public bool EqualsWild(AppId other)
	{
		if (other == null)
		{
			return false;
		}
		if (PathHelper.IsWildEqual(Path, other.Path))
		{
			return Sid.Equals(Sid, other.Sid);
		}
		return false;
	}

	public override string ToString()
	{
		if ((object)Sid != null)
		{
			return Path + " (" + Sid.ToString() + ")";
		}
		return Path ?? "";
	}

	public override bool Equals(object obj)
	{
		return base.Equals(obj);
	}

	public override int GetHashCode()
	{
		return ToString().GetHashCode();
	}
}}
