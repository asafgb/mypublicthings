﻿using System;
using System.Net;
using System.Runtime.Serialization;
using NLInfoTools;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class CnnLogEvent
{
	[DataMember]
	public IPAddress rAddr;

	[DataMember]
	public IPAddress lAddr;

	[DataMember]
	public ushort rPort;

	[DataMember]
	public ushort lPort;

	[DataMember]
	public DateTime startTime;

	[DataMember]
	public DateTime endTime;

	[DataMember]
	public AppId appId;

	[DataMember]
	public uint ruleId;

	[DataMember]
	public uint filterId;

	[DataMember]
	public FwAction fwAction;

	[DataMember]
	public CnnEventType type;

	[DataMember]
	public Location remLocation;
}}
