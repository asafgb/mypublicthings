﻿namespace NetLimiter.Service{

public enum ProductFeatures
{
	Blocker,
	Stats,
	Quotas,
	Filters,
	RemoteAdmin,
	Scheduler,
	UserPerms
}}
