﻿using System;
using System.Runtime.Serialization;
using CoreLib.Net;

namespace NetLimiter.Service{

[Serializable]
[DataContract(Name = "IPRangeFilterValue")]
public class IPRangeFilterValue
{
	private IPRange _range;

	[DataMember(Name = "range")]
	private string _rangeStr
	{
		get
		{
			return _range.Start.ToString() + "-" + _range.End.ToString();
		}
		set
		{
			string[] array = value.Split('-');
			if (array.Length != 2)
			{
				throw new ArgumentException();
			}
			IPAddress start = IPAddress.Parse(array[0]);
			IPAddress end = IPAddress.Parse(array[1]);
			_range = new IPRange(start, end);
		}
	}

	public IPRange Range => _range;

	public IPRangeFilterValue(IPRange range)
	{
		_range = range;
	}

	public IPRangeFilterValue(IPAddress addr)
	{
		_range = new IPRange(addr);
	}

	public IPRangeFilterValue(IPAddress start, IPAddress end)
	{
		_range = new IPRange(start, end);
	}

	public IPRangeFilterValue(string start, string end)
	{
		_range = new IPRange(start, end);
	}

	public IPRangeFilterValue(IPAddress addr, int prefixLen)
	{
		_range = new IPRange(addr, prefixLen);
	}

	public IPRangeFilterValue(string addr, int prefixLen)
	{
		_range = new IPRange(addr, prefixLen);
	}
}}
