﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract(Name = "fwRule")]
public class FwRule : Rule
{
	[DataMember]
	public new RuleDir Dir
	{
		get
		{
			return base.Dir;
		}
		set
		{
			base.Dir = value;
		}
	}

	[DataMember(Name = "action")]
	public FwAction Action { get; set; }

	public FwRule(RuleDir dir, FwAction action)
		: base(dir)
	{
		Init(action);
	}

	private void Init(FwAction action)
	{
		Action = action;
	}
}}
