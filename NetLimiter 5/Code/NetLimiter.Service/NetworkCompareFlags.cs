﻿using System;

namespace NetLimiter.Service{

[Flags]
public enum NetworkCompareFlags
{
	Bssid = 1,
	Ssid = 2,
	Mac = 4,
	RouterMac = 8,
	IfcName = 0x10
}}
