﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFDomainNameEqual : FilterFunctionT<DomainNameFilterValue>
{
	public FFDomainNameEqual()
	{
	}

	public FFDomainNameEqual(string domainName)
	{
		base.Values.Add(domainName);
	}
}}
