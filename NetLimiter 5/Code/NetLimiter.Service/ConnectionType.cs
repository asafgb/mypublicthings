﻿using System;

namespace NetLimiter.Service{

[Serializable]
public enum ConnectionType
{
	None,
	Listen,
	Accept,
	Connect
}}
