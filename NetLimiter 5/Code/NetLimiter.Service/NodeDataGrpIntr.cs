﻿using System;

namespace NetLimiter.Service{

[Serializable]
internal class NodeDataGrpIntr
{
	private ulong _parentId;

	private ulong _nodeId;

	public uint StartPos;

	public uint MaxRetNodeCnt = uint.MaxValue;

	public ulong CurIdPrev;

	public ulong CurId;

	public bool Load;

	public uint RetNodeCnt;

	public uint SumRetNodeCnt;

	public uint TotalNodeCnt;

	public bool IsComplete;

	public ulong ParentId
	{
		get
		{
			return _parentId;
		}
		set
		{
			NodeId = 0uL;
			_parentId = value;
		}
	}

	public ulong NodeId
	{
		get
		{
			return _nodeId;
		}
		set
		{
			if (value == 0L)
			{
				SelectAll();
				return;
			}
			Load = true;
			_nodeId = value;
			_parentId = 0uL;
			StartPos = 0u;
			MaxRetNodeCnt = 1u;
		}
	}

	public void SelectAll(bool select = true)
	{
		StartPos = 0u;
		MaxRetNodeCnt = uint.MaxValue;
		_parentId = 0uL;
		_nodeId = 0uL;
		Load = select;
	}

	public void Reset()
	{
		CurId = 0uL;
		SumRetNodeCnt = 0u;
	}

	public void PageMoveNext()
	{
		StartPos += MaxRetNodeCnt;
	}

	public void PageMovePrev()
	{
		if (StartPos >= MaxRetNodeCnt)
		{
			StartPos -= MaxRetNodeCnt;
		}
		else
		{
			StartPos = 0u;
		}
	}
}}
