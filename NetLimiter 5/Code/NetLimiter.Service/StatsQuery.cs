﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class StatsQuery
{
	[DataMember]
	public string Name = "DefaultQueryName";

	[DataMember]
	public StatsQueryTime Time = new StatsQueryTime();

	[DataMember]
	public List<List<Filter>> Filters = new List<List<Filter>>();

	[DataMember]
	public List<StatsQueryResultType> ResultTypes = new List<StatsQueryResultType>();

	[DataMember]
	public ushort FirstDayOfMonth = 1;

	[DataMember]
	public Stats2Query Query2;
}}
