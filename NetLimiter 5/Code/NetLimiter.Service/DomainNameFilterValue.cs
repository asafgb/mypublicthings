﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class DomainNameFilterValue
{
	[DataMember]
	public string DomainName { get; protected set; }

	public DomainNameFilterValue(string domainName)
	{
		if (domainName.Length >= 260)
		{
			throw new ArgumentException($"Path length greater then MAX_PATH");
		}
		DomainName = domainName;
	}

	public static implicit operator DomainNameFilterValue(string domainName)
	{
		return new DomainNameFilterValue(domainName);
	}

	public static implicit operator string(DomainNameFilterValue domainName)
	{
		return domainName.DomainName;
	}
}}
