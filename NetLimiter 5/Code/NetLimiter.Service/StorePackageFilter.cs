﻿using System;
using System.IO;
using System.Runtime.Serialization;
using NetLimiter.Service.AppList;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class StorePackageFilter : PackageFilter
{
	[DataMember]
	public string PackageId { get; set; }

	[DataMember]
	public string DisplayName { get; set; }

	[DataMember]
	public string PackageFolder { get; set; }

	[DataMember]
	public string IconPath { get; set; }

	[DataMember]
	public string Publisher { get; set; }

	[DataMember]
	public string Version { get; set; }

	internal StorePackageFilter(AppId appId, AppInfoCtx ctx = null)
		: base(appId.Sid)
	{
		if (appId == null)
		{
			throw new ArgumentNullException("appId");
		}
		if (appId.IsSidEmpty)
		{
			throw new ArgumentException("Sid must be valid");
		}
		if (!appId.Sid.IsStoreAppSid)
		{
			throw new ArgumentException("Invalid store app sid", "appId");
		}
		PackageFolder = StorePackageManifest.GetPackgeFolder(appId);
		UpdateInfo(ctx ?? new AppInfoCtx());
	}

	internal void UpdateInfo(AppInfoCtx ctx = null)
	{
		StorePackageManifest storePackageManifest = ctx?.Manifest;
		if (storePackageManifest == null)
		{
			try
			{
				storePackageManifest = new StorePackageManifest(new AppId(null, base.Sid));
			}
			catch
			{
				storePackageManifest = GetManifestFromPath(ctx);
			}
			if (ctx != null)
			{
				ctx.Manifest = storePackageManifest;
			}
		}
		base.Name = storePackageManifest.Name;
		PackageId = storePackageManifest.PackageId;
		IconPath = storePackageManifest.FirstAppIcon44;
		DisplayName = storePackageManifest.DisplayName;
		Publisher = storePackageManifest.Publisher;
		Version = storePackageManifest.Version;
		PackageFolder = storePackageManifest.Folder;
	}

	private StorePackageManifest GetManifestFromPath(AppInfoCtx ctx = null)
	{
		string text = ((ctx == null || ctx.AppId?.IsAppEmpty != false) ? PackageFolder : Path.GetDirectoryName(ctx.AppId.Path));
		if (Directory.Exists(text))
		{
			return new StorePackageManifest(text);
		}
		throw new ManifestNotFoundException(text);
	}

	public string TranslateIndirectStr(string str, string def)
	{
		return IndirectString.Translate(PackageId, base.Name, str, def);
	}
}}
