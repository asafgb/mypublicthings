﻿using System;

namespace NetLimiter.Service{

public class AppIdEventArgs : EventArgs
{
	public AppId AppId { get; internal set; }

	public AppIdEventArgs(AppId appId)
	{
		AppId = appId;
	}
}}
