﻿using NLInfoTools;

namespace NetLimiter.Service{

public class VTReportEventArgs
{
	public VTFileReport Report { get; set; }

	public string FileName { get; set; }
}}
