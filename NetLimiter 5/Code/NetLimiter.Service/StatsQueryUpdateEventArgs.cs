﻿namespace NetLimiter.Service{

public class StatsQueryUpdateEventArgs
{
	public string QueryName { get; set; }

	public StatsResultList Results { get; set; }

	public bool Finished { get; set; }

	public StatsQueryFinishReason FinishReason { get; set; }

	public Stats2Error Error { get; set; }

	public Stats2Results Results2 { get; set; }
}}
