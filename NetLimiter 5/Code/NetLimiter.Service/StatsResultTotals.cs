﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class StatsResultTotals
{
	[DataMember]
	public ulong CnnNum;

	[DataMember]
	public ulong CnnTime;

	[DataMember]
	public ulong DataIn;

	[DataMember]
	public ulong DataOut;

	[DataMember]
	public ulong DataInOH;

	[DataMember]
	public ulong DataOutOH;
}}
