﻿namespace NetLimiter.Service{

public class ConnectionNodeEventArgs : NLNodeEventArgs
{
	public new ConnectionNode Node
	{
		get
		{
			return base.Node as ConnectionNode;
		}
		internal set
		{
			base.Node = value;
		}
	}
}}
