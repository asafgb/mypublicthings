﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class TimeCondition : RuleCondition
{
	[DataMember]
	public DateTime Time;

	[DataMember]
	public bool IsEnabled { get; set; }

	[DataMember]
	public TimeConditionType TimeConditionType { get; set; }

	[DataMember]
	public List<DayOfWeek> Days { get; protected set; }

	[DataMember]
	public int DayOfMonth { get; set; }

	public TimeCondition()
	{
		Time = DateTime.UtcNow;
		IsEnabled = true;
		Init();
		DayOfMonth = 1;
	}

	protected void Init()
	{
		if (Days == null)
		{
			Days = new List<DayOfWeek>();
		}
	}

	[OnDeserialized]
	private void OnDeserialized(StreamingContext streamContext)
	{
		Init();
	}

	public DateTime GetExecutionTime(out DateTime prev, out DateTime next, DateTime? tmNow)
	{
		DateTime dateTime = (tmNow.HasValue ? tmNow.Value : DateTime.UtcNow);
		prev = DateTime.MinValue;
		next = DateTime.MaxValue;
		switch (TimeConditionType)
		{
		case TimeConditionType.EveryDay:
		{
			DateTime dateTime4 = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, Time.Hour, Time.Minute, Time.Second);
			if (dateTime4 > dateTime)
			{
				prev = dateTime4.AddDays(-1.0);
				next = dateTime4;
			}
			else
			{
				prev = dateTime4;
				next = dateTime4.AddDays(1.0);
			}
			break;
		}
		case TimeConditionType.Days:
		{
			DateTime dateTime2 = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, Time.Hour, Time.Minute, Time.Second);
			DateTime dateTime3 = dateTime2;
			for (int i = 0; i < 7; i++)
			{
				if (!(prev == DateTime.MinValue) && !(next == DateTime.MaxValue))
				{
					break;
				}
				DayOfWeek item = (DayOfWeek)((int)(dateTime.DayOfWeek - i + 7) % 7);
				DayOfWeek item2 = (DayOfWeek)((int)(dateTime.DayOfWeek + i) % 7);
				if (prev == DateTime.MinValue && dateTime2 <= dateTime && Days.Contains(item))
				{
					prev = dateTime2;
				}
				else
				{
					dateTime2 = dateTime2.AddDays(-1.0);
				}
				if (next == DateTime.MaxValue && dateTime3 > dateTime && Days.Contains(item2))
				{
					next = dateTime3;
				}
				else
				{
					dateTime3 = dateTime3.AddDays(1.0);
				}
			}
			break;
		}
		case TimeConditionType.DayOfMonth:
		{
			int day = Math.Min(DateTime.DaysInMonth(dateTime.Year, dateTime.Month), DayOfMonth);
			DateTime dateTime4 = new DateTime(dateTime.Year, dateTime.Month, day, Time.Hour, Time.Minute, Time.Second);
			if (dateTime4 <= dateTime)
			{
				prev = dateTime4;
				next = dateTime4.AddMonths(1);
			}
			else
			{
				next = dateTime4;
				prev = dateTime4.AddMonths(-1);
			}
			break;
		}
		case TimeConditionType.Once:
			if (Time > dateTime)
			{
				next = Time;
			}
			else
			{
				prev = Time;
			}
			break;
		}
		return dateTime;
	}
}}
