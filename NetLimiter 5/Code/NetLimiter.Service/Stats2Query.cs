﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2Query
{
	[DataMember]
	public string Name = "DefaultQueryName";

	[DataMember]
	public StatsQueryTime Time = new StatsQueryTime();

	[DataMember]
	public Filter Filter;

	[DataMember]
	public List<StatsQueryResultType> ResultTypes = new List<StatsQueryResultType>();

	[DataMember]
	public ushort FirstDayOfMonth = 1;

	[DataMember]
	public ushort ResponseTime;
}}
