﻿using System.Runtime.Serialization;
using CoreLib.Net;

namespace NetLimiter.Service{

[DataContract]
public class Stats2IpV6
{
	[DataMember]
	public IPAddress6 Ip;

	[DataMember]
	public string Domain;
}}
