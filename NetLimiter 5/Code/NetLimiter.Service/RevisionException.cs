﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class RevisionException : NLException
{
	public RevisionException(Filter flt)
		: base(string.Format(Langs.MsgFltRevision, flt.Name, flt.Id))
	{
	}

	public RevisionException(Rule rule)
		: base(string.Format(Langs.MsgRuleRevision, rule.Id))
	{
	}

	public RevisionException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
