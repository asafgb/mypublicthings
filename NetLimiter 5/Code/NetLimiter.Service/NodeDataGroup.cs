﻿using System;
using System.Collections.Generic;

namespace NetLimiter.Service{

public class NodeDataGroup<TNode, TEventArgs> where TNode : NLNode where TEventArgs : NLNodeEventArgs, new()
{
	private Func<NodeLoader, NodeDataGrpIntr> _grpSelector;

	private NodeLoaderNodes<TNode, TEventArgs> _nodes;

	private List<TNode> _list = new List<TNode>();

	private NodeLoader _ldr;

	public NodeLoaderNodes<TNode, TEventArgs> Nodes => _nodes;

	internal NodeDataGrpIntr Grp => _grpSelector(_ldr);

	public uint AvailNodeCount => Grp.TotalNodeCnt;

	public uint LoadedStartPosition => Grp.StartPos;

	public uint LoadedNodeCount => Grp.RetNodeCnt;

	public uint PageSize
	{
		get
		{
			return Grp.MaxRetNodeCnt;
		}
		set
		{
			Grp.MaxRetNodeCnt = value;
		}
	}

	public ulong ParentNodeId
	{
		get
		{
			return Grp.ParentId;
		}
		set
		{
			Grp.ParentId = value;
		}
	}

	public ulong NodeId
	{
		get
		{
			return Grp.NodeId;
		}
		set
		{
			Grp.NodeId = value;
		}
	}

	internal NodeDataGroup(NodeLoader ldr, Func<NodeLoader, NodeDataGrpIntr> grpSelector)
	{
		if (ldr == null || grpSelector == null)
		{
			throw new ArgumentNullException();
		}
		_ldr = ldr;
		_grpSelector = grpSelector;
		_nodes = new NodeLoaderNodes<TNode, TEventArgs>(ldr);
	}

	public void PageMoveNext()
	{
		Grp.PageMoveNext();
	}

	public void PageMovePrev()
	{
		Grp.PageMovePrev();
	}

	public void SelectAll(bool select = true)
	{
		Grp.SelectAll(select);
	}
}}
