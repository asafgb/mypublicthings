﻿using System;

namespace NetLimiter.Service{

[Flags]
public enum StatsIntegrityError
{
	None = 0,
	InvalidFileSize = 1,
	InvalidAppId = 2,
	CantOpenFile = 4,
	CantMoveFile = 8,
	InvalidTime = 0x10,
	CantDeleteFile = 0x20,
	CantSetFilePos = 0x40,
	CantReadFile = 0x80,
	CantWriteFile = 0x100,
	CantOpenOrReadAppFile = 0x200,
	Failed = 0x800
}}
