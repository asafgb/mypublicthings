﻿using System;
using System.Linq;

namespace NetLimiter.Service{

[Serializable]
public class SupportedFeatures
{
	private bool[] _isSupported;

	public SupportedFeatures(string editionId)
	{
		int num = Enum.GetValues(typeof(ProductFeatures)).Cast<int>().Max();
		_isSupported = Enumerable.Repeat(element: true, num + 1).ToArray();
		_ = editionId == "lite";
	}

	public bool IsSupported(ProductFeatures feature)
	{
		return _isSupported[(int)feature];
	}

	public bool IsCreateRuleSupported(Rule rule)
	{
		if (rule is FwRule && !IsSupported(ProductFeatures.Blocker))
		{
			return false;
		}
		if (rule is QuotaRule && !IsSupported(ProductFeatures.Quotas))
		{
			return false;
		}
		return true;
	}
}}
