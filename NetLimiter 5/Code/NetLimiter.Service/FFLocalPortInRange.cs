﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFLocalPortInRange : FilterFunctionT<PortRangeFilterValue>
{
	public FFLocalPortInRange()
	{
	}

	public FFLocalPortInRange(PortRangeFilterValue range)
	{
		base.Values.Add(range);
	}
}}
