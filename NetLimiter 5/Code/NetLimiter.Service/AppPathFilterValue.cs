﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract(Name = "appPath")]
public class AppPathFilterValue
{
	[DataMember(Name = "path")]
	private string _path;

	public string Path => _path;

	public AppPathFilterValue(string path)
	{
		if (path.Length >= 260)
		{
			throw new ArgumentException($"Path length greater then MAX_PATH");
		}
		_path = path;
	}

	public static implicit operator AppPathFilterValue(string path)
	{
		return new AppPathFilterValue(path);
	}

	public static implicit operator string(AppPathFilterValue path)
	{
		return path.Path;
	}
}}
