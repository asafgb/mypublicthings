﻿using System;
using System.Runtime.Serialization;
using CoreLib.Net;

namespace NetLimiter.Service{

[DataContract]
public class FirewallRequest
{
	[DataMember]
	public ulong Id;

	[DataMember]
	public string RuleId;

	[DataMember]
	public int Weight;

	[DataMember]
	public DateTime Time;

	[DataMember]
	public ConnectionType ConnectionType;

	[DataMember]
	public NLProtoEth ProtoEth;

	[DataMember]
	public int ProtoIp;

	[DataMember]
	public IPAddress LocalAddress;

	[DataMember]
	public IPAddress RemoteAddress;

	[DataMember]
	public string RemoteName;

	[DataMember]
	public ushort LocalPort;

	[DataMember]
	public ushort RemotePort;

	[DataMember]
	public AppId AppId;

	[DataMember]
	public uint ProcessId;
}}
