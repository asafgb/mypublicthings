﻿using System;

namespace NetLimiter.Service{

public class FwRequestReply
{
	public FwAction Action { get; set; }

	public TimeSpan Duration { get; set; }
}}
