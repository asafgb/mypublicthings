﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class FeatureNotAvailableException : NLException
{
	public FeatureNotAvailableException(NLLicense lic)
		: base(string.Format(Langs.MsgFeatureNotAvailable, lic.EditionId))
	{
	}

	public FeatureNotAvailableException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
