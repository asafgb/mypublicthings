﻿namespace NetLimiter.Service{

public class StatsQueryHelperDictKeyV6
{
	public ushort Ip_W0 { get; set; }

	public ushort Ip_W1 { get; set; }

	public ushort Ip_W2 { get; set; }

	public ushort Ip_W3 { get; set; }

	public ushort Ip_W4 { get; set; }

	public ushort Ip_W5 { get; set; }

	public ushort Ip_W6 { get; set; }

	public ushort Ip_W7 { get; set; }

	public ushort Port { get; set; }

	public ushort AppId { get; set; }
}}
