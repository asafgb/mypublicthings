﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoreLib;
using CoreLib.Process;
using LicenseKeeper.Api.Server;
using LicenseKeeper.Api.Server.License;
using LicenseKeeper.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Win32;
using NetLimiter.Service.Api;
using NetLimiter.Service.AppList;
using NetLimiter.Service.Notifications;
using Newtonsoft.Json;
using NLInfoTools;

namespace NetLimiter.Service{

[CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
public class NLClient : INLServiceCallback, IDisposable
{
	private static readonly ILogger<NLClient> _logger = Logging.LoggerFactory.CreateLogger<NLClient>();

	private object _ruleListLock = new object();

	private object _filterListLock = new object();

	private List<Filter> _filters;

	private volatile SynchronizationContext _synCtx;

	private List<Rule> _rules;

	private volatile INLSvcCnn _cnn;

	private volatile string _hostName;

	private volatile string _userName;

	private volatile NLLicense _license;

	private volatile string _localhostFootprint;

	private volatile bool _isLocalhost;

	private volatile string _addr;

	private volatile ushort _port;

	private string _svcPath;

	private string _clientId;

	private volatile bool _isBfeRunning;

	private volatile System.Exception _connectError;

	public bool IsBfeRunning => _isBfeRunning;

	public int SettingsVersion { get; protected set; }

	public string ServiceVersion { get; protected set; }

	public int ConfirmedSettingsVersion { get; protected set; }

	public string SvcAddress
	{
		get
		{
			if (_cnn != null)
			{
				return _cnn.SvcAddress;
			}
			return null;
		}
	}

	public NLServiceState State
	{
		get
		{
			if (_cnn != null)
			{
				return _cnn.State;
			}
			return NLServiceState.Initial;
		}
	}

	public string Address => _addr;

	public ushort Port => _port;

	public string UserName => _userName;

	public Version OSVersion { get; protected set; }

	public bool IsWin10OrGreater
	{
		get
		{
			Version oSVersion = OSVersion;
			if ((object)oSVersion == null)
			{
				return false;
			}
			return oSVersion.Major >= 10;
		}
	}

	public SynchronizationContext SynchronizationContext
	{
		get
		{
			return _synCtx;
		}
		set
		{
			_synCtx = value;
		}
	}

	public System.Exception ConnectError
	{
		get
		{
			return _connectError;
		}
		protected set
		{
			_connectError = value;
		}
	}

	public List<Filter> Filters
	{
		get
		{
			lock (_filterListLock)
			{
				return (_filters == null) ? null : _filters.ToList();
			}
		}
	}

	public List<Rule> Rules
	{
		get
		{
			lock (_ruleListLock)
			{
				return (_rules == null) ? null : _rules.ToList();
			}
		}
	}

	public List<AppInfo> Apps => _cnn.Svc.Apps.Where((AppInfo x) => x != null).ToList();

	public ushort TcpPort
	{
		get
		{
			return _cnn.Svc.TcpPort;
		}
		set
		{
			_cnn.Svc.TcpPort = value;
		}
	}

	public string TcpAddress
	{
		get
		{
			return _cnn.Svc.TcpAddress;
		}
		set
		{
			_cnn.Svc.TcpAddress = value;
		}
	}

	public bool UseTcpBinding
	{
		get
		{
			return _cnn.Svc.UseTcpBinding;
		}
		set
		{
			_cnn.Svc.UseTcpBinding = value;
		}
	}

	public bool UseNpBinding
	{
		get
		{
			return _cnn.Svc.UseNpBinding;
		}
		set
		{
			_cnn.Svc.UseNpBinding = value;
		}
	}

	public bool IsStatsEnabled
	{
		get
		{
			return _cnn.Svc.IsStatsEnabled;
		}
		set
		{
			_cnn.Svc.IsStatsEnabled = value;
		}
	}

	public bool IsFwEnabled
	{
		get
		{
			return _cnn.Svc.IsFwEnabled;
		}
		set
		{
			_cnn.Svc.IsFwEnabled = value;
		}
	}

	public bool RunClientOnFwRqeust
	{
		get
		{
			return _cnn.Svc.RunClientOnFwRequest;
		}
		set
		{
			_cnn.Svc.RunClientOnFwRequest = value;
		}
	}

	public bool IsLimiterEnabled
	{
		get
		{
			return _cnn.Svc.IsLimiterEnabled;
		}
		set
		{
			_cnn.Svc.IsLimiterEnabled = value;
		}
	}

	public bool IsPriorityEnabled
	{
		get
		{
			return _cnn.Svc.IsPriorityEnabled;
		}
		set
		{
			_cnn.Svc.IsPriorityEnabled = value;
		}
	}

	public string Ip2LocFolderName => _cnn.Svc.Ip2LocFolderName;

	public string SettingsFolderName => _cnn.Svc.SettingsFolderName;

	public string SettingsFileName => _cnn.Svc.SettingsFileName;

	public string StatsFolderName
	{
		get
		{
			return _cnn.Svc.StatsFolderName;
		}
		set
		{
			_cnn.Svc.StatsFolderName = value;
		}
	}

	public string StatsFolderNameNL4 => _cnn.Svc.StatsFolderNameNL4;

	public int StatsUpdateTime
	{
		get
		{
			return _cnn.Svc.StatsUpdateTime;
		}
		set
		{
			_cnn.Svc.StatsUpdateTime = value;
		}
	}

	public bool StatsSaveInternet
	{
		get
		{
			return _cnn.Svc.StatsSaveInternet;
		}
		set
		{
			_cnn.Svc.StatsSaveInternet = value;
		}
	}

	public bool StatsSaveLocal
	{
		get
		{
			return _cnn.Svc.StatsSaveLocal;
		}
		set
		{
			_cnn.Svc.StatsSaveLocal = value;
		}
	}

	public bool StatsSaveLoopback
	{
		get
		{
			return _cnn.Svc.StatsSaveLoopback;
		}
		set
		{
			_cnn.Svc.StatsSaveLoopback = value;
		}
	}

	public List<FirewallRequest> FirewallRequests => _cnn.Svc.FirewallRequests;

	public string VTAPIKey
	{
		get
		{
			return _cnn.Svc.VTAPIKey;
		}
		set
		{
			_cnn.Svc.VTAPIKey = value;
		}
	}

	public bool UseVTFileChecks
	{
		get
		{
			return _cnn.Svc.UseVTFileChecks;
		}
		set
		{
			_cnn.Svc.UseVTFileChecks = value;
		}
	}

	public List<Network> Networks => _cnn.Svc.Networks;

	public string HostName => _hostName;

	public NLLicense License => _license;

	public bool IsLocalhost => _isLocalhost;

	public PrioritySettings PrioritySettings
	{
		get
		{
			return _cnn.Svc.PrioritySettings;
		}
		set
		{
			_cnn.Svc.PrioritySettings = value;
		}
	}

	public event EventHandler<FilterEventArgs> FilterAdded;

	public event EventHandler<FilterUpdateEventArgs> FilterUpdated;

	public event EventHandler<AppInfoEventArgs> AppUpdated;

	public event EventHandler<FilterEventArgs> FilterRemoved;

	public event EventHandler<RuleEventArgs> RuleAdded;

	public event EventHandler<RuleUpdateEventArgs> RuleUpdated;

	public event EventHandler<RuleEventArgs> RuleRemoved;

	public event EventHandler FirewallRequestChange;

	public event EventHandler NetworkChanged;

	public event EventHandler<AppInfoEventArgs> AppAdded;

	public event EventHandler<AppIdEventArgs> AppRemoved;

	public event EventHandler<OptionsChangeEventArgs> OptionsChanged;

	public event EventHandler<StatsToolsCompletedEventArgs> StatsToolsCompleted;

	public event EventHandler<StatsQueryUpdateEventArgs> StatsQueryUpdate;

	public event EventHandler<VTReportEventArgs> VTReportUpdate;

	public event EventHandler<CnnEventLogUpdateEventArgs> CnnEventLogUpdate;

	public event EventHandler LicenseChanged;

	public event EventHandler StateChanged;

	public event EventHandler RuleOrderChanged;

	public event EventHandler IsBfeRunningChanged;

	public event EventHandler PrioritySettingsChanged;

	public event EventHandler NotificationsChanged;

	public Filter GetInternetZone()
	{
		return Filters.FirstOrDefault((Filter x) => x.Id == "{2bc7c021-b058-45dc-b22f-73d8e10e3fef}");
	}

	public Filter GetLocalNetworkZone()
	{
		return Filters.FirstOrDefault((Filter x) => x.Id == "{cbe59154-31dc-4cfb-a12c-7fffb87ff400}");
	}

	public Filter GetAnyFilter()
	{
		return Filters.FirstOrDefault((Filter x) => x.Id == "{c053e57e-d554-49cb-ac70-e0ce95302faa}");
	}

	public NLClient()
	{
		InitInternal();
	}

	private void InitInternal()
	{
		if (SynchronizationContext.Current != null)
		{
			_synCtx = SynchronizationContext.Current;
		}
		else
		{
			_synCtx = new SynchronizationContext();
		}
	}

	public void Connect(string hostName = null, ushort port = 0, string userName = null, SecureString password = null)
	{
		_addr = hostName;
		_port = port;
		_userName = userName;
		Connect(new NLWcfSvcCnn(this, hostName, port, userName, password));
	}

	internal void Connect(INLSvcCnn cnn)
	{
		if (cnn == null)
		{
			throw new ArgumentNullException();
		}
		lock (this)
		{
			if (_cnn != null)
			{
				throw new NLException("Connect already called");
			}
			_cnn = cnn;
			_cnn.StateChanged += Cnn_StateChanged;
		}
		ConnectError = null;
		try
		{
			_cnn.Connect();
		}
		catch (System.Exception ex)
		{
			System.Exception ex3 = (ConnectError = ex);
			throw ex3;
		}
	}

	~NLClient()
	{
		Close();
	}

	private T Invoke<T>(Func<T> f)
	{
		try
		{
			return f();
		}
		catch (FaultException<NLFaultContract> fault)
		{
			throw OnFault(fault);
		}
	}

	private void Invoke(Action a)
	{
		try
		{
			a();
		}
		catch (FaultException<NLFaultContract> fault)
		{
			throw OnFault(fault);
		}
	}

	private System.Exception OnFault(FaultException<NLFaultContract> fault)
	{
		System.Exception ex = fault.Detail.ToException();
		_logger.LogError(ex.Message);
		return ex;
	}

	private void OnConnected(ServiceInfo svcInfo)
	{
		_logger.LogInformation("Connected to {0}: osver={1}", svcInfo.HostName, svcInfo.OSVersion);
		_hostName = svcInfo.HostName;
		_license = svcInfo.License;
		_localhostFootprint = svcInfo.LocalhostFootprint;
		_isBfeRunning = svcInfo.IsBfeRunning;
		SettingsVersion = svcInfo.SettingsVersion;
		_svcPath = svcInfo.SvcInstPath;
		_clientId = svcInfo.ClientId;
		ServiceVersion = svcInfo.ServiceVersion;
		Version version = new Version(4, 1, 1);
		if (!Version.TryParse(ServiceVersion, out var result) || result < version)
		{
			throw new ServiceVersionTooLow(ServiceVersion);
		}
		try
		{
			OSVersion = new Version(svcInfo.OSVersion);
		}
		catch (System.Exception exception)
		{
			_logger.LogError(exception, "Failed initialize OSVersion: {0}", svcInfo.OSVersion);
		}
		try
		{
			using RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
			using RegistryKey registryKey2 = registryKey.OpenSubKey("Software\\Locktime Software\\NetLimiter\\5", writable: false);
			string text = registryKey2.GetValue("LocalhostFootprint") as string;
			_isLocalhost = _localhostFootprint == text;
		}
		catch (System.Exception exception2)
		{
			_logger.LogError(exception2, "Failed to handle localhost footprint");
		}
		if (_isLocalhost && svcInfo.IsElevationRequired)
		{
			try
			{
				svcInfo.IsElevationRequired = CheckElevationRequired(svcInfo);
			}
			catch (System.Exception exception3)
			{
				_logger.LogError(exception3, "RequestAdminRights failed");
			}
		}
		lock (_filterListLock)
		{
			_filters = new List<Filter>();
			_ = _cnn.Svc.IsFwEnabled;
			_ = _cnn.Svc.Filters;
			_cnn.Svc.Filters.ForEach(delegate(Filter flt)
			{
				_filters.Add(flt);
			});
			_filters.ForEach(delegate(Filter flt)
			{
				InitFilter(flt);
			});
		}
		lock (_ruleListLock)
		{
			_rules = _cnn.Svc.Rules;
		}
	}

	private bool CheckElevationRequired(ServiceInfo svcInfo)
	{
		string text = Path.Combine(svcInfo.SvcInstPath, "NLSvcCliCnnCheck.exe");
		if (!File.Exists(text))
		{
			_logger.LogWarning("NLSvcCliCnnCheck not found: path={path}", text);
			return false;
		}
		Process process = Process.Start(text, svcInfo.ClientId);
		try
		{
			return _cnn.Svc.CheckElevationRequired();
		}
		finally
		{
			process.Kill();
		}
	}

	public Filter AddFilter(Filter flt)
	{
		if (flt == null)
		{
			throw new ArgumentNullException();
		}
		Filter fltNew = null;
		Invoke(() => fltNew = _cnn.Svc.AddFilter(flt));
		CopyUpdatedFilter(fltNew, flt);
		_synCtx.Send(delegate
		{
			OnFilterAdded(fltNew);
		}, null);
		return fltNew;
	}

	public void RemoveFilter(Filter flt)
	{
		if (flt == null)
		{
			throw new ArgumentNullException();
		}
		Invoke(delegate
		{
			_cnn.Svc.RemoveFilter(flt.Id);
		});
		_synCtx.Send(delegate
		{
			OnFilterRemoved(flt.Id);
		}, null);
	}

	public FilterUpdateInfo UpdateFilter(Filter flt)
	{
		if (flt == null)
		{
			throw new ArgumentNullException();
		}
		FilterUpdateInfo info = null;
		Invoke(() => info = _cnn.Svc.UpdateFilter(flt));
		CopyUpdatedFilter(info.Filter, flt);
		if (info.HasChange)
		{
			_synCtx.Send(delegate
			{
				OnFilterUpdated(info);
			}, null);
		}
		return info;
	}

	private void CopyUpdatedFilter(Filter from, Filter to)
	{
		to._id = from.Id;
		to._internalId = from.InternalId;
		to._revision = from.Revision;
		to.CreatedTime = from.CreatedTime;
		to.UpdatedTime = from.UpdatedTime;
		to.Name = from.Name;
	}

	public StatsDbFileInfo StatsGetDBFileInfo()
	{
		return Invoke(() => _cnn.Svc.StatsGetDbFileInfo());
	}

	public void Stats2GetDBInfo()
	{
		Invoke(delegate
		{
			_cnn.Svc.Stats2GetDbInfo();
		});
	}

	public StatsResultList StatsQuery(Stats2Query query)
	{
		return Invoke(() => _cnn.Svc.StatsQuery(query));
	}

	public StatsResultList StatsGetQueryResults(string queryName)
	{
		return Invoke(() => _cnn.Svc.StatsGetQueryResults(queryName));
	}

	public void StatsStopQuery(string queryName)
	{
		Invoke(delegate
		{
			_cnn.Svc.StatsStopQuery(queryName);
		});
	}

	public List<string> GetStatsApps()
	{
		return Invoke(() => _cnn.Svc.GetStatsApps());
	}

	public DateTime GetStatsFirstRecordTime()
	{
		return Invoke(() => _cnn.Svc.GetStatsFirstRecordTime());
	}

	public bool ChangeStatsDataLocation(string newLocation, bool moveFiles)
	{
		return Invoke(() => _cnn.Svc.ChangeStatsDataLocation(newLocation, moveFiles));
	}

	public bool StatsCompactData(DateTime start, DateTime end, uint interval)
	{
		return Invoke(() => _cnn.Svc.StatsCompactData(start, end, interval));
	}

	public bool StatsDeleteData(List<long> appIdList)
	{
		return Invoke(() => _cnn.Svc.StatsDeleteData(appIdList));
	}

	public bool Stats2DeleteDataByTime(DateTime start, DateTime end)
	{
		return Invoke(() => _cnn.Svc.Stats2DeleteDataByTime(start, end));
	}

	public bool Stats2ImportNL4Database(string location)
	{
		return Invoke(() => _cnn.Svc.Stats2ImportNL4Database(location));
	}

	public bool StatsDeleteDatabase()
	{
		return Invoke(() => _cnn.Svc.StatsDeleteDatabase());
	}

	public bool StatsEnableCompression(bool enableCompression)
	{
		return Invoke(() => _cnn.Svc.StatsEnableCompression(enableCompression));
	}

	public bool StatsMergeData(List<long> appList, long mergeApp)
	{
		return Invoke(() => _cnn.Svc.StatsMergeData(appList, mergeApp));
	}

	public bool StatsCheckIntegrity(string alternateDbPath = null)
	{
		return Invoke(() => _cnn.Svc.StatsIntegrityCheck(alternateDbPath));
	}

	public bool StatsRepairData(StatsBadRowsListInfo badRows)
	{
		return Invoke(() => _cnn.Svc.StatsRepairData(badRows));
	}

	public string InstallIp2LocDB(string path)
	{
		return Invoke(() => _cnn.Svc.InstallIp2LocDB(path));
	}

	public Ip2LocVersion GetIp2LocVersion()
	{
		return Invoke(() => _cnn.Svc.GetIp2LocVersion());
	}

	public Location GetIpLocation(IPAddress ip)
	{
		return Invoke(() => _cnn.Svc.GetIpLocation(ip));
	}

	public VTFileReport GetVTReport(string filePath)
	{
		return Invoke(() => _cnn.Svc.GetVTReport(filePath));
	}

	internal void InitFilter(Filter flt)
	{
	}

	void INLServiceCallback.OnFilterAdded(Filter filter)
	{
		_synCtx.Post(delegate
		{
			OnFilterAdded(filter);
		}, null);
	}

	private void OnFilterAdded(Filter filter)
	{
		lock (_filterListLock)
		{
			if (_filters == null || _filters.Exists((Filter x) => x.Id == filter.Id))
			{
				return;
			}
			_filters.Add(filter);
			InitFilter(filter);
		}
		_logger.LogInformation("Filter added: id={0} name={1}", filter?.Id, filter?.Name);
		this.FilterAdded?.Invoke(this, new FilterEventArgs(filter));
	}

	void INLServiceCallback.OnFilterRemoved(string fltId)
	{
		_synCtx.Post(delegate
		{
			OnFilterRemoved(fltId);
		}, null);
	}

	private void OnFilterRemoved(string fltId)
	{
		_logger.LogTrace(">>");
		_logger.LogInformation("Filter removed: id={0}", fltId);
		Filter filter = null;
		lock (_filterListLock)
		{
			if (_filters == null)
			{
				return;
			}
			filter = _filters.Find((Filter x) => x.Id == fltId);
			if (filter == null)
			{
				return;
			}
			_filters.Remove(filter);
			_logger.LogInformation("Filter found: name={0}", filter.Name);
		}
		lock (_ruleListLock)
		{
			foreach (Rule item in _rules.Where((Rule x) => x.FilterId == fltId).ToList())
			{
				((INLServiceCallback)this).OnRuleRemoved(item.Id);
			}
		}
		this.FilterRemoved?.Invoke(this, new FilterEventArgs(filter));
		_logger.LogTrace("<<");
	}

	void INLServiceCallback.OnFilterUpdated(FilterUpdateInfo info)
	{
		_synCtx.Post(delegate
		{
			OnFilterUpdated(info);
		}, null);
	}

	private void OnFilterUpdated(FilterUpdateInfo info)
	{
		_logger.LogTrace(">>");
		Filter filter = null;
		_logger.LogInformation("Filter updated: id={0}, name={1}", info.Filter.Id, info.Filter.Name);
		lock (_filterListLock)
		{
			if (_filters == null)
			{
				return;
			}
			filter = _filters.Find((Filter x) => x.Id == info.Filter.Id);
			if (filter == null)
			{
				_logger.LogDebug("Filter not found");
				OnFilterAdded(info.Filter);
				return;
			}
			if (filter.Revision >= info.Filter.Revision)
			{
				_logger.LogDebug("Filter revision old: {0}/{1}", filter.Revision, info.Filter.Revision);
				return;
			}
			_filters[_filters.IndexOf(filter)] = info.Filter;
			InitFilter(info.Filter);
		}
		this.FilterUpdated?.Invoke(this, new FilterUpdateEventArgs(info, filter));
		_logger.LogTrace("<<");
	}

	public void RemoveFilterDeep(Filter filter)
	{
		if (filter == null)
		{
			throw new ArgumentNullException("zone");
		}
		_cnn.Svc.RemoveFilterDeep(filter.Id);
	}

	public NodeLoader CreateNodeLoader()
	{
		return new NodeLoader(_cnn.Svc);
	}

	public bool RemoveApp(AppId appId)
	{
		return Invoke(() => _cnn.Svc.RemoveApp(appId));
	}

	public void Close()
	{
		if (_cnn != null)
		{
			_cnn.Close();
		}
	}

	public void Dispose()
	{
		Close();
	}

	public void SetDefaultFwAction(FwAction fwaIn, FwAction fwaOut)
	{
		_cnn.Svc.SetDefaultFwAction(fwaIn, fwaOut);
	}

	public void GetDefaultFwAction(out FwAction fwaIn, out FwAction fwaOut)
	{
		_cnn.Svc.GetDefaultFwAction(out fwaIn, out fwaOut);
	}

	void INLServiceCallback.OnNotification(NLServiceNotificationType type)
	{
		switch (type)
		{
		case NLServiceNotificationType.FirewallRequest:
			RunAsync(delegate
			{
				this.FirewallRequestChange?.Invoke(this, EventArgs.Empty);
			});
			break;
		case NLServiceNotificationType.NetworkChange:
			RunAsync(delegate
			{
				this.NetworkChanged?.Invoke(this, EventArgs.Empty);
			});
			break;
		case NLServiceNotificationType.RuleOrderChanged:
			OnRuleOrderChanged();
			break;
		case NLServiceNotificationType.BfeStateChange:
			RunAsync(delegate
			{
				_isBfeRunning = _cnn.Svc.IsBfeRunning;
				this.IsBfeRunningChanged?.Invoke(this, EventArgs.Empty);
			});
			break;
		case NLServiceNotificationType.PriSettingsChanged:
			RunAsync(delegate
			{
				this.PrioritySettingsChanged?.Invoke(this, EventArgs.Empty);
			});
			break;
		case NLServiceNotificationType.NotificationsChanged:
			RunAsync(delegate
			{
				this.NotificationsChanged?.Invoke(this, EventArgs.Empty);
			});
			break;
		case NLServiceNotificationType.QuotaChange:
			break;
		}
	}

	private void RunAsync(Action action, int repeatOnTimeout = 5)
	{
		_synCtx.Post(delegate
		{
			RunAsyncCallback(action, repeatOnTimeout);
		}, null);
	}

	private void RunAsyncCallback(Action action, int repeatOnTimeout)
	{
		try
		{
			action();
		}
		catch (TimeoutException exception)
		{
			if (repeatOnTimeout > 0)
			{
				RunAsync(action, repeatOnTimeout--);
			}
			else
			{
				_logger.LogError(exception, "RunAsync failed");
			}
		}
		catch (System.Exception exception2)
		{
			_logger.LogError(exception2, "Failed to invoke the action");
		}
	}

	void INLServiceCallback.OnLicenseChange(NLLicense license)
	{
		_synCtx.Post(delegate
		{
			_license = license;
			if (this.LicenseChanged != null)
			{
				this.LicenseChanged(this, EventArgs.Empty);
			}
		}, null);
	}

	public void ReplyFirewallRequest(FirewallRequest fwr, FwAction action)
	{
		_cnn.Svc.ReplyFirewallRequest(fwr.Id, new FwRequestReply
		{
			Action = action
		});
	}

	public void ReplyFirewallRequest(FirewallRequest fwr, FwRequestReply reply)
	{
		_cnn.Svc.ReplyFirewallRequest(fwr.Id, reply);
	}

	public void ApproveNetwork(Network ntw)
	{
		_cnn.Svc.ApproveNetwork(ntw.Id);
		ntw.IsApproved = true;
	}

	void INLServiceCallback.OnAppAdded(AppInfo info)
	{
		_synCtx.Post(delegate
		{
			if (info != null)
			{
				this.AppAdded?.Invoke(this, new AppInfoEventArgs(info));
			}
		}, null);
	}

	void INLServiceCallback.OnAppRemoved(AppId appId)
	{
		_synCtx.Post(delegate
		{
			this.AppRemoved?.Invoke(this, new AppIdEventArgs(appId));
		}, null);
	}

	void INLServiceCallback.OnAppUpdated(AppInfo info)
	{
		_synCtx.Post(delegate
		{
			this.AppUpdated?.Invoke(this, new AppInfoEventArgs(info));
		}, null);
	}

	public void RemoveNetwork(string ntwId)
	{
		Invoke(delegate
		{
			_cnn.Svc.RemoveNetwork(ntwId);
		});
	}

	void INLServiceCallback.OnRuleAdded(Rule rule)
	{
		_synCtx.Post(delegate
		{
			OnRuleAdded(rule);
		}, null);
	}

	private void OnRuleAdded(Rule rule)
	{
		_logger.LogTrace(">>");
		lock (_ruleListLock)
		{
			if (_rules == null || _rules.Exists((Rule x) => x.Id == rule.Id))
			{
				return;
			}
			_rules.Add(rule);
		}
		_logger.LogInformation("Ruled added: type={0}, id={1}", rule.GetType().Name, rule.Id);
		this.RuleAdded?.Invoke(this, new RuleEventArgs(rule));
		_logger.LogTrace("<<");
	}

	void INLServiceCallback.OnRuleRemoved(string ruleId)
	{
		_synCtx.Post(delegate
		{
			OnRuleRemoved(ruleId);
		}, null);
	}

	private void OnRuleRemoved(string ruleId)
	{
		_logger.LogTrace(">>");
		Rule rule = null;
		lock (_ruleListLock)
		{
			if (_rules == null)
			{
				return;
			}
			rule = _rules.Find((Rule x) => x.Id == ruleId);
			if (rule == null)
			{
				_logger.LogInformation("Rule not found: id={0}", ruleId);
				return;
			}
			_rules.Remove(rule);
			_logger.LogInformation("Rule removed: type={0}, id={1}", rule.GetType().Name, ruleId);
		}
		this.RuleRemoved?.Invoke(this, new RuleEventArgs(rule));
		_logger.LogTrace("<<");
	}

	void INLServiceCallback.OnRuleUpdated(RuleUpdateInfo info)
	{
		_synCtx.Post(delegate
		{
			OnRuleUpdated(info);
		}, null);
	}

	private void OnRuleUpdated(RuleUpdateInfo info)
	{
		_logger.LogTrace(">>");
		Rule rule = null;
		bool flag = false;
		lock (_ruleListLock)
		{
			if (_rules == null)
			{
				return;
			}
			rule = _rules.Find((Rule x) => x.Id == info.Rule.Id);
			if (rule != null)
			{
				flag = !ObjectCloner.IsBinaryClone(rule, info.Rule);
				_rules[_rules.IndexOf(rule)] = info.Rule;
			}
		}
		if (flag)
		{
			_logger.LogInformation("Rule updated: type={0}, id={1}", rule.GetType().Name, rule.Id);
			this.RuleUpdated?.Invoke(this, new RuleUpdateEventArgs(info, rule));
		}
		_logger.LogTrace("<<");
	}

	public Rule AddRule(string filterId, Rule rule)
	{
		if (rule == null)
		{
			throw new ArgumentNullException();
		}
		Rule ruleNew = null;
		Invoke(() => ruleNew = _cnn.Svc.AddRule(filterId, rule));
		_synCtx.Send(delegate
		{
			OnRuleAdded(ruleNew);
		}, null);
		return ruleNew;
	}

	public void RemoveRule(Rule rule)
	{
		if (rule == null)
		{
			throw new ArgumentNullException();
		}
		Invoke(delegate
		{
			_cnn.Svc.RemoveRule(rule.Id);
		});
		_synCtx.Send(delegate
		{
			OnRuleRemoved(rule.Id);
		}, null);
	}

	public RuleUpdateInfo UpdateRule(Rule rule)
	{
		if (rule == null)
		{
			throw new ArgumentNullException();
		}
		RuleUpdateInfo info = null;
		Invoke(() => info = _cnn.Svc.UpdateRule(rule));
		if (info.HasChange)
		{
			_synCtx.Send(delegate
			{
				OnRuleUpdated(info);
			}, null);
		}
		return info;
	}

	void INLServiceCallback.OnOptionsChange(OptionsChangeType changeType, object value)
	{
		_synCtx.Post(delegate
		{
			if (this.OptionsChanged != null)
			{
				this.OptionsChanged(this, new OptionsChangeEventArgs(changeType, value));
			}
		}, null);
	}

	public void ResetNodeTotals()
	{
		ResetNodeTotals(ulong.MaxValue);
	}

	public void ResetNodeTotals(ulong nodeId)
	{
		_cnn.Svc.ResetNodeTotals(nodeId);
	}

	public ChartData GetChart(ulong nodeId, NLFlowDir? dir = null)
	{
		return _cnn.Svc.GetChart(nodeId, dir);
	}

	public ChartAppCutData GetChartAppCut(DateTime time)
	{
		return _cnn.Svc.GetChartAppCut(time);
	}

	public IEnumerable<AccessTableRow> GetAccessTable()
	{
		return _cnn.Svc.GetAccessTable();
	}

	public void SetAccessTable(IEnumerable<AccessTableRow> table)
	{
		_cnn.Svc.SetAccessTable(table);
	}

	public string ResolveIdentity(string userOrGroupName)
	{
		return _cnn.Svc.ResolveIdentity(userOrGroupName);
	}

	public void RestartCommunication()
	{
		try
		{
			_cnn.Svc.RestartCommunication();
		}
		catch
		{
		}
	}

	private void Cnn_StateChanged(object sender, NLServiceStateChangedEventArgs e)
	{
		ConnectError = _cnn.ConnectError;
		if (ConnectError != null)
		{
			_logger.LogError(ConnectError, "ConnectError");
		}
		if (State == NLServiceState.Connected)
		{
			OnConnected(e.ServiceInfo);
		}
		if (this.StateChanged != null)
		{
			_synCtx.Send(delegate
			{
				try
				{
					this.StateChanged?.Invoke(this, EventArgs.Empty);
				}
				catch (System.Exception exception2)
				{
					Close();
					_logger.LogError(exception2, "Failed to invoke StateChanged event");
				}
			}, null);
		}
		if (e.ServiceInfo == null || !e.ServiceInfo.HasPendingFwr)
		{
			return;
		}
		_synCtx.Send(delegate
		{
			try
			{
				this.FirewallRequestChange?.Invoke(this, EventArgs.Empty);
			}
			catch (System.Exception exception)
			{
				Close();
				_logger.LogError(exception, "Failed to invoke FirewallRequestChange event");
			}
		}, null);
	}

	internal static async Task<RegData> CheckLicenseAsync(string hwCodeHash, string regCode)
	{
		CheckLicenseRequest value = new CheckLicenseRequest
		{
			Version = Enum.GetValues(typeof(RegDataVersion)).Cast<RegDataVersion>().Last(),
			RegCode = regCode,
			HWCodeHash = hwCodeHash
		};
			
		try
		{
			HttpResponseMessage httpResponseMessage = await new HttpClient().PostAsync(Common.ApiHostUrl + "/api/pub/CheckLicense", new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json"));
			if (!httpResponseMessage.IsSuccessStatusCode)
			{
				throw new System.Exception($"Failed to contact remote server: statusCode={httpResponseMessage.StatusCode}");
			}
			if (httpResponseMessage.Content == null)
			{
				throw new System.Exception("Remote server returned an empty response");
			}
			ApiResponse<CheckLicenseResponse> apiResponse;
			try
			{
				apiResponse = JsonConvert.DeserializeObject<ApiResponse<CheckLicenseResponse>>(await httpResponseMessage.Content.ReadAsStringAsync()) ?? throw new System.Exception();
			}
			catch
			{
				throw new System.Exception("Failed to deserialize the server response");
			}
			if (!apiResponse.Success)
			{
				if (apiResponse.StatusCode == 404)
				{
					throw new System.Exception("Subscription not found");
				}
				if (apiResponse.StatusCode == 1001)
				{
					throw new System.Exception("The subscription is cancelled");
				}
				string arg = "Remote server error";
				arg = (string.IsNullOrEmpty(apiResponse.Message) ? $"{arg}: statusCode={apiResponse.StatusCode}" : $"{arg}: {apiResponse.Message} ({apiResponse.StatusCode})");
				throw new System.Exception(arg);
			}
			if (apiResponse.Result.RegData == null)
			{
				throw new System.Exception("Remote server returned no registration data");
			}
			return apiResponse.Result.RegData;
		}
		catch (System.Exception exception)
		{
			string licensePath = GetLicensePath();
            if (File.Exists(licensePath))
			{
                RegData regData = null;
                    string data = File.ReadAllText(licensePath);
                _logger.LogInformation("RegData path: {path}", licensePath);
                 regData = JsonConvert.DeserializeObject<RegData>(data);
				return regData;
            }
			else
				{
					RegData reg = new RegData();
                    reg.Version = value.Version;
					reg.ProductId = "MiewProductID";
                    reg.EditionId = "pro";
                    reg.LicenseId = "AsafIsKing";
                    reg.PlanId = "MiewPlanID";
                    reg.IsRecurring = false;
                    reg.IsCancelled = false;
                    reg.EndTime = DateTime.Now.AddYears(999);
                    reg.Quantity = 1000;
                    reg.RegName = "Asaf-Is-King";
					reg.HWCodeHash = value.HWCodeHash;
                    reg.HWCodeHash = hwCodeHash;
					reg.Signature = "NoNeedAnyMore";
					File.WriteAllText(licensePath, JsonConvert.SerializeObject(reg));
                    return reg;
                }

            _logger.LogError(exception, "CheckLicenseAsync failed");
			throw;
		}
	}

        public static string GetLicensePath(bool create = true)
        {
			string _dataPath = "";
                _dataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Locktime\\NetLimiter\\5");
                if (create && !Directory.Exists(_dataPath))
                {
                    Directory.CreateDirectory(_dataPath);
                }
            return Path.Combine(_dataPath, "license.json");
        }





        public async Task SetRegistrationCodeAsync(string regCode)
	{
		string hwCodeHash = null;
		await Task.Run(delegate
		{
			hwCodeHash = _cnn.Svc.GetHWCodeHash();
		});
		try
		{
			RegData regData = await CheckLicenseAsync(hwCodeHash, regCode);
			await Task.Run(delegate
			{
				_license = _cnn.Svc.SetRegistrationData(regData);
			});
		}
		catch (System.Exception exception)
		{
			_logger.LogError(exception, "SetRegistrationCodeAsync failed");
			throw;
		}
	}

	public async Task ReloadLicense()
	{
		await SetRegistrationCodeAsync(License.RegCodeHash);
	}

	public void RemoveRegistrationData()
	{
		Invoke(() => _license = _cnn.Svc.RemoveRegistrationData());
	}

	void INLServiceCallback.OnStatsToolsCompleted(StatsToolsResult result)
	{
		_synCtx.Post(delegate
		{
			if (this.StatsToolsCompleted != null)
			{
				this.StatsToolsCompleted(this, new StatsToolsCompletedEventArgs
				{
					Result = result
				});
			}
		}, null);
	}

	void INLServiceCallback.OnStatsQueryUpdate(StatsQueryUpdateEventArgs args)
	{
		_synCtx.Post(delegate
		{
			if (this.StatsQueryUpdate != null)
			{
				this.StatsQueryUpdate(this, args);
			}
		}, null);
	}

	void INLServiceCallback.OnVTReport(VTReportEventArgs args)
	{
		_synCtx.Post(delegate
		{
			if (this.VTReportUpdate != null)
			{
				this.VTReportUpdate(this, args);
			}
		}, null);
	}

	void INLServiceCallback.OnCnnEventLogUpdate(CnnEventLogUpdateEventArgs args)
	{
		_synCtx.Post(delegate
		{
			if (this.CnnEventLogUpdate != null)
			{
				this.CnnEventLogUpdate(this, args);
			}
		}, null);
	}

	public Network UpdateNetwork(Network network)
	{
		return _cnn.Svc.UpdateNetwork(network);
	}

	public List<UserInfo> GetUserList()
	{
		return _cnn.Svc.GetUserList();
	}

	public bool IsAppInUse(AppId appId)
	{
		return _cnn.Svc.IsAppInUse(appId);
	}

	public void KillCnns(ulong nodeId)
	{
		_cnn.Svc.KillCnns(nodeId);
	}

	public void KillCnnsByFilterId(string fltId)
	{
		_cnn.Svc.KillCnnsByFilter(fltId);
	}

	public void KillCnnsByQuota(string ruleId, long quotaPtr = 0L)
	{
		_cnn.Svc.KillCnnsByQuota(ruleId, quotaPtr);
	}

	public void SetRuleTable(IEnumerable<Rule> rules)
	{
		_cnn.Svc.SetRuleTable(rules.Select((Rule x) => x.Id));
	}

	private void OnRuleOrderChanged()
	{
		List<Rule> rules = _cnn.Svc.Rules;
		lock (_ruleListLock)
		{
			foreach (Rule newRule in rules)
			{
				Rule rule = _rules.FirstOrDefault((Rule x) => x.Id == newRule.Id);
				if (rule != null)
				{
					_rules.Remove(rule);
					_rules.Add(rule);
				}
				else
				{
					OnRuleAdded(rule);
				}
			}
		}
		_synCtx.Post(delegate
		{
			this.RuleOrderChanged?.Invoke(this, EventArgs.Empty);
		}, null);
	}

	public void ResetQuota(QuotaRule rule, long quotaPtr = 0L, bool stop = false)
	{
		_cnn.Svc.ResetQuota(rule.Id, quotaPtr, stop);
	}

	public List<QuotaState> GetQuotaStates()
	{
		return _cnn.Svc.GetQuotaStates();
	}

	public async Task<List<QuotaState>> GetQuotaStatesAsync()
	{
		return await Task.Run(() => _cnn.Svc.GetQuotaStates());
	}

	public QuotaStateDetails GetQuotaStateDetails(string quotaRuleId, int ix)
	{
		return _cnn.Svc.GetQuotaStateDetails(quotaRuleId, ix);
	}

	public void InitQuotaTotalFromStats(string quotaRuleId)
	{
		_cnn.Svc.InitQuotaTotalFromStats(quotaRuleId);
	}

	public async Task<List<AppListSvcItem>> GetInstalledServicesAsync()
	{
		return await Task.Run(() => _cnn.Svc.GetInstalledServices());
	}

	public async Task<List<AppListStoreItem>> GetInstalledStoreAppsAsync()
	{
		return await Task.Run(() => _cnn.Svc.GetInstalledStoreApps());
	}

	public SidName GetSidName(Sid sid)
	{
		return _cnn.Svc.GetSidName(sid);
	}

	public AppInfo GetAppInfo(AppId appId)
	{
		return _cnn.Svc.GetAppInfo(appId);
	}

	public void UpdateAppInfo(AppId appId, AppInfo appInfo)
	{
		_cnn.Svc.UpdateAppInfo(appId, appInfo);
	}

	public Filter GetFilterByInternalId(uint id)
	{
		return _cnn.Svc.GetFilterByInternalId(id);
	}

	public List<AppId> GetRemovedApps()
	{
		return _cnn.Svc.GetRemovedApps();
	}

	public CnnLogEventLists GetCnnLog(CnnLogEventFilter filter)
	{
		return _cnn.Svc.GetCnnLog(filter);
	}

	public List<string> GetDeadFilters()
	{
		return _cnn.Svc.GetDeadFilters();
	}

	public bool UACElevate()
	{
		INLSvcCnn cnn = _cnn;
		if (cnn == null || cnn.State != NLServiceState.Connected)
		{
			return false;
		}
		if (!IsLocalhost)
		{
			throw new System.Exception("Client must be connected to localhost");
		}
		if (ProcessHelper.IsAdmin())
		{
			return true;
		}
		string text = Path.Combine(_svcPath, "NLCliElevator.exe");
		if (!File.Exists(text))
		{
			throw new System.Exception("NLCliElevator.exe not found");
		}
		ProcessStartInfo processStartInfo = new ProcessStartInfo();
		processStartInfo.FileName = text;
		processStartInfo.Arguments = "nlclientid:" + _clientId;
		processStartInfo.UseShellExecute = true;
		processStartInfo.CreateNoWindow = true;
		processStartInfo.Verb = "runas";
		try
		{
			Process.Start(processStartInfo).WaitForExit();
		}
		catch
		{
		}
		return !GetIsElevationRequired();
	}

	public ClientInfo GetClientInfo(string clientId)
	{
		return Invoke(() => _cnn.Svc.GetClientInfo(clientId));
	}

	public void Elevate(string clientId)
	{
		Invoke(delegate
		{
			_cnn.Svc.Elevate(clientId);
		});
	}

	public bool GetIsElevationRequired()
	{
		try
		{
			return Invoke(() => _cnn.Svc.GetClientInfo(null)).IsElevationRequired;
		}
		catch (AdminRequiredException)
		{
			return false;
		}
	}

	public void SubscribeAsFwReqeustHandler(bool subscribe)
	{
		Invoke(delegate
		{
			_cnn.Svc.SubscribeAsFwReqeustHandler(subscribe);
		});
	}

	public List<ServiceNotification> GetNotifications()
	{
		return Invoke(() => _cnn.Svc.GetNotifications());
	}

	public void ClearNotifications()
	{
		Invoke(delegate
		{
			_cnn.Svc.ClearNotifications();
		});
	}

	public void MuteNotifications(string[] notifIds)
	{
		Invoke(delegate
		{
			_cnn.Svc.MuteNotifications(notifIds);
		});
	}
}}
