﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFRemotePortInRange : FilterFunctionT<PortRangeFilterValue>
{
	public FFRemotePortInRange()
	{
	}

	public FFRemotePortInRange(PortRangeFilterValue range)
	{
		base.Values.Add(range);
	}
}}
