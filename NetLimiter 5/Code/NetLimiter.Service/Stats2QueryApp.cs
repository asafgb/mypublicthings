﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2QueryApp
{
	[DataMember]
	public string Path;

	[DataMember]
	public string Sid;

	[DataMember]
	public bool IsMatch = true;

	[DataMember]
	public bool IsFragment;
}}
