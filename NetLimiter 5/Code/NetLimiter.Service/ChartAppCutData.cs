﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class ChartAppCutData
{
	[DataMember]
	public List<ChartAppCutItem> Items;

	[DataMember]
	public DateTime UtcTime { get; set; }
}}
