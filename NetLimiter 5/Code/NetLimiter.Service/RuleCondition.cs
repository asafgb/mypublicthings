﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
[KnownType(typeof(TimeCondition))]
public abstract class RuleCondition
{
	[DataMember]
	public RuleConditionAction Action { get; set; }

	[DataMember]
	public DateTime LastExecuted { get; set; }
}}
