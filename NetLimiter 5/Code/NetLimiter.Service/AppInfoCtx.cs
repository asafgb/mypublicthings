﻿using System.ServiceProcess;
using NetLimiter.Service.AppList;

namespace NetLimiter.Service{

public class AppInfoCtx
{
	private ServiceController[] _services;

	public ServiceController[] Services
	{
		get
		{
			ServiceController[] obj = _services ?? ServiceController.GetServices();
			ServiceController[] result = obj;
			_services = obj;
			return result;
		}
	}

	public StorePackageManifest Manifest { get; set; }

	public AppId AppId { get; set; }
}}
