﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract(Name = "portRang")]
public class PortRangeFilterValue
{
	[DataMember(Name = "start")]
	public ushort Start;

	[DataMember(Name = "end")]
	public ushort End;

	public PortRangeFilterValue()
	{
	}

	public PortRangeFilterValue(ushort port)
	{
		Start = port;
		End = port;
	}

	public PortRangeFilterValue(ushort start, ushort end)
	{
		Start = start;
		End = end;
	}
}}
