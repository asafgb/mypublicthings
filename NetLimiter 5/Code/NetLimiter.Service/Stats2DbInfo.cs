﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2DbInfo
{
	[DataMember]
	public string Path;

	[DataMember]
	public ulong FileSize;

	[DataMember]
	public DateTime FirstData;

	[DataMember]
	public DateTime LastData;

	[DataMember]
	public List<Stats2DbApp> Apps;
}}
