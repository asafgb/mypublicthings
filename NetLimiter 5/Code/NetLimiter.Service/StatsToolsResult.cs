﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class StatsToolsResult
{
	[DataMember]
	public StatsToolsAction Action;

	[DataMember]
	public string StatsLocation = "";

	[DataMember]
	public Stats2DbInfo DbInfo;

	[DataMember]
	public Stats2Error Error;

	[DataMember]
	public bool Finished { get; set; }

	public bool Succeeded => Error == null;

	[DataMember]
	public ulong TotalRows { get; set; }

	[DataMember]
	public ulong TotalRowsProcessed { get; set; }
}}
