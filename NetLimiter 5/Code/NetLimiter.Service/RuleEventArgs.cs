﻿using System;

namespace NetLimiter.Service{

public class RuleEventArgs : EventArgs
{
	public Rule Rule { get; protected set; }

	public RuleEventArgs(Rule rule)
	{
		Rule = rule;
	}
}}
