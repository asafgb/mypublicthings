﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2DbApp
{
	[DataMember]
	public long Id;

	[DataMember]
	public string Path;

	[DataMember]
	public string Sid;
}}
