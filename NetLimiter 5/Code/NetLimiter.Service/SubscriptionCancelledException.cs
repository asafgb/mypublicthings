﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class SubscriptionCancelledException : NLException
{
	public SubscriptionCancelledException()
		: base(Langs.MsgSubscCancelled)
	{
	}

	public SubscriptionCancelledException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
