﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2ResultDataIpV6
{
	[DataMember]
	public ulong In;

	[DataMember]
	public ulong Out;

	[DataMember]
	public uint[] Ip = new uint[4];
}}
