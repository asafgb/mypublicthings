﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class StatsResultList
{
	[DataMember]
	public StatsResultTotals Totals = new StatsResultTotals();

	[DataMember]
	public List<StatsResult> Results = new List<StatsResult>();

	[DataMember]
	public Stats2Results Results2;
}}
