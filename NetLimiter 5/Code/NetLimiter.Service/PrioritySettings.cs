﻿namespace NetLimiter.Service{

public class PrioritySettings
{
	public PriorityReactionSpeedType ReactionSpeedType;

	public bool Adaptive { get; set; }

	public int ReactionSpeedMillis { get; set; }

	public int ReservationPercent { get; set; }

	public bool UseReservationWhenIdle { get; set; }

	public int CriticalPercent { get; set; }

	public int HighPercent { get; set; }

	public int NormalPercent { get; set; }

	public PrioritySettings()
	{
		Adaptive = true;
		ReactionSpeedType = PriorityReactionSpeedType.Normal;
		ReservationPercent = 20;
		UseReservationWhenIdle = false;
		CriticalPercent = 70;
		HighPercent = 65;
		NormalPercent = 65;
	}
}}
