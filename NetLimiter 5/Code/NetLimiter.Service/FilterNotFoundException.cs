﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class FilterNotFoundException : NLException
{
	public FilterNotFoundException(string fltId)
		: base(string.Format(Langs.MsgFilterNotFound, fltId))
	{
	}

	public FilterNotFoundException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
