﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFRemoteAddressInRange : FilterFunctionT<IPRangeFilterValue>
{
	public FFRemoteAddressInRange()
	{
	}

	public FFRemoteAddressInRange(IPRangeFilterValue range)
	{
		base.Values.Add(range);
	}
}}
