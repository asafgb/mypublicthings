﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class StatsBadRows
{
	[DataMember]
	public uint StartTime;

	[DataMember]
	public uint EndTime;

	[DataMember]
	public uint NumRows;

	[DataMember]
	public uint FirstBadTime;

	[DataMember]
	public ulong FirstRow;

	[DataMember]
	public uint Type;
}}
