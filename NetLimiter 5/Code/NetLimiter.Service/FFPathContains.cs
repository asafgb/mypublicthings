﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFPathContains : FilterFunctionT<AppPathFilterValue>
{
	public FFPathContains()
	{
	}

	public FFPathContains(string path)
	{
		base.Values.Add(path);
	}
}}
