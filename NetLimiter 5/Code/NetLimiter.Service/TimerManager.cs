﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using Microsoft.Extensions.Logging;

namespace NetLimiter.Service{

internal class TimerManager
{
	private class RunOnceItem
	{
		public TimeSpan Interval { get; set; }

		public Action Action { get; set; }
	}

	private static readonly ILogger<TimerManager> _logger = Logging.LoggerFactory.CreateLogger<TimerManager>();

	private System.Timers.Timer _timer;

	private DateTime _lastUtcTime;

	private SynchronizationContext _synCtx;

	private List<RunOnceItem> _runOnceItems = new List<RunOnceItem>();

	private Random _rnd = new Random();

	public EventHandler NewDayDetected;

	public void Start()
	{
		_logger.LogInformation("Starting timer manager");
		_synCtx = SynchronizationContext.Current;
		_lastUtcTime = DateTime.Now;
		_timer = new System.Timers.Timer(60000.0);
		_timer.Elapsed += _timer_Elapsed;
		_timer.Start();
	}

	private void _timer_Elapsed(object sender, ElapsedEventArgs e)
	{
		DateTime utcNow = DateTime.UtcNow;
		if (_lastUtcTime.Day != utcNow.Day)
		{
			_synCtx.Send(delegate
			{
				OnNewUtcDay();
			}, null);
		}
		_lastUtcTime = utcNow;
		CheckRunOnceItems();
	}

	private void OnNewUtcDay()
	{
		_logger.LogDebug("New day detected");
		NewDayDetected?.Invoke(this, EventArgs.Empty);
	}

	public void Stop()
	{
		_timer.Dispose();
		_timer = null;
	}

	public void Reset()
	{
	}

	public void RunRandomOncePerInternval(Action action, TimeSpan timeSpan)
	{
		if (action == null)
		{
			throw new ArgumentNullException("action");
		}
		_runOnceItems.Add(new RunOnceItem
		{
			Interval = timeSpan,
			Action = action
		});
	}

	private void CheckRunOnceItems()
	{
		foreach (RunOnceItem item in _runOnceItems)
		{
			int num = (int)item.Interval.TotalMinutes;
			if (_rnd.Next(1, num + 1) == 1)
			{
				_synCtx.Send(delegate
				{
					item.Action();
				}, null);
			}
		}
	}
}}
