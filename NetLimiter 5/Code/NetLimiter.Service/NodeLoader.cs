﻿using System;
using System.Collections.Generic;
using System.Threading;
using NLInterop;

namespace NetLimiter.Service{

public class NodeLoader : IDisposable
{
	private INLService _nlsvc;

	private NodeDataParser _parser = new NodeDataParser();

	private uint _maxTransferSize = 1048576u;

	private NodeDataHdr _hdr;

	private NodeLoaderProgressEventArgs _progressArgs = new NodeLoaderProgressEventArgs();

	private SynchronizationContext _synCtx;

	private int _transferRateTimeout = 5;

	private NodeDataGroup<TopNode, TopNodeEventArgs> _topGrp;

	private NodeDataGroup<ConnectionNode, ConnectionNodeEventArgs> _cnnGrp;

	private NodeDataGroup<InstanceNode, InstanceNodeEventArgs> _instGrp;

	private NodeDataGroup<ApplicationNode, ApplicationNodeEventArgs> _appGrp;

	private NodeDataGroup<FilterNode, FilterNodeEventArgs> _fltGrp;

	private bool _isBfeRunning = true;

	public NodeLoaderProgressEventArgs LastProgress => _progressArgs;

	public TopNode TopNode
	{
		get
		{
			if (_topGrp.Nodes._list.First != null)
			{
				return _topGrp.Nodes._list.First.Value;
			}
			return null;
		}
	}

	public NodeDataGroup<TopNode, TopNodeEventArgs> Tops => _topGrp;

	public NodeDataGroup<FilterNode, FilterNodeEventArgs> Filters => _fltGrp;

	public NodeDataGroup<ApplicationNode, ApplicationNodeEventArgs> Applications => _appGrp;

	public NodeDataGroup<InstanceNode, InstanceNodeEventArgs> Instances => _instGrp;

	public NodeDataGroup<ConnectionNode, ConnectionNodeEventArgs> Connections => _cnnGrp;

	public long SvcTickTime => _hdr.TickTime;

	public int TransferRateTimeout
	{
		get
		{
			return _transferRateTimeout;
		}
		set
		{
			_transferRateTimeout = value;
		}
	}

	internal NodeDataHdr Hdr => _hdr;

	public bool IsBfeRunning
	{
		get
		{
			return _isBfeRunning;
		}
		protected set
		{
			if (value != _isBfeRunning)
			{
				_isBfeRunning = value;
				if (this.IsBfeRunningChanged != null)
				{
					this.IsBfeRunningChanged(this, EventArgs.Empty);
				}
			}
		}
	}

	public uint MaxTransferSize
	{
		get
		{
			return _maxTransferSize;
		}
		set
		{
			_maxTransferSize = value;
			_hdr.MaxRetBufferSize = value;
		}
	}

	public event EventHandler<NodeLoaderProgressEventArgs> ProgressUpdated;

	public event EventHandler IsBfeRunningChanged;

	internal NodeLoader(INLService nlsvc, bool useSynCtx = true)
	{
		if (nlsvc == null)
		{
			throw new ArgumentNullException();
		}
		if (useSynCtx)
		{
			_synCtx = SynchronizationContext.Current;
		}
		if (_synCtx == null)
		{
			_synCtx = new SynchronizationContext();
		}
		InitHeader();
		_nlsvc = nlsvc;
	}

	public void Load()
	{
		if (_nlsvc != null)
		{
			while (!_hdr.IsComplete)
			{
				_synCtx.Send(LoadInternalProc, _nlsvc.GetNodeData(ref _hdr));
			}
			_hdr.Reset();
		}
	}

	private void LoadInternalProc(object state)
	{
		byte[] data = (byte[])state;
		if (TopNode == null)
		{
			_topGrp.Nodes.AddNode(new TopNode(this, _parser.GetTopNodeData(data)), null);
		}
		else
		{
			IsBfeRunning = _hdr.IsBfeRunning;
			TopNode.Update(this, _parser.GetTopNodeData(data));
		}
		Load<ApplicationNode, AppNodeDataWrapper, ApplicationNodeEventArgs>(_hdr.Apps, _parser.ParseAppData, data, CreateNode, _appGrp.Nodes);
		Load<InstanceNode, InstNodeDataWrapper, InstanceNodeEventArgs>(_hdr.Insts, _parser.ParseInstData, data, CreateNode, _instGrp.Nodes);
		Load<ConnectionNode, CnnNodeDataWrapper, ConnectionNodeEventArgs>(_hdr.Cnns, _parser.ParseCnnData, data, CreateNode, _cnnGrp.Nodes);
		Load<FilterNode, FltNodeDataWrapper, FilterNodeEventArgs>(_hdr.Flts, _parser.ParseFltData, data, CreateNode, _fltGrp.Nodes);
		_progressArgs.Update(_hdr);
		if (this.ProgressUpdated != null)
		{
			this.ProgressUpdated(this, _progressArgs);
		}
	}

	private void Load<TNode, TNodeData, TArgs>(NodeDataGrpIntr grp, ParserFun<TNodeData> parserFun, byte[] data, Func<TNodeData, TNode> createFun, NodeLoaderNodes<TNode, TArgs> nodes) where TNode : NLNode where TNodeData : NodeDataWrapper where TArgs : NLNodeEventArgs, new()
	{
		LinkedListNode<TNode> current = nodes._list.First;
		if (grp.RetNodeCnt != 0)
		{
			parserFun(data, delegate(TNodeData nodeData)
			{
				TNode val = null;
				while (current != null)
				{
					if (current.Value.Id < grp.CurIdPrev)
					{
						current = current.Next;
					}
					else
					{
						if (current.Value.Id >= nodeData.GetNodeId())
						{
							if (current.Value.Id == nodeData.GetNodeId())
							{
								val = current.Value;
								current = current.Next;
							}
							break;
						}
						LinkedListNode<TNode> node = current;
						current = current.Next;
						nodes.Remove(node);
					}
				}
				if (val == null)
				{
					nodes.AddNode(createFun(nodeData), current);
				}
				else
				{
					val.Update(this, nodeData);
				}
				return true;
			});
		}
		uint num = Math.Min(grp.TotalNodeCnt, grp.MaxRetNodeCnt);
		if (grp.IsComplete)
		{
			while (nodes._list.Count > num)
			{
				nodes.RemoveLast();
			}
		}
	}

	private ApplicationNode CreateNode(AppNodeDataWrapper data)
	{
		return new ApplicationNode(this, data);
	}

	private InstanceNode CreateNode(InstNodeDataWrapper data)
	{
		return new InstanceNode(this, data);
	}

	private ConnectionNode CreateNode(CnnNodeDataWrapper data)
	{
		return new ConnectionNode(this, data);
	}

	private FilterNode CreateNode(FltNodeDataWrapper data)
	{
		return new FilterNode(this, data);
	}

	public void Clear()
	{
		_cnnGrp.Nodes.Clear();
		_instGrp.Nodes.Clear();
		_appGrp.Nodes.Clear();
		_fltGrp.Nodes.Clear();
		_topGrp.Nodes.Clear();
	}

	public void Dispose()
	{
		if (_nlsvc != null)
		{
			Clear();
			_nlsvc = null;
		}
	}

	public uint GetTransferSizeOf(int fltCnt, int appCnt, int instCnt, int cnnCnt)
	{
		return _parser.GetSizeOf(fltCnt, appCnt, instCnt, cnnCnt);
	}

	public void SelectAllNodes(bool select = true)
	{
		_hdr.Flts.SelectAll(select);
		_hdr.Apps.SelectAll(select);
		_hdr.Insts.SelectAll(select);
		_hdr.Cnns.SelectAll(select);
	}

	private void InitHeader()
	{
		_hdr = new NodeDataHdr();
		_hdr.MaxRetBufferSize = _maxTransferSize;
		_cnnGrp = new NodeDataGroup<ConnectionNode, ConnectionNodeEventArgs>(this, (NodeLoader x) => x.Hdr.Cnns);
		_instGrp = new NodeDataGroup<InstanceNode, InstanceNodeEventArgs>(this, (NodeLoader x) => x.Hdr.Insts);
		_appGrp = new NodeDataGroup<ApplicationNode, ApplicationNodeEventArgs>(this, (NodeLoader x) => x.Hdr.Apps);
		_fltGrp = new NodeDataGroup<FilterNode, FilterNodeEventArgs>(this, (NodeLoader x) => x.Hdr.Flts);
		_topGrp = new NodeDataGroup<TopNode, TopNodeEventArgs>(this, (NodeLoader x) => x.Hdr.Tops);
		_cnnGrp.Grp.MaxRetNodeCnt = 400u;
	}

	public void SelectNode(NLNode node)
	{
		if (node != null)
		{
			if (node is ApplicationNode)
			{
				_hdr.Apps.NodeId = node.Id;
			}
			else if (node is InstanceNode)
			{
				_hdr.Insts.NodeId = node.Id;
			}
			else if (node is ConnectionNode)
			{
				_hdr.Cnns.NodeId = node.Id;
			}
			else if (node is FilterNode)
			{
				_hdr.Flts.NodeId = node.Id;
			}
		}
	}

	public void SelectChildren(NLNode parent)
	{
		if (parent == null || parent is TopNode)
		{
			_hdr.Apps.SelectAll();
		}
		if (parent is ApplicationNode)
		{
			_hdr.Insts.ParentId = parent.Id;
		}
		else if (parent is InstanceNode)
		{
			_hdr.Cnns.ParentId = parent.Id;
		}
	}
}}
