﻿using System;
using System.Runtime.Serialization;
using CoreLib;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class NLVersioned<T> where T : NLVersioned<T>
{
	private const int MaxIdLength = 38;

	[DataMember(Name = "Id")]
	internal string _id;

	[DataMember(Name = "Revision")]
	internal uint _revision;

	public string Id => _id;

	public uint Revision => _revision;

	protected NLVersioned()
	{
		InitId();
	}

	[OnDeserialized]
	private void OnDeserialized(StreamingContext context)
	{
		InitId();
	}

	internal void InitId()
	{
		if (_id != null)
		{
			if (_id.Length > 38)
			{
				throw new System.Exception("Id too long: " + _id);
			}
		}
		else
		{
			_id = Guid.NewGuid().ToString().ToLower();
		}
	}

	public virtual T Clone()
	{
		return (T)this.BinaryClone();
	}

	public T CloneAsNew()
	{
		T val = Clone();
		val.InitId();
		val._revision = 0u;
		return val;
	}

	public bool IsCloneOf(T other)
	{
		return ObjectCloner.IsBinaryClone(this, other);
	}
}}
