﻿using System.Collections.Generic;

namespace NetLimiter.Service{

public class StatsQueriesInProgressManager
{
	private List<StatsQueryInProgress> _queries = new List<StatsQueryInProgress>();

	public int QueryCount => _queries.Count;

	public bool QueryExists(string name)
	{
		lock (_queries)
		{
			foreach (StatsQueryInProgress query in _queries)
			{
				if (query.Name == name)
				{
					return true;
				}
			}
		}
		return false;
	}

	public bool AddQuery(string name)
	{
		lock (_queries)
		{
			if (QueryExists(name))
			{
				return false;
			}
			_queries.Add(new StatsQueryInProgress(name));
		}
		return true;
	}

	public bool RequestStopQuery(string name)
	{
		lock (_queries)
		{
			foreach (StatsQueryInProgress query in _queries)
			{
				if (query.Name == name)
				{
					query.StopRequest = true;
					return true;
				}
			}
		}
		return false;
	}

	public bool IsStopRequested(string name)
	{
		lock (_queries)
		{
			foreach (StatsQueryInProgress query in _queries)
			{
				if (query.Name == name)
				{
					return query.StopRequest;
				}
			}
		}
		return false;
	}

	public bool RemoveQuery(string name)
	{
		bool flag = false;
		lock (_queries)
		{
			for (int i = 0; i < _queries.Count; i++)
			{
				if (_queries[i].Name == name)
				{
					_queries[i] = null;
					flag = true;
				}
			}
		}
		if (flag)
		{
			_queries.RemoveAll((StatsQueryInProgress x) => x == null);
			return false;
		}
		return false;
	}
}}
