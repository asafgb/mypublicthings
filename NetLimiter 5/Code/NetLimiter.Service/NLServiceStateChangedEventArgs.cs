﻿using System;

namespace NetLimiter.Service{

internal class NLServiceStateChangedEventArgs : EventArgs
{
	public ServiceInfo ServiceInfo { get; set; }

	public NLServiceStateChangedEventArgs(ServiceInfo svcInfo = null)
	{
		ServiceInfo = svcInfo;
	}
}}
