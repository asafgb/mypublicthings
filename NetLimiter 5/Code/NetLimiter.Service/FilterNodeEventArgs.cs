﻿namespace NetLimiter.Service{

public class FilterNodeEventArgs : NLNodeEventArgs
{
	public new FilterNode Node
	{
		get
		{
			return base.Node as FilterNode;
		}
		internal set
		{
			base.Node = value;
		}
	}
}}
