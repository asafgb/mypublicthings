﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NetLimiter.Service{

public static class FilterExtensions
{
	public static bool GetAppFilterAppId(this Filter flt, out AppId appId)
	{
		if (flt != null && flt.PerType == FilterPerType.None && flt.Functions.Count == 1 && flt.Functions.First() is FFAppIdEqual { IsMatch: not false } fFAppIdEqual && fFAppIdEqual.Values.Count == 1)
		{
			appId = fFAppIdEqual.Values.First();
			return true;
		}
		appId = null;
		return false;
	}

	public static bool IsAppFilter(this Filter flt)
	{
		AppId appId;
		return flt.GetAppFilterAppId(out appId);
	}

	public static bool IsAppFilter(this Filter flt, AppId appId)
	{
		if (flt == null || appId == null)
		{
			return false;
		}
		if (flt.GetAppFilterAppId(out var appId2))
		{
			if (PathHelper.IsEqualVersionIgnore(appId.Path, appId2.Path))
			{
				return Sid.Equals(appId.Sid, appId2.Sid);
			}
			return false;
		}
		return false;
	}

	public static bool IsInPackage(this Filter flt, AppId appId)
	{
		if (flt.GetAppFilterAppId(out var appId2))
		{
			if (appId2.IsAppEmpty)
			{
				return Sid.Equals(appId2.Sid, appId.Sid);
			}
			return false;
		}
		return false;
	}

	public static bool TryGetAppPath(this Filter flt, out string path)
	{
		if (flt != null && flt.PerType == FilterPerType.None && flt.Functions.Count == 1 && flt.Functions.First() is FFPathEqual { IsMatch: not false } fFPathEqual && fFPathEqual.Values.Count == 1)
		{
			path = fFPathEqual.Values.FirstOrDefault();
			return true;
		}
		path = null;
		return false;
	}

	public static string CreateFilterNameFromPath(string path)
	{
		string text = path;
		try
		{
			text = Path.GetFileName(path);
			if (path.Contains("x86"))
			{
				text += " (x86)";
			}
		}
		catch
		{
		}
		return text;
	}

	public static bool IsTagFilter(this Filter flt)
	{
		if (flt.PerType == FilterPerType.None && flt.Functions.Count >= 1)
		{
			if (flt.Functions.Any((FilterFunction x) => !(x is FFTagEqual)))
			{
				return false;
			}
			return true;
		}
		return false;
	}

	public static List<string> GetTagList(this Filter flt)
	{
		List<string> list = new List<string>();
		foreach (FilterFunction item in flt.Functions.FindAll((FilterFunction x) => x is FFTagEqual))
		{
			if (!(item is FFTagEqual fFTagEqual))
			{
				continue;
			}
			foreach (AppTagFilterValue v in fFTagEqual.Values)
			{
				if (!list.Any((string x) => x == v))
				{
					list.Add(v);
				}
			}
		}
		if (list.Count <= 0)
		{
			return null;
		}
		return list;
	}

	public static void RemoveFilterTagsFromList(this Filter flt, ref List<string> tl)
	{
		foreach (FilterFunction item in flt.Functions.FindAll((FilterFunction x) => x is FFTagEqual))
		{
			if (!(item is FFTagEqual fFTagEqual))
			{
				continue;
			}
			foreach (AppTagFilterValue value in fFTagEqual.Values)
			{
				tl.Remove(value);
			}
		}
	}

	internal static bool IsDead(this Filter filter)
	{
		if (filter is StorePackageFilter storePackageFilter)
		{
			try
			{
				if (storePackageFilter.PackageFolder == null || !Directory.Exists(storePackageFilter.PackageFolder))
				{
					return true;
				}
				return false;
			}
			catch
			{
				return true;
			}
		}
		if (filter.GetAppFilterAppId(out var appId))
		{
			try
			{
				if (!appId.IsAppEmpty && !appId.IsWild && AppInfo.GetIsDead(appId))
				{
					return true;
				}
				return false;
			}
			catch
			{
			}
		}
		return false;
	}

	public static bool IsNtwFilter(this Filter flt, Network ntw)
	{
		if (flt.IsNtwFilter(out var ntwIx))
		{
			return ntw.Index == ntwIx;
		}
		return false;
	}

	public static bool IsNtwFilter(this Filter flt, out int ntwIx)
	{
		FilterFunCollection functions = flt.Functions;
		if (functions != null && functions.Count == 1 && flt.Functions.First() is FFNetworkIxEquals { IsMatch: not false } fFNetworkIxEquals && fFNetworkIxEquals.Values.Count == 1)
		{
			ntwIx = fFNetworkIxEquals.Values.First();
			return true;
		}
		ntwIx = 0;
		return false;
	}
}}
