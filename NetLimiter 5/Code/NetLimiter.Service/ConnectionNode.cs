﻿using System;
using System.Linq;
using System.Net.Sockets;
using CoreLib.Net;
using NLInterop;

namespace NetLimiter.Service{

public class ConnectionNode : NLNode
{
	public IPAddress LocalAddress { get; protected set; }

	public IPAddress RemoteAddress { get; protected set; }

	public ushort LocalPort { get; protected set; }

	public ushort RemotePort { get; protected set; }

	public ConnectionType ConnectionType { get; protected set; }

	public ProtocolType Protocol { get; protected set; }

	public string DnsName { get; protected set; }

	public new InstanceNode Parent
	{
		get
		{
			return base.Parent as InstanceNode;
		}
		private set
		{
			base.Parent = value;
		}
	}

	public event EventHandler<ConnectionNodeEventArgs> Updated;

	public event EventHandler<ConnectionNodeEventArgs> Removed;

	internal ConnectionNode(NodeLoader ldr, CnnNodeDataWrapper data)
		: base(data)
	{
		Parent = ldr.Instances.Nodes.FirstOrDefault((InstanceNode x) => x.Id == data.GetParentId());
		ConnectionType = (ConnectionType)data.GetCnnType();
		Protocol = (ProtocolType)data.GetProtocol();
		DnsName = data.GetDnsName();
		switch (data.GetAddrFamily())
		{
		case AddressFamily.InterNetwork:
			LocalAddress = new IPAddress4(data.GetLocalAddr4(), ByteOrder.Host);
			RemoteAddress = new IPAddress4(data.GetRemoteAddr4(), ByteOrder.Host);
			LocalPort = data.GetLocalPort();
			RemotePort = data.GetRemotePort();
			break;
		case AddressFamily.InterNetworkV6:
			LocalAddress = new IPAddress6(data.GetLAddrWords6(), ByteOrder.Host);
			RemoteAddress = new IPAddress6(data.GetRAddrWords6(), ByteOrder.Host);
			LocalPort = data.GetLocalPort();
			RemotePort = data.GetRemotePort();
			break;
		}
		Update(ldr, data);
	}

	internal override void Update(NodeLoader ldr, NodeDataWrapper data)
	{
		CnnNodeDataWrapper obj = (CnnNodeDataWrapper)data;
		Update(ldr, data, ldr.Connections.Nodes._args);
		if (obj.IsClosed() != base.IsClosed)
		{
			SetClosed();
			ldr.Connections.Nodes._args.Flags |= NLNodeChangeFlags.Closed;
		}
		string dnsName = obj.GetDnsName();
		if (DnsName == null && dnsName != null)
		{
			DnsName = dnsName;
			ldr.Connections.Nodes._args.Flags |= NLNodeChangeFlags.TotalOutTimeout;
		}
		ldr.Connections.Nodes.OnNodeUpdated();
		this.Updated?.Invoke(this, ldr.Connections.Nodes._args);
	}

	internal override void OnRemoved(NodeLoader ldr)
	{
		this.Removed?.Invoke(this, ldr.Connections.Nodes._args);
	}
}}
