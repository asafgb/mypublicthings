﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2ResultDataPort
{
	[DataMember]
	public ulong In;

	[DataMember]
	public ulong Out;

	[DataMember]
	public ushort Port;
}}
