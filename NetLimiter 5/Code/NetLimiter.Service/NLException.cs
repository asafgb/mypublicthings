﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
public class NLException : Exception
{
	public NLException()
	{
	}

	public NLException(string msg, Exception inner = null)
		: base(msg, inner)
	{
	}

	public NLException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
