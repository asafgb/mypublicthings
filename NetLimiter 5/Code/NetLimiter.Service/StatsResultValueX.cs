﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

public class StatsResultValueX
{
	[Serializable]
	[DataContract]
	public class StatsResultValueXDescription
	{
		[DataMember]
		public string DescString;

		[DataMember]
		public ushort Port;

		[DataMember]
		public ushort PortLocal;

		[DataMember]
		public StatsIPVersion IpVer;

		[DataMember]
		public uint Index;

		[DataMember]
		public ushort AppId;

		[DataMember]
		private byte[] Ip;

		[DataMember]
		private byte[] IpLocal;

		public uint IPv4
		{
			get
			{
				if (IpVer != 0)
				{
					return 0u;
				}
				if (Ip == null)
				{
					Ip = new byte[4];
				}
				return (uint)((Ip[0] << 24) | (Ip[1] << 16) | (Ip[2] << 8) | Ip[3]);
			}
			set
			{
				if (IpVer == StatsIPVersion.IPV4)
				{
					if (Ip == null)
					{
						Ip = new byte[4];
					}
					Ip[3] = (byte)(value & 0xFFu);
					Ip[2] = (byte)((value >> 8) & 0xFFu);
					Ip[1] = (byte)((value >> 16) & 0xFFu);
					Ip[0] = (byte)((value >> 24) & 0xFFu);
				}
			}
		}

		public uint IPv4Local
		{
			get
			{
				if (IpVer != 0)
				{
					return 0u;
				}
				if (IpLocal == null)
				{
					IpLocal = new byte[4];
				}
				return (uint)((IpLocal[0] << 24) | (IpLocal[1] << 16) | (IpLocal[2] << 8) | IpLocal[3]);
			}
			set
			{
				if (IpVer == StatsIPVersion.IPV4)
				{
					if (IpLocal == null)
					{
						IpLocal = new byte[4];
					}
					IpLocal[3] = (byte)(value & 0xFFu);
					IpLocal[2] = (byte)((value >> 8) & 0xFFu);
					IpLocal[1] = (byte)((value >> 16) & 0xFFu);
					IpLocal[0] = (byte)((value >> 24) & 0xFFu);
				}
			}
		}

		public byte[] IPv6
		{
			get
			{
				if (IpVer != StatsIPVersion.IPV6)
				{
					return null;
				}
				if (Ip == null)
				{
					Ip = new byte[16];
				}
				return Ip;
			}
		}

		public byte[] IPv6Local
		{
			get
			{
				if (IpVer != StatsIPVersion.IPV6)
				{
					return null;
				}
				if (IpLocal == null)
				{
					IpLocal = new byte[16];
				}
				return IpLocal;
			}
		}

		public StatsResultValueXDescription(StatsIPVersion ipVer)
		{
			IpVer = ipVer;
		}
	}

	public DateTime Start;

	public DateTime End;

	public StatsResultValueXDescription Description;
}}
