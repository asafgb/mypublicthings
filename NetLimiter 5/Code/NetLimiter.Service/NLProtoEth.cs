﻿using System;

namespace NetLimiter.Service{

[Serializable]
public enum NLProtoEth
{
	NLProtoEthIPv4 = 2048,
	NLProtoEthIPv6 = 34525
}}
