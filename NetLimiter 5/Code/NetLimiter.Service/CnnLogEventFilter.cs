﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class CnnLogEventFilter
{
	[DataMember]
	public string Name { get; set; }

	[DataMember]
	public DateTime Time { get; set; }
}}
