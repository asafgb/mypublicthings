﻿using System;

namespace NetLimiter.Service{

public class NLNodeEventArgs : EventArgs
{
	public NLNodeChangeFlags Flags;

	public NLNode Node { get; internal set; }

	internal void Init(NLNode node)
	{
		Node = node;
		Flags = (NLNodeChangeFlags)0;
	}
}}
