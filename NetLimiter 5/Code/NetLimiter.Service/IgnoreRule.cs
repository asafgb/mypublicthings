﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class IgnoreRule : Rule
{
	[DataMember]
	public bool IgnoreData { get; set; }

	[DataMember]
	public bool IgnoreLimit { get; set; }

	[DataMember]
	public new RuleDir Dir
	{
		get
		{
			return base.Dir;
		}
		set
		{
			base.Dir = value;
		}
	}

	public IgnoreRule(RuleDir dir)
		: base(dir)
	{
	}
}}
