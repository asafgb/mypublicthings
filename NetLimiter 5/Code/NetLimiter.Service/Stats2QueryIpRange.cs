﻿using System.Runtime.Serialization;
using CoreLib.Net;

namespace NetLimiter.Service{

[DataContract]
public class Stats2QueryIpRange
{
	[DataMember]
	public IPRange Range;

	[DataMember]
	public bool IsMatch = true;
}}
