﻿using System;
using NLInterop;

namespace NetLimiter.Service{

public class TopNode : NLNode
{
	public event EventHandler<TopNodeEventArgs> Updated;

	public event EventHandler<TopNodeEventArgs> Removed;

	internal TopNode(NodeLoader ldr, TopNodeDataWrapper data)
		: base(data)
	{
		Update(ldr, data);
	}

	internal override void Update(NodeLoader ldr, NodeDataWrapper data)
	{
		Update(ldr, data, ldr.Tops.Nodes._args);
		ldr.Tops.Nodes.OnNodeUpdated();
		if (this.Updated != null)
		{
			this.Updated(this, ldr.Tops.Nodes._args);
		}
	}

	internal override void OnRemoved(NodeLoader ldr)
	{
		if (this.Removed != null)
		{
			this.Removed(this, ldr.Tops.Nodes._args);
		}
	}
}}
