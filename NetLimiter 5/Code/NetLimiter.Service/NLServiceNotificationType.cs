﻿namespace NetLimiter.Service{

internal enum NLServiceNotificationType
{
	RuleOrderChanged,
	NetworkChange,
	FirewallRequest,
	QuotaChange,
	BfeStateChange,
	PriSettingsChanged,
	NotificationsChanged
}}
