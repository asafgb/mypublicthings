﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using LicenseKeeper.Models;
using NetLimiter.Service.Api;
using NetLimiter.Service.AppList;
using NetLimiter.Service.Notifications;
using NetLimiter.Service.Security;
using NLInfoTools;

namespace NetLimiter.Service{

[ServiceContract(Namespace = "NetLimiter.Service", CallbackContract = typeof(INLServiceCallback), SessionMode = SessionMode.Required)]
internal interface INLService
{
	List<Filter> Filters
	{
		[OperationContract]
		get;
	}

	List<Rule> Rules
	{
		[OperationContract]
		get;
	}

	List<AppInfo> Apps
	{
		[OperationContract]
		get;
	}

	bool UseTcpBinding
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	string TcpAddress
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	ushort TcpPort
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	bool UseNpBinding
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	string Ip2LocFolderName
	{
		[OperationContract]
		get;
	}

	string SettingsFolderName
	{
		[OperationContract]
		get;
	}

	string SettingsFileName
	{
		[OperationContract]
		get;
	}

	string StatsFolderName
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	string StatsFolderNameNL4
	{
		[OperationContract]
		get;
	}

	bool StatsSaveInternet
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	bool StatsSaveLocal
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	bool StatsSaveLoopback
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	int StatsUpdateTime
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	bool IsStatsEnabled
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	List<FirewallRequest> FirewallRequests
	{
		[OperationContract]
		get;
	}

	List<Network> Networks
	{
		[OperationContract]
		get;
	}

	bool IsFwEnabled
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	bool IsLimiterEnabled
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	bool IsPriorityEnabled
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	NLLicense License
	{
		[OperationContract]
		get;
	}

	string LocalhostFootprint
	{
		[OperationContract]
		get;
	}

	bool IsBfeRunning
	{
		[OperationContract]
		get;
	}

	PrioritySettings PrioritySettings
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	bool RunClientOnFwRequest
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	bool UseVTFileChecks
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	string VTAPIKey
	{
		[OperationContract]
		get;
		[OperationContract]
		set;
	}

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	ServiceInfo Open();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	Filter AddFilter(Filter flt);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	FilterUpdateInfo UpdateFilter(Filter flt);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void RemoveFilter(string fltId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void RemoveFilterDeep(string fltId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	byte[] GetNodeData(ref NodeDataHdr hdr);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	StatsResultList StatsQuery(Stats2Query query);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	StatsResultList StatsGetQueryResults(string queryName);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void StatsStopQuery(string queryName);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	StatsDbFileInfo StatsGetDbFileInfo();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void Stats2GetDbInfo();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool StatsIntegrityCheck(string alternateDbPath = null);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool StatsRepairData(StatsBadRowsListInfo badDataSegments);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool StatsDeleteData(List<long> appList);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool Stats2DeleteDataByTime(DateTime start, DateTime end);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool Stats2ImportNL4Database(string location);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool StatsDeleteDatabase();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool StatsEnableCompression(bool enableCompression);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool StatsMergeData(List<long> appList, long mergeApp);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool StatsCompactData(DateTime start, DateTime end, uint interval);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	List<string> GetStatsApps();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	DateTime GetStatsFirstRecordTime();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool ChangeStatsDataLocation(string newLocation, bool moveFiles);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	string InstallIp2LocDB(string path);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	Ip2LocVersion GetIp2LocVersion();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	Location GetIpLocation(IPAddress ip);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	VTFileReport GetVTReport(string filePath);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool RemoveApp(AppId appId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void ReplyFirewallRequest(ulong fwrId, FwRequestReply reply);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void ApproveNetwork(string ntwId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void RemoveNetwork(string ntwId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	Rule AddRule(string filterId, Rule rule);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	RuleUpdateInfo UpdateRule(Rule rule);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void RemoveRule(string ruleId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void ResetNodeTotals(ulong nodeId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	ChartData GetChart(ulong nodeId, NLFlowDir? dir = null);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	ChartAppCutData GetChartAppCut(DateTime time);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	IEnumerable<NetLimiter.Service.Api.AccessTableRow> GetAccessTable();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void SetAccessTable(IEnumerable<NetLimiter.Service.Api.AccessTableRow> table);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	string ResolveIdentity(string userOrGroupName);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void RestartCommunication();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	NLLicense SetRegistrationData(RegData regData);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	NLLicense RemoveRegistrationData();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	Network UpdateNetwork(Network network);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	List<UserInfo> GetUserList();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool IsAppInUse(AppId appId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void KillCnns(ulong nodeId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void KillCnnsByFilter(string filterId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void KillCnnsByQuota(string ruleId, long quotaPtr = 0L);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void SetRuleTable(IEnumerable<string> rules);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void ResetQuota(string ruleId, long quotaPtr = 0L, bool stop = false);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	List<QuotaState> GetQuotaStates();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	QuotaStateDetails GetQuotaStateDetails(string quotaRuleId, int ix);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void SetDefaultFwAction(FwAction fwaIn, FwAction fwaOut);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void GetDefaultFwAction(out FwAction fwaIn, out FwAction fwaOut);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void InitQuotaTotalFromStats(string quotaRuleId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	List<AppListSvcItem> GetInstalledServices();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	List<AppListStoreItem> GetInstalledStoreApps();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	SidName GetSidName(Sid sid);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	AppInfo GetAppInfo(AppId appId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void UpdateAppInfo(AppId appId, AppInfo appInfo);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	Filter GetFilterByInternalId(uint id);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	List<AppId> GetRemovedApps();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	CnnLogEventLists GetCnnLog(CnnLogEventFilter filter);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	List<string> GetDeadFilters();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void CheckAccess(Rights rights);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	bool CheckElevationRequired();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void Elevate(string clientId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	ClientInfo GetClientInfo(string clientId);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void SubscribeAsFwReqeustHandler(bool subscribe);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	List<ServiceNotification> GetNotifications();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void ClearNotifications();

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	void MuteNotifications(string[] notifIds);

	[OperationContract]
	[FaultContract(typeof(NLFaultContract))]
	string GetHWCodeHash();
}}
