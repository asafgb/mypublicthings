﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class StatsIntegrityCheckResult
{
	[DataMember]
	public ulong DataOut;

	[DataMember]
	public ulong DataIn;

	[DataMember]
	public ulong DataInOH;

	[DataMember]
	public ulong DataOutOH;

	[DataMember]
	public uint RowNum;

	[DataMember]
	public uint ErrFlags;
}}
