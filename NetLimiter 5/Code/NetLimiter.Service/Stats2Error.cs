﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2Error
{
	[DataMember]
	public uint ErrFlags;

	[DataMember]
	public uint WinErrCode;

	[DataMember]
	public string ErrMessage;

	public Stats2Error(uint c, uint wc, string msg)
	{
		ErrFlags = c;
		WinErrCode = wc;
		ErrMessage = msg;
	}
}}
