﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFProcessIdEqual : FilterFunctionT<ulong>
{
	public FFProcessIdEqual()
	{
	}

	public FFProcessIdEqual(ulong id)
	{
		base.Values.Add(id);
	}
}}
