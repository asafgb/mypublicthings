﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;
using NetLimiter.Service.Security;

namespace NetLimiter.Service{

[Serializable]
public class AccessDeniedException : NLException
{
	public bool RemoteAccess { get; set; }

	public AccessDeniedException()
		: base(string.Format(Langs.MsgAccessDenied))
	{
	}

	internal AccessDeniedException(Rights right, string userName)
		: base(string.Format(Langs.MsgAccessDeniedRightUser, right.ToString(), userName))
	{
	}

	public AccessDeniedException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
