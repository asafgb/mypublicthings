﻿using System;
using System.Linq;
using NLInterop;

namespace NetLimiter.Service{

public class InstanceNode : NLNode
{
	public uint ProcessId { get; private set; }

	public new ApplicationNode Parent
	{
		get
		{
			return base.Parent as ApplicationNode;
		}
		private set
		{
			base.Parent = value;
		}
	}

	public event EventHandler<InstanceNodeEventArgs> Updated;

	public event EventHandler<InstanceNodeEventArgs> Removed;

	internal InstanceNode(NodeLoader ldr, InstNodeDataWrapper data)
		: base(data)
	{
		ProcessId = data.GetProcessId();
		Parent = ldr.Applications.Nodes.FirstOrDefault((ApplicationNode x) => x.Id == data.GetParentId());
		Update(ldr, data);
	}

	internal override void Update(NodeLoader ldr, NodeDataWrapper data)
	{
		InstNodeDataWrapper obj = (InstNodeDataWrapper)data;
		Update(ldr, data, ldr.Instances.Nodes._args);
		if (obj.IsClosed() != base.IsClosed)
		{
			SetClosed();
			ldr.Instances.Nodes._args.Flags |= NLNodeChangeFlags.Closed;
		}
		ldr.Applications.Nodes.OnNodeUpdated();
		if (this.Updated != null)
		{
			this.Updated(this, ldr.Instances.Nodes._args);
		}
	}

	internal override void OnRemoved(NodeLoader ldr)
	{
		if (this.Removed != null)
		{
			this.Removed(this, ldr.Instances.Nodes._args);
		}
	}
}}
