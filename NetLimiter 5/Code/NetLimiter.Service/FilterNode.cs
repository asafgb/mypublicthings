﻿using System;
using NLInterop;

namespace NetLimiter.Service{

public class FilterNode : NLNode
{
	public new TopNode Parent
	{
		get
		{
			return base.Parent as TopNode;
		}
		private set
		{
			base.Parent = value;
		}
	}

	public uint FilterId { get; private set; }

	public event EventHandler<FilterNodeEventArgs> Updated;

	public event EventHandler<FilterNodeEventArgs> Removed;

	internal FilterNode(NodeLoader ldr, FltNodeDataWrapper data)
		: base(data)
	{
		Parent = ldr.TopNode;
		FilterId = data.GetFilterId();
		Update(ldr, data);
	}

	internal override void Update(NodeLoader ldr, NodeDataWrapper data)
	{
		Update(ldr, data, ldr.Filters.Nodes._args);
		ldr.Filters.Nodes.OnNodeUpdated();
		if (this.Updated != null)
		{
			this.Updated(this, ldr.Filters.Nodes._args);
		}
	}

	internal override void OnRemoved(NodeLoader ldr)
	{
		if (this.Removed != null)
		{
			this.Removed(this, ldr.Filters.Nodes._args);
		}
	}
}}
