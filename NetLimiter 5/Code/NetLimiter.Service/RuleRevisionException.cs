﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class RuleRevisionException : NLException
{
	public RuleRevisionException(Rule rule)
		: base(string.Format(Langs.MsgRuleRevision, rule.Id))
	{
	}

	public RuleRevisionException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
