﻿namespace NetLimiter.Service{

public enum StatsTimeStep
{
	Minutes10,
	Minutes15,
	Minutes20,
	Minutes30,
	Hour,
	Day,
	Week,
	Month,
	Year,
	Custom,
	Full
}}
