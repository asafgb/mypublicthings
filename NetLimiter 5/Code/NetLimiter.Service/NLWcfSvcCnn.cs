﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Microsoft.Extensions.Logging;

namespace NetLimiter.Service{

internal class NLWcfSvcCnn : INLSvcCnn, IDisposable
{
	private static readonly ILogger<NLWcfSvcCnn> _logger = Logging.LoggerFactory.CreateLogger<NLWcfSvcCnn>();

	private DuplexChannelFactory<INLService> _factory;

	private volatile INLService _svc;

	private volatile NLServiceState _state;

	private volatile Exception _connectError;

	public Exception ConnectError => _connectError;

	public INLService Svc => _svc;

	public string SvcAddress
	{
		get
		{
			lock (this)
			{
				IClientChannel clientChannel = (IClientChannel)_svc;
				return (clientChannel.RemoteAddress == null) ? null : clientChannel.RemoteAddress.ToString();
			}
		}
	}

	public NLServiceState State => _state;

	public event EventHandler<NLServiceStateChangedEventArgs> StateChanged;

	public NLWcfSvcCnn(INLServiceCallback callback, string hostName = null, ushort port = 0, string userName = null, SecureString password = null)
	{
		_logger.LogInformation("hostName={0}, port={1}, userName={2}", hostName, port, userName);
		if (callback == null)
		{
			throw new ArgumentNullException("callback");
		}
		if (hostName == null || string.Compare(hostName, "localhost", ignoreCase: true) == 0)
		{
			string npUri = NLWcf.GetNpUri();
			_logger.LogInformation("Using named pipe: pipe={0}", npUri);
			NetNamedPipeBinding binding = new NetNamedPipeBinding
			{
				Security = 
				{
					Mode = NetNamedPipeSecurityMode.Transport
				},
				ReceiveTimeout = TimeSpan.MaxValue,
				MaxReceivedMessageSize = 10485760L
			};
			EndpointAddress addr = new EndpointAddress(npUri);
			InitFactory(callback, binding, addr);
		}
		else
		{
			_logger.LogInformation("Using tcp");
			InitFactory(callback, new NetTcpBinding
			{
				Security = 
				{
					Mode = SecurityMode.Transport
				},
				ReceiveTimeout = TimeSpan.MaxValue,
				MaxReceivedMessageSize = 10485760L
			}, new EndpointAddress(NLWcf.GetTcpUri(hostName, (ushort)((port == 0) ? 4045 : port))));
		}
		_factory.Credentials.Windows.ClientCredential.UserName = userName;
		if (password != null)
		{
			_logger.LogInformation("Password is set");
			IntPtr intPtr = Marshal.SecureStringToGlobalAllocUnicode(password);
			try
			{
				_factory.Credentials.Windows.ClientCredential.Password = Marshal.PtrToStringUni(intPtr);
			}
			finally
			{
				Marshal.ZeroFreeGlobalAllocUnicode(intPtr);
			}
		}
		_svc = _factory.CreateChannel();
		ICommunicationObject obj = (ICommunicationObject)_svc;
		obj.Faulted += ComObj_Disconnected;
		obj.Closed += ComObj_Disconnected;
	}

	private void ComObj_Disconnected(object sender, EventArgs e)
	{
		_logger.LogInformation("Comm object disconnected: closing the connection");
		lock (this)
		{
			if (_state != NLServiceState.Disconnecting && _state != NLServiceState.Connected)
			{
				return;
			}
			_state = NLServiceState.Disconnected;
		}
		OnStateChanged();
		_state = NLServiceState.Initial;
		OnStateChanged();
	}

	public ServiceInfo Connect()
	{
		_logger.LogTrace(">>");
		_connectError = null;
		lock (this)
		{
			if (State != 0)
			{
				throw new NLException("Connect can be called in initial state only");
			}
			_logger.LogDebug("Connecting...");
			_state = NLServiceState.Connecting;
		}
		OnStateChanged();
		_ = (ICommunicationObject)_svc;
		try
		{
			_logger.LogDebug("Try to open the service");
			ServiceInfo serviceInfo = _svc.Open();
			_logger.LogDebug("Service opened: {0}", serviceInfo.HostName);
			_state = NLServiceState.Connected;
			OnStateChanged(serviceInfo);
			return serviceInfo;
		}
		catch (Exception ex)
		{
			_logger.LogError(ex, "Failed to connect to the service");
			_connectError = ex;
			_state = NLServiceState.Disconnected;
			OnStateChanged();
			Close();
			_state = NLServiceState.Initial;
			OnStateChanged();
			throw ex;
		}
		finally
		{
			_logger.LogTrace("<<");
		}
	}

	private void InitFactory(INLServiceCallback callback, Binding binding, EndpointAddress addr)
	{
		InstanceContext callbackInstance = new InstanceContext(callback);
		_factory = new DuplexChannelFactory<INLService>(callbackInstance, binding, addr);
		_factory.Credentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
	}

	public void Close()
	{
		lock (this)
		{
			if (_state != NLServiceState.Connected)
			{
				return;
			}
			_state = NLServiceState.Disconnecting;
		}
		OnStateChanged();
		ICommunicationObject communicationObject = (ICommunicationObject)_svc;
		try
		{
			communicationObject.Close();
		}
		catch
		{
			try
			{
				communicationObject.Abort();
			}
			catch
			{
			}
		}
	}

	private void OnStateChanged(ServiceInfo svcInfo = null)
	{
		if (this.StateChanged != null)
		{
			this.StateChanged(this, new NLServiceStateChangedEventArgs(svcInfo));
		}
	}

	public void Dispose()
	{
		Close();
	}
}}
