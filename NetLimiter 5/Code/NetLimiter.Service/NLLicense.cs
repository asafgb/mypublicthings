﻿using System;
using System.Runtime.Serialization;
using System.Threading;
using LicenseKeeper.Models;
using NLInterop;

namespace NetLimiter.Service{

[DataContract]
public class NLLicense
{
	public const int ExtendedPeriodDays = 14;

	[DataMember]
	public string EditionId { get; set; }

	[DataMember]
	public int Quantity { get; set; }

	[DataMember]
	public string LicenseType { get; set; }

	[DataMember]
	public string PlanId { get; set; }

	[DataMember]
	public bool IsRecurring { get; set; }

	[DataMember]
	public DateTime Expiration { get; set; }

	[DataMember]
	public bool IsTestingVersion { get; set; }

	[DataMember]
	public bool IsCancelled { get; set; }

	[DataMember]
	public bool IsRegistered { get; set; }

	[DataMember]
	public string RegName { get; set; }

	[DataMember]
	public string RegCodeHash { get; set; }

	[DataMember]
	public SupportedFeatures SupporetedFeatures { get; set; }

	public bool IsExpired => DaysLeft == 0;

	public bool IsExtendedExpired
	{
		get
		{
			if (!IsRegistered)
			{
				return IsExpired;
			}
			return ExtendedDaysLeft == 0;
		}
	}

	public int DaysLeftRaw => (int)Math.Ceiling((Expiration - DateTime.UtcNow).TotalDays);

	public int DaysLeft => Math.Max(0, DaysLeftRaw);

	public DateTime ExtendedExpiration
	{
		get
		{
			if (!IsRegistered || IsTestingVersion)
			{
				return Expiration;
			}
			return Expiration.AddDays(14.0);
		}
	}

	public int ExtendedDaysLeft => (int)Math.Max(0.0, Math.Ceiling((ExtendedExpiration - DateTime.UtcNow).TotalDays));

	public NLLicense()
	{
			Thread.Sleep(10000);
		Expiration = DateTime.MaxValue;
		EditionId = "pro";
		Quantity = 100;
		IsRegistered = false;
		IsRecurring = false;
		SupporetedFeatures = new SupportedFeatures(EditionId);
		InitTestingVersion();
	}

	public NLLicense(DateTime expiration)
		: this()
	{
			// First edit to Max
            Expiration = DateTime.MaxValue;
            InitTestingVersion();
	}

	public NLLicense(RegData regData)
	{
        Expiration = regData.EndTime;
		PlanId = regData.PlanId;
		EditionId = regData.EditionId;
		Quantity = Math.Max(1, regData.Quantity);
		LicenseType = regData.LicenseId;
		IsRegistered = true;
		IsCancelled = regData.IsCancelled;
		SupporetedFeatures = new SupportedFeatures(EditionId);
		RegName = regData.RegName;
		RegCodeHash = regData.RegCodeHash;
		IsRecurring = regData.IsRecurring;
		InitTestingVersion();
	}

	private void InitTestingVersion()
	{
		IsTestingVersion = false;
		if (IsTestingVersion && Expiration < NLEnv.TestingExpiration)
		{
			Expiration = NLEnv.TestingExpiration;
		}
		
	}
		private void SetRenew()
		{
			
		}
	}
}
