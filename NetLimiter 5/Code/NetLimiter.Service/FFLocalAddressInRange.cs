﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFLocalAddressInRange : FilterFunctionT<IPRangeFilterValue>
{
	public FFLocalAddressInRange()
	{
	}

	public FFLocalAddressInRange(IPRangeFilterValue range)
	{
		base.Values.Add(range);
	}
}}
