﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FilterFunctionT<T> : FilterFunction
{
	[DataMember(Name = "Values")]
	private FilterValueCollection<T> _values = new FilterValueCollection<T>();

	public FilterValueCollection<T> Values => _values;
}}
