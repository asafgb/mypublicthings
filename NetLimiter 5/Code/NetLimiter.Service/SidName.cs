﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class SidName
{
	[DataMember]
	public string Name { get; set; }
}}
