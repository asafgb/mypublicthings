﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public abstract class PackageFilter : Filter
{
	[DataMember]
	public Sid Sid { get; protected set; }

	protected PackageFilter(Sid sid)
	{
		_type = FilterType.Filter;
		Init();
		Sid = sid;
		new AppId(null, sid);
		base.Functions.Add(new FFAppIdEqual(new AppId(null, sid)));
	}
}}
