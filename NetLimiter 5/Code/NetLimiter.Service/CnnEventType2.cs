﻿namespace NetLimiter.Service{

public enum CnnEventType2
{
	NoEvent,
	ConnectAllowed,
	ConnectAllowedByRule,
	ConnectDenied,
	AcceptAllowedByRule,
	AcceptDenied,
	ListenAllowedByRule,
	ListenAllowed,
	ListenDenied,
	Close
}}
