﻿using System;

namespace NetLimiter.Service{

[Serializable]
public enum FwAction
{
	None,
	Ask,
	Allow,
	Deny,
	Block
}}
