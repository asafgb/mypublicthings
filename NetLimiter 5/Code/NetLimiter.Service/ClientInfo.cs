﻿namespace NetLimiter.Service{

public class ClientInfo
{
	public string AppName { get; set; }

	public string AppPath { get; set; }

	public bool IsElevationRequired { get; set; }
}}
