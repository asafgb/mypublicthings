﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class ExpiredException : NLException
{
	public ExpiredException(NLLicense lic)
		: base(GetMessage(lic))
	{
	}

	public ExpiredException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}

	private static string GetMessage(NLLicense lic)
	{
		if (lic.IsTestingVersion)
		{
			return Langs.MsgTestingExpired;
		}
		return Langs.MsgTrialExpired;
	}
}}
