﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract(Name = "rule")]
[KnownType(typeof(LimitRule))]
[KnownType(typeof(FwRule))]
[KnownType(typeof(PriorityRule))]
[KnownType(typeof(QuotaRule))]
[KnownType(typeof(IgnoreRule))]
public abstract class Rule : NLVersioned<Rule>
{
	[NonSerialized]
	internal uint _internalFltId;

	[DataMember]
	public uint InternalId { get; internal set; }

	[DataMember]
	public string FilterId { get; internal set; }

	[DataMember]
	public RuleDir Dir { get; protected set; }

	[DataMember]
	public bool IsEnabled { get; set; }

	[DataMember]
	public int Weight { get; set; }

	[DataMember]
	public DateTime CreatedTime { get; set; }

	[DataMember]
	public DateTime UpdatedTime { get; set; }

	[DataMember]
	public bool IsActive { get; internal set; }

	public RuleState State
	{
		get
		{
			if (!IsEnabled)
			{
				return RuleState.Disabled;
			}
			if (!IsActive)
			{
				return RuleState.Inactive;
			}
			return RuleState.Active;
		}
	}

	public bool IsPredefined
	{
		get
		{
			if (string.Compare(base.Id, "518A6EE0-FF49-4515-A804-53E110F45AAD") != 0)
			{
				return string.Compare(base.Id, "DDFACCF7-9CA5-4C81-B9EE-4788B1274597") == 0;
			}
			return true;
		}
	}

	[DataMember]
	public List<RuleCondition> Conditions { get; protected set; }

	public static int MaxRuleWeight => int.MaxValue;

	protected Rule(RuleDir dir)
	{
		Init(dir);
	}

	protected void Init(RuleDir dir)
	{
		Init();
		IsEnabled = true;
		Dir = dir;
	}

	protected void Init()
	{
		if (CreatedTime == default(DateTime) || UpdatedTime == default(DateTime))
		{
			DateTime createdTime = (UpdatedTime = DateTime.UtcNow);
			CreatedTime = createdTime;
		}
		if (Conditions == null)
		{
			Conditions = new List<RuleCondition>();
		}
		if (Weight > MaxRuleWeight)
		{
			Weight = MaxRuleWeight;
		}
		if (Weight < 0)
		{
			Weight = 0;
		}
	}

	[OnDeserialized]
	private void OnDeserialized(StreamingContext streamContext)
	{
		Init();
	}
}}
