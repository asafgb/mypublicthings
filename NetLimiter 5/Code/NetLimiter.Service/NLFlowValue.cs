﻿using System;

namespace NetLimiter.Service{

public class NLFlowValue<T> : INLFlowValue<T>
{
	private T[] _value = new T[2];

	public T this[NLFlowDir dir]
	{
		get
		{
			return this[(int)dir];
		}
		set
		{
			this[(int)dir] = value;
		}
	}

	public T this[int dir]
	{
		get
		{
			if (dir < 0 || dir > 1)
			{
				throw new ArgumentException("Direction must be eq. to 1 or 2");
			}
			return _value[dir];
		}
		set
		{
			if (dir < 0 || dir > 1)
			{
				throw new ArgumentException("Direction must be eq. to 1 or 2");
			}
			_value[dir] = value;
		}
	}

	public T In
	{
		get
		{
			return this[0];
		}
		set
		{
			this[0] = value;
		}
	}

	public T Out
	{
		get
		{
			return this[1];
		}
		set
		{
			this[1] = value;
		}
	}
}}
