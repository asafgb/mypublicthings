﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class InvalidRegNameOrCodeException : NLException
{
	internal InvalidRegNameOrCodeException()
		: base(Langs.MsgInvalidRegNameOrCode)
	{
	}

	public InvalidRegNameOrCodeException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
