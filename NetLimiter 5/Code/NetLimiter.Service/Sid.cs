﻿using System;
using System.Runtime.Serialization;
using NLInterop;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class Sid : IEquatable<Sid>
{
	[DataMember]
	public byte[] Bytes { get; protected set; }

	public bool IsServiceSid => AppIdHelper.IsServiceSid(Bytes);

	public bool IsStoreAppSid => AppIdHelper.IsStoreAppSid(Bytes);

	public bool IsUserSid => AppIdHelper.IsUserSid(Bytes);

	protected Sid(byte[] bytes)
	{
		Bytes = bytes;
	}

	public static Sid FromBytes(byte[] bytes)
	{
		if (bytes != null && bytes.Length != 0 && bytes[0] != 0)
		{
			return new Sid(bytes);
		}
		return null;
	}

	public static Sid FromString(string sid)
	{
		return FromBytes(AppIdHelper.StringSidToBytes(sid));
	}

	public static Sid FromServiceName(string svcName)
	{
		try
		{
			return FromBytes(AppIdHelper.ServiceNameToSid(svcName));
		}
		catch
		{
			return null;
		}
	}

	public static bool operator ==(Sid sid1, Sid sid2)
	{
		return sid1?.Equals(sid2) ?? ((object)sid2 == null);
	}

	public static bool operator !=(Sid sid1, Sid sid2)
	{
		return !(sid1 == sid2);
	}

	public override bool Equals(object obj)
	{
		if (!(obj is Sid other))
		{
			return false;
		}
		return Equals(other);
	}

	public override int GetHashCode()
	{
		return Bytes.GetHashCode();
	}

	public bool Equals(Sid other)
	{
		if ((object)other == null || Bytes.Length != other.Bytes.Length)
		{
			return false;
		}
		for (int i = 0; i < Bytes.Length; i++)
		{
			if (Bytes[i] != other.Bytes[i])
			{
				return false;
			}
		}
		return true;
	}

	public static bool Equals(Sid sid1, Sid sid2)
	{
		if (sid1 == null)
		{
			return sid2 == null;
		}
		if (sid2 == null)
		{
			return sid1 == null;
		}
		return sid1.Equals(sid2);
	}

	public override string ToString()
	{
		return AppIdHelper.BytesToStringSid(Bytes);
	}
}}
