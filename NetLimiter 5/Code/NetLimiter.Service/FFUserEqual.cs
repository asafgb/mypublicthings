﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFUserEqual : FilterFunctionT<Sid>
{
	public FFUserEqual()
	{
	}

	public FFUserEqual(Sid sid)
	{
		base.Values.Add(sid);
	}
}}
