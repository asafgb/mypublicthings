﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract(Name = "limitRule")]
public class LimitRule : Rule
{
	[DataMember]
	public new RuleDir Dir
	{
		get
		{
			return base.Dir;
		}
		set
		{
			base.Dir = value;
		}
	}

	[DataMember(Name = "limitSize")]
	public uint LimitSize { get; set; }

	public LimitRule(RuleDir dir, uint limitSize)
		: base(dir)
	{
		Init(limitSize);
	}

	private void Init(uint limitSize)
	{
		LimitSize = limitSize;
	}
}}
