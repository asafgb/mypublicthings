﻿using System;
using NetLimiter.Properties;

namespace NetLimiter.Service{

public class SvcClosedException : Exception
{
	public SvcClosedException()
		: base(Langs.MsgSvcClosed)
	{
	}
}}
