﻿namespace NetLimiter.Service{

public enum PriorityReactionSpeedType
{
	Low,
	Normal,
	Fast,
	Custom
}}
