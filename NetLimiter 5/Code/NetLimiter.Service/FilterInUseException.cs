﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class FilterInUseException : NLException
{
	public FilterInUseException(Filter flt)
		: base(string.Format(Langs.MsgFilterInUse, flt.Name))
	{
	}

	public FilterInUseException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
