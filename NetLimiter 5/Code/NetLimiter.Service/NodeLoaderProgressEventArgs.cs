﻿namespace NetLimiter.Service{

public class NodeLoaderProgressEventArgs
{
	public uint BytesLoaded { get; internal set; }

	public uint BytesToLoad { get; internal set; }

	public bool IsComplete { get; internal set; }

	internal void Update(NodeDataHdr hdr)
	{
		BytesLoaded = hdr.Transferred;
		BytesToLoad = hdr.TotalSize;
		IsComplete = hdr.IsComplete;
	}
}}
