﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public enum StatsQueryResultType
{
	[EnumMember]
	Time,
	[EnumMember]
	Apps,
	[EnumMember]
	IPs,
	[EnumMember]
	IPsbyApps,
	[EnumMember]
	IPsbyAppsbyPorts,
	[EnumMember]
	ConnectionTotals,
	[EnumMember]
	Users,
	[EnumMember]
	Countries,
	[EnumMember]
	Histogram24Hours,
	[EnumMember]
	Ports
}}
