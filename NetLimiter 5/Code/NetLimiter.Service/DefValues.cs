﻿namespace NetLimiter.Service{

internal class DefValues
{
	public const int MaxWfcRcvSizeNp = 10485760;

	public const int MaxWfcRcvSizeTcp = 10485760;

	public const int MaxNodeDataSize = 1048576;

	public const int MaxLoadCnnCount = 400;
}}
