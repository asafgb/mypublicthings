﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class RuleUpdateInfo
{
	[DataMember]
	public RuleUpdateFlags Flags { get; set; }

	[DataMember]
	public Rule Rule { get; private set; }

	[DataMember]
	public bool HasChange { get; set; }

	public RuleUpdateInfo(Rule rule, bool hasChange)
	{
		if (rule == null)
		{
			throw new ArgumentNullException();
		}
		Rule = rule;
		HasChange = hasChange;
	}
}}
