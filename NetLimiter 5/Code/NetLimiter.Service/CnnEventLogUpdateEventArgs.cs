﻿using System.Collections.Generic;

namespace NetLimiter.Service{

public class CnnEventLogUpdateEventArgs
{
	public List<CnnLogEvent> Events { get; set; }
}}
