﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class RuleNotFoundException : NLException
{
	public RuleNotFoundException(string ruleId)
		: base(string.Format(Langs.MsgRuleNotFound, ruleId))
	{
	}

	public RuleNotFoundException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
