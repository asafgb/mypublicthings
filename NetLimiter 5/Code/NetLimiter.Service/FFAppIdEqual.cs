﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFAppIdEqual : FilterFunctionT<AppId>
{
	public FFAppIdEqual()
	{
	}

	public FFAppIdEqual(AppId id)
	{
		base.Values.Add(id);
	}
}}
