﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Win32;
using NLInterop;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class UserInfo
{
	[DataMember]
	public string Name { get; set; }

	[DataMember]
	public Sid Sid { get; set; }

	public static List<UserInfo> GetLocalUsers()
	{
		List<UserInfo> list = new List<UserInfo>();
		using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\ProfileList"))
		{
			if (registryKey != null)
			{
				string[] subKeyNames = registryKey.GetSubKeyNames();
				foreach (string text in subKeyNames)
				{
					try
					{
						Sid sid = Sid.FromString(text);
						string text2 = AppIdHelper.GetSidName(sid.Bytes);
						if (text2 == null)
						{
							text2 = text;
						}
						list.Add(new UserInfo
						{
							Sid = sid,
							Name = text2
						});
					}
					catch
					{
					}
				}
			}
		}
		return list;
	}
}}
