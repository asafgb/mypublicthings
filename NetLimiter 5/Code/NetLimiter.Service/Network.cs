﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
[KnownType(typeof(WiFiNetwork))]
public class Network
{
	public const int IanaIfTypeEthernet = 6;

	public const int IanaIfTypeWiFi = 71;

	public const int IanaIfTypePPP = 23;

	[DataMember]
	public string Id { get; protected set; }

	[DataMember]
	public string SystemId { get; protected set; }

	[DataMember]
	public ushort Index { get; set; }

	[DataMember]
	public uint IanaIfType { get; set; }

	[DataMember]
	public NetworkCompareFlags CompareFlags { get; set; }

	[DataMember]
	public DateTime TimeCreated { get; set; }

	[DataMember]
	public string InterfaceName { get; set; }

	[DataMember]
	public string ConnectionName { get; set; }

	[DataMember]
	public string MacAddress { get; set; }

	[DataMember]
	public string RouterMacAddress { get; set; }

	[DataMember]
	public string WifiSsid { get; set; }

	[DataMember]
	public string WifiBssid { get; set; }

	[DataMember]
	public bool IsApproved { get; set; }

	[DataMember]
	public bool IsActive { get; set; }

	[DataMember]
	public string RouterIPAddress { get; set; }

	public Network(uint ianaIfType, string systemId, ushort index, string ifcName, string cnnName, string macAddress, string routerMac, string wifiBssid, string wifiSsid)
	{
		Id = Guid.NewGuid().ToString();
		CompareFlags = NetworkCompareFlags.Ssid | NetworkCompareFlags.IfcName;
		TimeCreated = DateTime.UtcNow;
		IanaIfType = ianaIfType;
		SystemId = systemId;
		Index = index;
		InterfaceName = ifcName;
		ConnectionName = cnnName;
		MacAddress = macAddress;
		RouterMacAddress = routerMac;
		WifiSsid = ((wifiSsid == null) ? "" : wifiSsid);
		WifiBssid = ((wifiBssid == null) ? "" : wifiBssid);
	}

	public virtual bool IsEqual(uint ianaIfType, string ifcName, string systemId, string macAddress, string routerMacAddress, string wifiBssid, string wifiSsid)
	{
		if (IanaIfType != ianaIfType)
		{
			return false;
		}
		if (CompareFlags.HasFlag(NetworkCompareFlags.IfcName) && !string.Equals(InterfaceName, ifcName, StringComparison.InvariantCultureIgnoreCase))
		{
			return false;
		}
		if (CompareFlags.HasFlag(NetworkCompareFlags.Bssid) && !string.Equals(WifiBssid, wifiBssid, StringComparison.InvariantCultureIgnoreCase))
		{
			return false;
		}
		if (CompareFlags.HasFlag(NetworkCompareFlags.Ssid) && !string.Equals(WifiSsid, wifiSsid, StringComparison.InvariantCultureIgnoreCase))
		{
			return false;
		}
		if (CompareFlags.HasFlag(NetworkCompareFlags.RouterMac) && !string.Equals(RouterMacAddress, routerMacAddress, StringComparison.InvariantCultureIgnoreCase))
		{
			return false;
		}
		if (CompareFlags.HasFlag(NetworkCompareFlags.Mac) && !string.Equals(MacAddress, macAddress, StringComparison.InvariantCultureIgnoreCase))
		{
			return false;
		}
		return true;
	}
}}
