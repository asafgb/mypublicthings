﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using NetLimiter.Service.AppList;
using NLInterop;

namespace NetLimiter.Service{

internal class NLSvcHelper
{
	public static void SetFilterId(Rule rule, string fltId)
	{
		rule.FilterId = fltId;
	}

	public static void SetInternalFilterId(Rule rule, uint fltId)
	{
		rule._internalFltId = fltId;
	}

	public static uint GetInternalFilterId(Rule rule)
	{
		return rule._internalFltId;
	}

	public static void SetInternalId(Filter filter, uint id)
	{
		filter._internalId = id;
	}

	public static uint GetInternalId(Filter filter)
	{
		return filter._internalId;
	}

	public static void SetIsActive(Rule rule, bool value)
	{
		rule.IsActive = value;
	}

	public static SidName GetSidName(Sid sid)
	{
		SidName sidName = new SidName();
		try
		{
			if (sid.IsServiceSid)
			{
				sidName.Name = AppIdHelper.GetSvcNameFromSid(sid.Bytes);
			}
			else if (sid.IsStoreAppSid)
			{
				sidName.Name = StoreAppHelper.GetStoreApp(sid)?.Name;
			}
			else
			{
				sidName.Name = AppIdHelper.GetSidName(sid.Bytes);
			}
		}
		catch
		{
		}
		if (string.IsNullOrEmpty(sidName.Name))
		{
			sidName.Name = AppIdHelper.BytesToStringSid(sid.Bytes);
		}
		return sidName;
	}

	public static void InitFilterName(Filter flt)
	{
		bool flag = false;
		if (!string.IsNullOrEmpty(flt.Name))
		{
			if (!flt.Name.Contains("{appid_name}"))
			{
				return;
			}
			flag = true;
		}
		FFAppIdEqual fFAppIdEqual = flt.Functions.OfType<FFAppIdEqual>().FirstOrDefault((FFAppIdEqual x) => x.Values.Any());
		string text = null;
		if (fFAppIdEqual != null)
		{
			AppId appId = fFAppIdEqual.Values[0];
			if (!appId.IsSidEmpty)
			{
				try
				{
					text = GetSidName(appId.Sid).Name;
					if (text != null && appId.Sid.IsStoreAppSid)
					{
						text = text.Split('_')[0];
					}
				}
				catch
				{
				}
			}
			try
			{
				if (text == null && File.Exists(appId.Path))
				{
					FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(appId.Path);
					if (!string.IsNullOrWhiteSpace(versionInfo.FileDescription))
					{
						text = versionInfo.FileDescription;
					}
				}
			}
			catch
			{
			}
			try
			{
				text = text ?? PathHelper.GetFileName(appId.Path);
			}
			catch
			{
			}
			text = text ?? "nullapp";
			flt.Name = (flag ? flt.Name.Replace("{appid_name}", text) : text);
		}
		if (string.IsNullOrEmpty(flt.Name))
		{
			flt.Name = Guid.NewGuid().ToString();
		}
	}
}}
