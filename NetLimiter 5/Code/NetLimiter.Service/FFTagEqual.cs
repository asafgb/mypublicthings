﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFTagEqual : FilterFunctionT<AppTagFilterValue>
{
	public FFTagEqual()
	{
	}

	public FFTagEqual(string tag)
	{
		base.Values.Add(tag);
	}
}}
