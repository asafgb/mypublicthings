﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2Results
{
	[DataMember]
	public List<Stats2ResultDataTime> TimePeriods = new List<Stats2ResultDataTime>();

	[DataMember]
	public List<Stats2ResultDataApp> Apps = new List<Stats2ResultDataApp>();

	[DataMember]
	public List<Stats2ResultDataUser> Users = new List<Stats2ResultDataUser>();

	[DataMember]
	public List<Stats2ResultDataPort> Ports = new List<Stats2ResultDataPort>();

	[DataMember]
	public List<Stats2ResultDataAppIpPort> AppIpPorts = new List<Stats2ResultDataAppIpPort>();

	[DataMember]
	public List<Stats2Ip> IpLookUpTable = new List<Stats2Ip>();

	[DataMember]
	public ulong TotalIn;

	[DataMember]
	public ulong TotalOut;
}}
