﻿using System;

namespace NetLimiter.Service{

[Serializable]
public enum NLFlowDir
{
	In,
	Out
}}
