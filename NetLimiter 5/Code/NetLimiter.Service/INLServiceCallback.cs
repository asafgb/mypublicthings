﻿using System.ServiceModel;

namespace NetLimiter.Service{

[ServiceContract(Namespace = "NetLimiter.Service")]
internal interface INLServiceCallback
{
	[OperationContract(IsOneWay = true)]
	void OnFilterAdded(Filter filter);

	[OperationContract(IsOneWay = true)]
	void OnFilterRemoved(string fltId);

	[OperationContract(IsOneWay = true)]
	void OnFilterUpdated(FilterUpdateInfo info);

	[OperationContract(IsOneWay = true)]
	void OnAppUpdated(AppInfo info);

	[OperationContract(IsOneWay = true)]
	void OnAppAdded(AppInfo info);

	[OperationContract(IsOneWay = true)]
	void OnAppRemoved(AppId appId);

	[OperationContract(IsOneWay = true)]
	void OnRuleAdded(Rule rule);

	[OperationContract(IsOneWay = true)]
	void OnRuleRemoved(string ruleId);

	[OperationContract(IsOneWay = true)]
	void OnRuleUpdated(RuleUpdateInfo info);

	[OperationContract(IsOneWay = true)]
	void OnOptionsChange(OptionsChangeType type, object value);

	[OperationContract(IsOneWay = true)]
	void OnLicenseChange(NLLicense license);

	[OperationContract(IsOneWay = true)]
	void OnStatsToolsCompleted(StatsToolsResult result);

	[OperationContract(IsOneWay = true)]
	void OnStatsQueryUpdate(StatsQueryUpdateEventArgs args);

	[OperationContract(IsOneWay = true)]
	void OnVTReport(VTReportEventArgs args);

	[OperationContract(IsOneWay = true)]
	void OnNotification(NLServiceNotificationType type);

	[OperationContract(IsOneWay = true)]
	void OnCnnEventLogUpdate(CnnEventLogUpdateEventArgs args);
}}
