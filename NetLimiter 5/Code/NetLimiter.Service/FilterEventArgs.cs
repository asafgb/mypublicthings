﻿using System;

namespace NetLimiter.Service{

public class FilterEventArgs : EventArgs
{
	public Filter Filter { get; internal set; }

	public FilterEventArgs(Filter filter)
	{
		Filter = filter;
	}
}}
