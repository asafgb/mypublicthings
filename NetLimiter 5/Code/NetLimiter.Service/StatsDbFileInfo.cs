﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class StatsDbFileInfo
{
	[DataMember]
	public ulong FileSizeV4;

	[DataMember]
	public ulong FileSizeV6;

	[DataMember]
	public bool CmpEnabledV4;

	[DataMember]
	public bool CmpEnabledV6;

	[DataMember]
	public uint ErrorV4;

	[DataMember]
	public uint ErrorV6;
}}
