﻿namespace NetLimiter.Service{

public enum StatsQueryError
{
	None,
	ResultTypeNotSpecified,
	Unknown
}}
