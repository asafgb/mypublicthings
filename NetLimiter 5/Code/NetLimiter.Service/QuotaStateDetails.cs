﻿using System;
using System.Runtime.Serialization;
using CoreLib.Net;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class QuotaStateDetails : QuotaState
{
	[DataMember]
	public ulong NodeId;

	[DataMember]
	public long QuotaPtr;

	[DataMember]
	public string FilterId;

	[DataMember]
	public ConnectionType ConnectionType;

	[DataMember]
	public int ProtoIp;

	[DataMember]
	public IPAddress LocalAddress;

	[DataMember]
	public IPAddress RemoteAddress;

	[DataMember]
	public ushort LocalPort;

	[DataMember]
	public ushort RemotePort;

	[DataMember]
	public string ApplicationPath;

	[DataMember]
	public uint ProcessId;

	[DataMember]
	public int Ix;
}}
