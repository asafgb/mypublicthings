﻿using System;

namespace NetLimiter.Service{

public class RuleUpdateEventArgs : EventArgs
{
	public Rule Rule { get; protected set; }

	public Rule OldRule { get; protected set; }

	public RuleUpdateInfo Info { get; protected set; }

	public RuleUpdateEventArgs(RuleUpdateInfo info, Rule oldRule)
	{
		Rule = info.Rule;
		OldRule = oldRule;
		Info = info;
	}
}}
