﻿namespace NetLimiter.Service{

public enum StatsToolsAction
{
	NoAction,
	CheckIntegrity,
	CompactDb,
	RemoveData,
	RemoveDataByTime,
	MergeData,
	RepairData,
	DeleteDatabase,
	EnableCompression,
	GetDbFileInfo,
	ImportNL4DB,
	ChangeDBLocation
}}
