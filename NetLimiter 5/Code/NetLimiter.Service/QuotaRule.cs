﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class QuotaRule : Rule
{
	[DataMember]
	public new RuleDir Dir
	{
		get
		{
			return base.Dir;
		}
		set
		{
			base.Dir = value;
		}
	}

	public long Total { get; set; }

	[DataMember]
	public long Quota { get; set; }

	[DataMember]
	public List<string> OnOverflowRules { get; protected set; }

	[DataMember]
	public bool ShowAlertWindow { get; set; }

	[DataMember]
	public bool RunActions { get; set; }

	[DataMember]
	public bool SendEmail { get; set; }

	[DataMember]
	public string EmeailRcps { get; set; }

	[DataMember]
	public bool IsOverflow { get; set; }

	[DataMember]
	public bool IsAnswered { get; set; }

	public QuotaRule(RuleDir dir, long quota)
		: base(dir)
	{
		ShowAlertWindow = true;
		Quota = quota;
		InitInternal();
	}

	[OnDeserialized]
	private void OnDeserialized(StreamingContext ctx)
	{
		InitInternal();
	}

	private void InitInternal()
	{
		OnOverflowRules = OnOverflowRules ?? new List<string>();
	}
}}
