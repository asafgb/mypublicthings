﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class FilterAlreadyExistsException : NLException
{
	public FilterAlreadyExistsException(Filter flt)
		: base(string.Format(Langs.MsgFilterAlreadyExists, flt.Name, flt.Id))
	{
	}

	public FilterAlreadyExistsException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
