﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
internal class ServiceInfo
{
	[DataMember]
	public int SettingsVersion;

	[DataMember]
	public string HostName;

	[DataMember]
	public bool HasPendingFwr;

	[DataMember]
	public NLLicense License;

	[DataMember]
	public string LocalhostFootprint;

	[DataMember]
	public bool IsBfeRunning;

	[DataMember]
	public string OSVersion;

	[DataMember]
	public string ClientId;

	[DataMember]
	public string SvcInstPath;

	[DataMember]
	public bool IsElevationRequired;

	[DataMember]
	public string ServiceVersion;
}}
