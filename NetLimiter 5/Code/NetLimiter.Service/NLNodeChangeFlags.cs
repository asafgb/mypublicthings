﻿using System;

namespace NetLimiter.Service{

[Flags]
public enum NLNodeChangeFlags
{
	Closed = 1,
	RateInChanged = 2,
	RateOutChanged = 4,
	TotalInChanged = 8,
	TotalOutChanged = 0x10,
	TotalInTimeout = 0x20,
	TotalOutTimeout = 0x40,
	NameChanged = 0x40
}}
