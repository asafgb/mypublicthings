﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Runtime.CompilerServices;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Linq;
using CoreLib;
using CoreLib.Process;
using LicenseKeeper.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Win32;
using NetLimiter.DnsCache;
using NetLimiter.Service.Api;
using NetLimiter.Service.AppList;
using NetLimiter.Service.Comm;
using NetLimiter.Service.Driver;
using NetLimiter.Service.Notifications;
using NetLimiter.Service.RuleScheduler;
using NetLimiter.Service.Security;
using Newtonsoft.Json;

namespace NetLimiter.Service{

internal class NLServiceTemp : INLServiceCallback
{
	private enum CheckLicenseExpirationType
	{
		NoReload,
		ReloadIfExpired,
		ReloadAlways
	}

	private string _licPubKey = "eyJEIjpudWxsLCJEUCI6bnVsbCwiRFEiOm51bGwsIkV4cG9uZW50IjoiQVFBQiIsIkludmVyc2VRIjpudWxsLCJNb2R1bHVzIjoieUdkVm9kY2hpMDU0cVQwcEVBc2dRRTJId3hPTTIwSXZKQ3hUc0xyck1WdU9BSG8yT2dMVlk3SVB0NDBRUlBWa254aUJVdk1MQ0hGYlBjNXVDbndqRnFhM3UwZHRHbC93bXR6aVcwVUpWR29KRHhFUFAwS3gxdzVCS1k4b0p2SG1mVkMwM1dNVWZMZElDUEtrdmpPSlB5UXdsNmtUWWZTUDhCUDNOS3RFWG1yV1RmUnlseTdkVVhyV2Q2Ujh1QU5wTDNSVmJOVStiZXYyT1ZMUlAycVMzYkVsZjZPSDVVQ2owQi9LZEtwWExuRXFYZXFUMjBOaWpiV3ZzRnYyZDhnRC9ERFBibG9SNWx2L3hxWDd3RVhJNFNSSS9KWU9LNTRqcWh1aTZsRWdkYXBuUk8yVWwwbElPM1l0clhWWHh6L01OdEdtcERSWUQ1ejBFcGFPQ21PVXJRPT0iLCJQIjpudWxsLCJRIjpudWxsfQ==";

	private const int RightsVersion = 2;

	private const string ClientExeName = "nlclientapp.exe";

	public const string LicenseFileName = "license.json";

	private static readonly ILogger<NLServiceTemp> _logger = Logging.LoggerFactory.CreateLogger<NLServiceTemp>();

	private System.Timers.Timer _settingsTimer;

	private System.Timers.Timer _fwrTimer;

	private SecurityManager _securityManager;

	private INLService _service;

	private INLDriver _driver;

	private uint _lastFwrCnt;

	private bool _started;

	private bool _fwrTimerRunning;

	private INLCommService _comm;

	internal SynchronizationContext _synCtx;

	private DnsWmiCache _dnsCache;

	private CancellationTokenSource _reloadLicenseCancel = new CancellationTokenSource();

	private uint _nextRuleId;

	private string _dataPath;

	private bool _isBfeRunning = true;

	public TimerManager TimerManager { get; } = new TimerManager();


	public NLSvcSettings Settings { get; protected set; }

	public NetLimiter.Service.RuleScheduler.RuleScheduler RuleScheduler { get; protected set; }

	public NLLicense License { get; protected set; }

	public string LocalhostFootprint { get; protected set; }

	public bool RunClientOnFwRequest
	{
		get
		{
			return Settings.RunClientOnFwRequest;
		}
		set
		{
			_logger.LogInformation("RunClientOnFwRequest setter: value={value}", value);
			CheckAccess(Rights.Control);
			CheckFeature(ProductFeatures.Blocker);
			Settings.RunClientOnFwRequest = value;
			OnSettingsChanged();
		}
	}

	public bool IsBfeRunning
	{
		get
		{
			return _isBfeRunning;
		}
		set
		{
			if (_isBfeRunning != value)
			{
				_logger.LogInformation("BFE state change: running={bfeState}", _isBfeRunning);
				_isBfeRunning = value;
				if (value)
				{
					RemoveNotification("notifid-BFE-not-running");
				}
				else
				{
					AddNotification(NotificationSeverity.High, "BFE not running", "Windows Base Filtering Engine (BFE) has stopped. NetLimiter can't work without BFE.", "notifid-BFE-not-running", "https://www.netlimiter.com/docs/netlimiter-overview/system-requirements/base-filtering-engine", null, isManaged: true);
				}
				Callback.OnNotification(NLServiceNotificationType.BfeStateChange);
			}
		}
	}

	public INLServiceCallback Callback => this;

	public NLServiceTemp(INLDriver driver)
	{
		_service = (INLService)driver;
		_driver = driver ?? throw new ArgumentNullException("driver");
		_synCtx = SynchronizationContext.Current;
		if (_synCtx == null)
		{
			throw new InvalidOperationException("Synchronization context must be set");
		}
		RuleScheduler = new NetLimiter.Service.RuleScheduler.RuleScheduler();
	}

	internal void Start(NLSvcSettings settings)
	{
		CheckMainThread();
		if (settings == null)
		{
			LoadSettings();
		}
		else
		{
			Settings = settings;
		}
		InitLocalhostFootprint();
		_started = true;
		MessageLoop.TimeChanged += MessageLoop_TimeChanged;
		MessageLoop.PowerModeChanged += MessageLoop_PowerModeChanged;
		TimerManager timerManager = TimerManager;
		timerManager.NewDayDetected = (EventHandler)Delegate.Combine(timerManager.NewDayDetected, new EventHandler(OnNewDayTimer));
		TimerManager.Start();
		_driver.AppAdded += _driver_AppAdded;
		_driver.FwrCountChanged += _driver_FwrCountChanged;
		_driver.QuotaOverflow += _driver_QuotaOverflow;
		_driver.RuleChanged += _driver_RuleChanged;
		_driver.BfeChanged += _driver_BfeChanged;
		InitLicense();
		CheckLicenseExpiration(CheckLicenseExpirationType.ReloadIfExpired);
		TimerManager.RunRandomOncePerInternval(OnReloadLicense, TimeSpan.FromDays(7.0));
	}

	public void OnDriverStarted()
	{
		CheckMainThread();
		if (Settings.UseDnsFiltering)
		{
			_dnsCache = new DnsWmiCache();
			_dnsCache.DnsItemsRemoved += OnDnsCacheItemsRemoved;
			_dnsCache.DnsItemsAdded += OnDnsCacheItemsAdded;
			_dnsCache.Start(TimeSpan.FromSeconds(10.0), 100);
		}
	}

	internal void Stop()
	{
		CheckMainThread();
		if (_dnsCache != null)
		{
			_dnsCache.Stop();
			_dnsCache.DnsItemsRemoved -= OnDnsCacheItemsRemoved;
			_dnsCache.DnsItemsAdded -= OnDnsCacheItemsAdded;
			_dnsCache = null;
		}
		_driver.AppAdded -= _driver_AppAdded;
		_driver.FwrCountChanged -= _driver_FwrCountChanged;
		_driver.QuotaOverflow -= _driver_QuotaOverflow;
		_driver.RuleChanged -= _driver_RuleChanged;
		_driver.BfeChanged -= _driver_BfeChanged;
		TimerManager.Stop();
		MessageLoop.TimeChanged -= MessageLoop_TimeChanged;
		MessageLoop.PowerModeChanged -= MessageLoop_PowerModeChanged;
		if (!_started)
		{
			return;
		}
		_started = false;
		_fwrTimerRunning = false;
		_fwrTimer?.Stop();
		if (_settingsTimer != null)
		{
			_settingsTimer.Dispose();
			_settingsTimer = null;
		}
		try
		{
			SaveSettings();
		}
		catch (System.Exception exception)
		{
			_logger.LogError(exception, "Failed to save settings");
		}
	}

	private RegistryKey GetRightsKey()
	{
		IdentityReference user = WindowsIdentity.GetCurrent().User;
		string value = user.Value;
		using RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);
		using RegistryKey registryKey2 = registryKey.CreateSubKey("Software\\Locktime Software\\NetLimiter\\5\\Rights");
		RegistryKey registryKey3 = registryKey2.OpenSubKey(value, writable: true);
		if (registryKey3 != null)
		{
			if (!ResetOldRights(registryKey2, registryKey3, value))
			{
				return registryKey3;
			}
			registryKey3 = null;
		}
		RegistrySecurity registrySecurity = new RegistrySecurity();
		SecurityIdentifier identity = new SecurityIdentifier(WellKnownSidType.BuiltinAdministratorsSid, null);
		SecurityIdentifier identity2 = new SecurityIdentifier(WellKnownSidType.BuiltinUsersSid, null);
		registrySecurity.SetOwner(user);
		registrySecurity.AddAccessRule(new RegistryAccessRule(user, RegistryRights.FullControl, AccessControlType.Allow));
		registrySecurity.AddAccessRule(new RegistryAccessRule(identity, RegistryRights.ExecuteKey, AccessControlType.Allow));
		registrySecurity.AddAccessRule(new RegistryAccessRule(identity2, RegistryRights.ExecuteKey, AccessControlType.Allow));
		registryKey3 = registryKey2.CreateSubKey(value, RegistryKeyPermissionCheck.Default, registrySecurity);
		registryKey3.SetValue("Version", 2);
		return registryKey3;
	}

	private bool ResetOldRights(RegistryKey rightsKey, RegistryKey rightsUserKey, string userId)
	{
		if ((int)rightsUserKey.GetValue("Version", 0) < 2)
		{
			string name = rightsUserKey.Name;
			rightsUserKey.Dispose();
			rightsKey.DeleteSubKeyTree(name, throwOnMissingSubKey: false);
			AddNotification(NotificationSeverity.High, "Permissions reset", "Permission table has been reset");
			return true;
		}
		return false;
	}

	private List<NetLimiter.Service.Api.AccessTableRow> LoadAccessTable()
	{
		try
		{
			using RegistryKey registryKey = GetRightsKey();
			if (registryKey.GetValue("Value", null) is string value)
			{
				return DCSerializer.FromString<List<NetLimiter.Service.Api.AccessTableRow>>(value);
			}
		}
		catch (System.Exception exception)
		{
			_logger.LogWarning(exception, "Failed to load permission table");
		}
		_logger.LogInformation("Creating default permission table");
		List<NetLimiter.Service.Api.AccessTableRow> list = SecurityManager.CreateDefaultAccessTable();
		try
		{
			SaveAccessTable(list);
		}
		catch (System.Exception exception2)
		{
			_logger.LogError(exception2, "Failed to save permission table");
		}
		return list;
	}

	private void SaveAccessTable(List<NetLimiter.Service.Api.AccessTableRow> table)
	{
		using RegistryKey registryKey = GetRightsKey();
		string value = DCSerializer.ToString(table);
		registryKey.SetValue("Value", value);
	}

	private void _driver_AppAdded(object sender, AppAddedEventArgs e)
	{
		SynPost(delegate
		{
			AddAppPath(e.AppId);
		}, "_driver_AppAdded");
	}

	private void _driver_FwrCountChanged(object sender, FwrCountChangedEventArgs e)
	{
		SynPost(delegate
		{
			OnFwrCountChanged(e.Count);
		}, "_driver_FwrCountChanged");
	}

	private void _driver_QuotaOverflow(object sender, QuotaOverflowEventArgs e)
	{
		SynPost(delegate
		{
			OnQuotaOverflow(e.RuleId, e.OverflowCount);
		}, "_driver_QuotaOverflow");
	}

	private void _driver_RuleChanged(object sender, RuleChangedEventArgs e)
	{
		SynPost(delegate
		{
			OnRuleChanged(e.RuleId, e.IsEnabled);
		}, "_driver_RuleChanged");
	}

	private void _driver_BfeChanged(object sender, BfeChangedEventArgs e)
	{
		SynPost(delegate
		{
			IsBfeRunning = e.IsRunning;
		}, "_driver_BfeChanged");
	}

	private void MessageLoop_PowerModeChanged(object sender, PowerModeEventArgs args)
	{
		SynSend(delegate
		{
			OnPowerModeChanged(args.ChangeType);
		}, "MessageLoop_PowerModeChanged");
	}

	private void MessageLoop_TimeChanged(object sender, EventArgs e)
	{
		TimerManager.Reset();
	}

	private void OnPowerModeChanged(PowerModeChangeType changeType)
	{
		switch (changeType)
		{
		case PowerModeChangeType.Suspend:
			_logger.LogInformation("Going to suspended mode");
			_driver.OnPowerStateChange(resume: false);
			break;
		case PowerModeChangeType.Resume:
			_logger.LogInformation("Resuming from suspended mode");
			_driver.OnPowerStateChange(resume: true);
			break;
		}
	}

	private void SynPost(Action action, [CallerMemberName] string callerName = null)
	{
		_synCtx.Post(delegate
		{
			try
			{
				action?.Invoke();
			}
			catch (System.Exception exception)
			{
				_logger.LogError(exception, "Failed to post call: callerName={callerName}", callerName);
			}
		}, null);
	}

	private void SynSend(Action action, [CallerMemberName] string callerName = null)
	{
		_synCtx.Send(delegate
		{
			try
			{
				action?.Invoke();
			}
			catch (System.Exception exception)
			{
				_logger.LogError(exception, "Failed to send call: callerName={callerName}", callerName);
			}
		}, null);
	}

	public void StartComm()
	{
		if (!License.SupporetedFeatures.IsSupported(ProductFeatures.RemoteAdmin))
		{
			Settings.UseTcpBinding = false;
		}
		if (Settings.UseNpBinding || Settings.UseTcpBinding)
		{
			NLWcf nLWcf = (NLWcf)(_comm = new NLWcf());
			_comm.ClientCreated += _comm_ClientCreated;
			nLWcf.Start(_service);
		}
	}

	public void StopComm()
	{
		if (_comm != null)
		{
			_comm.Dispose();
			_comm.ClientCreated -= _comm_ClientCreated;
			_comm = null;
		}
	}

	private void _comm_ClientCreated(object sender, ConnectionEventArgs e)
	{
		e.Client.IsElevationRequired = !e.Client.IsRemote && Settings.RequireElevationLocal;
	}

	public void RestartCommunication()
	{
		CheckAccess(Rights.Control);
		StopComm();
		StartComm();
	}

	private void OnBfeChanged(bool running)
	{
	}

	internal void InitSecurityManager()
	{
		_logger.LogTrace("InitSecurityManager: >>");
		try
		{
			List<NetLimiter.Service.Security.AccessTableRow> table = CreateAccessTable(LoadAccessTable(), ignoreErrors: true);
			_securityManager = new SecurityManager(table);
		}
		catch (System.Exception ex)
		{
			_logger.LogError(ex, "InitSecurityManager: SetAccessTableInternal failed.");
			throw ex;
		}
		finally
		{
			_logger.LogTrace("InitSecurityManager: <<");
		}
	}

	private void OnSettingsTimer()
	{
		_logger.LogTrace("Settings timer handler fired");
		try
		{
			SaveSettings();
		}
		catch (System.Exception exception)
		{
			_logger.LogError(exception, "Failed to save settings");
		}
	}

	internal void OnSettingsChanged()
	{
		_logger.LogTrace("OnSettingsChanged");
		SynchronizationContext synCtx = SynchronizationContext.Current;
		if (_settingsTimer == null)
		{
			_settingsTimer = new System.Timers.Timer();
			_settingsTimer.AutoReset = false;
			_settingsTimer.Elapsed += delegate
			{
				synCtx.Send(delegate
				{
					OnSettingsTimer();
				}, null);
			};
		}
		_settingsTimer.Interval = 5000.0;
		_settingsTimer.Start();
	}

	private void SaveSettings()
	{
		string settingsPath = GetSettingsPath();
		string settingsTempPath = GetSettingsTempPath(settingsPath);
		string settingsLastWorkingPath = GetSettingsLastWorkingPath(settingsPath);
		CheckMainThread();
		try
		{
			Settings.Save(settingsTempPath);
		}
		catch (System.Exception ex)
		{
			_logger.LogError("Failed to save temp settings: {path}", settingsTempPath);
			throw ex;
		}
		try
		{
			XElement.Load(settingsTempPath);
		}
		catch
		{
			_logger.LogError("Failed to verify saved temp settings: {path}", settingsTempPath);
			CreateOnFailedFileCopy(settingsTempPath);
			throw;
		}
		if (File.Exists(settingsPath))
		{
			if (File.Exists(settingsLastWorkingPath))
			{
				File.Delete(settingsLastWorkingPath);
			}
			File.Move(settingsPath, settingsLastWorkingPath);
		}
		File.Move(settingsTempPath, settingsPath);
	}

	public ServiceInfo Open()
	{
		CheckAccess(Rights.Monitor);
		ClientContext currentClient = _comm.GetCurrentClient();
		ServiceInfo serviceInfo = new ServiceInfo();
		currentClient.ClientId = Guid.NewGuid().ToString().ToLower()
			.Replace("-", "")
			.Trim('{', '}');
		serviceInfo.SettingsVersion = Settings.Version;
		serviceInfo.HostName = Environment.MachineName;
		serviceInfo.HasPendingFwr = _lastFwrCnt != 0;
		serviceInfo.License = License;
		serviceInfo.LocalhostFootprint = LocalhostFootprint;
		serviceInfo.IsBfeRunning = _isBfeRunning;
		serviceInfo.OSVersion = Environment.OSVersion.Version.ToString();
		serviceInfo.ClientId = currentClient.ClientId;
		serviceInfo.SvcInstPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
		serviceInfo.IsElevationRequired = IsElevationRequired(currentClient);
		serviceInfo.ServiceVersion = "5.3.6.0";
		return serviceInfo;
	}

	internal void OnFwrCountChanged(uint count)
	{
		if (_started)
		{
			_lastFwrCnt = count;
			if (_fwrTimer == null)
			{
				_fwrTimer = new System.Timers.Timer();
				_fwrTimer.AutoReset = false;
				_fwrTimer.Elapsed += _fwrTimer_Elapsed;
			}
			if (!_fwrTimerRunning)
			{
				_fwrTimer.Interval = 100.0;
				_fwrTimer.Start();
				_fwrTimerRunning = true;
			}
		}
	}

	private void _fwrTimer_Elapsed(object sender, ElapsedEventArgs e)
	{
		SynSend(delegate
		{
			OnFwrTimer();
		}, "_fwrTimer_Elapsed");
	}

	private void OnFwrTimer()
	{
		_logger.LogTrace("OnFwrTimer: cnt={cnt}, autoStart={autoStart}", _comm.Clients.Count, Settings.RunClientOnFwRequest);
		_fwrTimerRunning = false;
		if (!_started)
		{
			_logger.LogTrace("OnFwrTimer: not started");
			return;
		}
		if (!_comm.Clients.Where((ClientContext x) => x.IsHandlingFwrs).Any() && Settings.RunClientOnFwRequest)
		{
			try
			{
				StartClient();
				return;
			}
			catch (System.Exception exception)
			{
				_logger.LogError(exception, "Failed to start nlclient from service");
				return;
			}
		}
		Callback.OnNotification(NLServiceNotificationType.FirewallRequest);
	}

	public Filter AddFilter(Filter flt)
	{
		if (flt == null)
		{
			throw new ArgumentNullException("flt");
		}
		CheckAccess(Rights.Control);
		_logger.LogTrace("Adding filter: name={name}", flt.Name);
		flt = Settings.ConvertFilter(flt);
		return AddFilterInternal(flt);
	}

	private Filter AddFilterInternal(Filter flt)
	{
		if (Settings.Filters.Any((Filter x) => x.Id == flt.Id))
		{
			throw new FilterAlreadyExistsException(flt);
		}
		NLSvcHelper.InitFilterName(flt);
		Filter filter = flt;
		DateTime createdTime = (flt.UpdatedTime = DateTime.UtcNow);
		filter.CreatedTime = createdTime;
		Settings.Filters.Add(flt);
		_driver.AddFilterToDriver(flt);
		Callback.OnFilterAdded(flt);
		OnSettingsChanged();
		return flt;
	}

	public IEnumerable<NetLimiter.Service.Api.AccessTableRow> GetAccessTable()
	{
		return LoadAccessTable();
	}

	internal AppInfo AddAppPath(AppId appId)
	{
		if (appId == null)
		{
			throw new ArgumentNullException("appId");
		}
		StorePackageFilter storePackageFilter = null;
		AppInfoCtx ctx = new AppInfoCtx
		{
			AppId = appId
		};
		AppInfo appInfo = Settings.AppInfos.FirstOrDefault((AppInfo x) => appId.Equals(x.AppId));
		if (appInfo != null)
		{
			if (appInfo.CheckFileCreationTime())
			{
				appInfo.UpdateInfo(ctx);
			}
			return appInfo;
		}
		Sid sid = appId.Sid;
		if ((object)sid != null && sid.IsStoreAppSid)
		{
			storePackageFilter = Settings.Filters.OfType<StorePackageFilter>().FirstOrDefault((StorePackageFilter x) => x.Sid.Equals(appId.Sid));
			try
			{
				if (storePackageFilter == null)
				{
					storePackageFilter = new StorePackageFilter(appId, ctx);
					AddFilterInternal(storePackageFilter);
				}
				else
				{
					storePackageFilter.UpdateInfo(ctx);
					FilterUpdateInfo info = new FilterUpdateInfo(storePackageFilter, hasChange: true);
					Callback.OnFilterUpdated(info);
				}
			}
			catch (ManifestNotFoundException)
			{
			}
			catch
			{
				_logger.LogWarning("Failed to create store package: " + appId.Path);
			}
		}
		appInfo = AppInfo.Create(appId, ctx);
		List<AppInfo> list = (from x in Settings.AppInfos
			where PathHelper.IsEqualVersionIgnore(x.AppId.Path, appId.Path)
			where !File.Exists(x.AppId.Path)
			select x).ToList();
		Settings.AppInfos.Add(appInfo);
		OnSettingsChanged();
		Callback.OnAppAdded(appInfo);
		foreach (AppInfo item in list)
		{
			RemoveAppInternal(item.AppId);
		}
		return appInfo;
	}

	public bool RemoveApp(AppId appId)
	{
		CheckAccess(Rights.Control);
		return RemoveAppInternal(appId);
	}

	public bool IsAppInUse(AppId appId)
	{
		return !AppInfo.GetIsDead(appId);
	}

	private bool RemoveAppInternal(AppId appId)
	{
		if (!AppInfo.GetIsDead(appId))
		{
			return false;
		}
		if (Settings.RemoveApps(appId))
		{
			_driver.RemoveAppFromDriver(appId);
			Callback.OnAppRemoved(appId);
			return true;
		}
		return false;
	}

	public void SetAccessTable(IEnumerable<NetLimiter.Service.Api.AccessTableRow> srcTable)
	{
		CheckAccess(Rights.Control);
		CheckFeature(ProductFeatures.UserPerms);
		_logger.LogInformation("Setting new access table");
		SecurityManager securityManager = new SecurityManager(CreateAccessTable(srcTable, ignoreErrors: false));
		using (_comm.Impersonate())
		{
			try
			{
				securityManager.CheckAccess(Rights.Monitor);
			}
			catch (AccessDeniedException)
			{
				throw new AccessLossException(Rights.Monitor, WindowsIdentity.GetCurrent().Name);
			}
		}
		using (_comm.Impersonate())
		{
			try
			{
				securityManager.CheckAccess(Rights.Control);
			}
			catch (AccessDeniedException)
			{
				throw new AccessLossException(Rights.Control, WindowsIdentity.GetCurrent().Name);
			}
		}
		try
		{
			SaveAccessTable(srcTable.ToList());
		}
		catch (System.Exception ex3)
		{
			_logger.LogError(ex3, "Failed to save access table");
			throw ex3;
		}
		_securityManager = securityManager;
	}

	private List<NetLimiter.Service.Security.AccessTableRow> CreateAccessTable(IEnumerable<NetLimiter.Service.Api.AccessTableRow> srcTable, bool ignoreErrors)
	{
		List<NetLimiter.Service.Security.AccessTableRow> list = new List<NetLimiter.Service.Security.AccessTableRow>();
		foreach (NetLimiter.Service.Api.AccessTableRow item in srcTable)
		{
			SecurityIdentifier securityIdentifier;
			try
			{
				if (!string.IsNullOrEmpty(item.Sid))
				{
					securityIdentifier = new SecurityIdentifier(item.Sid);
				}
				else
				{
					if (string.IsNullOrEmpty(item.Name))
					{
						throw new ArgumentNullException("Name");
					}
					securityIdentifier = (SecurityIdentifier)new NTAccount(item.Name).Translate(typeof(SecurityIdentifier));
				}
			}
			catch (System.Exception ex)
			{
				_logger.LogError(ex, "Failed to parse sid: sid={sid}", item.Sid);
				if (ignoreErrors)
				{
					continue;
				}
				throw ex;
			}
			NTAccount identity;
			try
			{
				identity = (NTAccount)securityIdentifier.Translate(typeof(NTAccount));
			}
			catch (System.Exception ex2)
			{
				_logger.LogError(ex2, "Failed to translate sid: sid={sid}", item.Sid);
				if (ignoreErrors)
				{
					continue;
				}
				throw ex2;
			}
			NetLimiter.Service.Security.AccessTableRow accessTableRow = new NetLimiter.Service.Security.AccessTableRow(identity);
			accessTableRow.AllowedRights = item.AllowedRights;
			accessTableRow.DeniedRights = item.DeniedRights;
			list.Add(accessTableRow);
		}
		return list;
	}

	internal void CheckLicense()
	{
		CheckMainThread();
		if (License.IsExtendedExpired)
		{
			throw new ExpiredException(License);
		}
		if (License.IsCancelled)
		{
			throw new SubscriptionCancelledException();
		}
	}

	internal void CheckFeature(ProductFeatures feature)
	{
		CheckMainThread();
		if (!License.SupporetedFeatures.IsSupported(feature))
		{
			throw new FeatureNotAvailableException(License);
		}
	}

	internal void CheckAccess(Rights rights)
	{
		CheckMainThread();
		using (_comm.Impersonate())
		{
			ClientContext currentClient = _comm.GetCurrentClient();
			if (currentClient.IsRemote)
			{
				_securityManager.CheckAccess(Rights.RemoteConnect);
			}
			if (rights.HasFlag(Rights.Control) && currentClient.IsElevationRequired && !ProcessHelper.IsAdmin())
			{
				throw new AdminRequiredException();
			}
			_securityManager.CheckAccess(rights);
		}
	}

	private void CheckMainThread()
	{
		if (SynchronizationContext.Current != _synCtx)
		{
			_logger.LogWarning("Main thread check failed: the stack={stack}", Environment.StackTrace);
		}
	}

	public string GetOrCreateDataPath(bool create = true)
	{
		if (_dataPath == null)
		{
			_dataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Locktime\\NetLimiter\\5");
			if (create && !Directory.Exists(_dataPath))
			{
				Directory.CreateDirectory(_dataPath);
			}
		}
		return _dataPath;
	}

	public string GetSettingsPath()
	{
		return Path.Combine(GetOrCreateDataPath(), "nl_settings.xml");
	}

	public string GetLicensePath()
	{
		return Path.Combine(GetOrCreateDataPath(), "license.json");
	}

	public string GetSettingsTempPath(string path)
	{
		return path + ".tmp";
	}

	public string GetSettingsLastWorkingPath(string path)
	{
		return path + ".lastworking";
	}

	public List<string> GetDeadFilters()
	{
		return (from x in Settings.Filters
			where x.IsDead()
			select x.Id).ToList();
	}

	public List<AppId> GetRemovedApps()
	{
		return (from x in Settings.AppInfos
			where x.IsDead
			select x.AppId).ToList();
	}

	public void LoadSettings()
	{
		string settingsPath = GetSettingsPath();
		bool flag = false;
		try
		{
			Settings = LoadSettings(settingsPath);
			if (Settings != null)
			{
				return;
			}
		}
		catch
		{
			CreateOnFailedFileCopy(settingsPath);
			flag = true;
		}
		string settingsLastWorkingPath = GetSettingsLastWorkingPath(settingsPath);
		try
		{
			Settings = LoadSettings(settingsLastWorkingPath);
			if (Settings != null)
			{
				_logger.LogWarning("Settings loaded from last working copy: {path}", settingsLastWorkingPath);
				AddNotification(NotificationSeverity.Normal, "Service settings restored", "Service settings loaded from last working copy");
				return;
			}
		}
		catch
		{
			flag = true;
		}
		try
		{
			settingsLastWorkingPath = GetSettingsTempPath(settingsPath);
			Settings = LoadSettings(settingsLastWorkingPath);
			if (Settings != null)
			{
				_logger.LogWarning("Settings loaded from tmp file: {path}", settingsLastWorkingPath);
				AddNotification(NotificationSeverity.Normal, "Service settings restored", "Service settings loaded from temp file");
				return;
			}
		}
		catch
		{
			flag = true;
		}
		try
		{
			settingsLastWorkingPath = settingsPath.Replace("\\5\\", "\\4\\");
			if (File.Exists(settingsLastWorkingPath))
			{
				File.Copy(settingsLastWorkingPath, settingsPath);
				Settings = LoadSettings(settingsPath);
				return;
			}
		}
		catch
		{
			_logger.LogWarning("Failed to load v4 settings");
		}
		_logger.LogInformation("Creating new settings");
		Settings = new NLSvcSettings();
		if (flag)
		{
			AddNotification(NotificationSeverity.High, "Failed to load settings", "New settings created");
		}
	}

	private NLSvcSettings LoadSettings(string path)
	{
		if (File.Exists(path))
		{
			try
			{
				return NLSvcSettings.Load(path);
			}
			catch (System.Exception ex)
			{
				_logger.LogError(ex, "Failed to load settings: {path}", path);
				throw ex;
			}
		}
		return null;
	}

	private bool CreateOnFailedFileCopy(string path)
	{
		try
		{
			string text = path + ".onfailed.copy";
			if (File.Exists(text))
			{
				File.Delete(text);
			}
			File.Copy(path, text);
			return true;
		}
		catch (System.Exception exception)
		{
			_logger.LogError(exception, "Failed to create .onfailed settings copy");
			return false;
		}
	}

	public AppInfo GetAppInfo(AppId appId)
	{
		if (appId == null)
		{
			throw new ArgumentNullException("appId");
		}
		AppInfo appInfo = Settings.AppInfos.FirstOrDefault((AppInfo x) => appId.Equals(x.AppId));
		if (appInfo != null)
		{
			return appInfo;
		}
		if (!File.Exists(appId.Path))
		{
			throw new FileNotFoundException("The app doesn't exist: " + appId.Path);
		}
		return AppInfo.Create(appId);
	}

	private void InitLocalhostFootprint()
	{
		using RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
		using RegistryKey registryKey2 = registryKey.CreateSubKey("Software\\Locktime Software\\NetLimiter\\5");
		LocalhostFootprint = Guid.NewGuid().ToString();
		registryKey2.SetValue("LocalhostFootprint", LocalhostFootprint);
	}

	public bool CheckElevationRequired()
	{
		ClientContext client = _comm.GetCurrentClient();
		if (string.IsNullOrEmpty(client.ClientId))
		{
			throw new System.Exception("No check code. Call Open first.");
		}
		bool flag = !client.IsElevationRequired;
		if (flag)
		{
			return true;
		}
		try
		{
			uint pid = _driver.SvcCliCnnCheck(client.ClientId);
			if (pid == 0)
			{
				_logger.LogDebug("SvcCliCnnCheck: pid is null");
				return false;
			}
			Process process = (from x in Process.GetProcesses()
				where x.Id == pid
				select x).FirstOrDefault();
			if (process == null)
			{
				_logger.LogDebug("SvcCliCnnCheck process not found: pid={pid}", pid);
				return false;
			}
			client.AppPath = process.Modules[0].FileName;
			client.AppName = process.ProcessName;
			if (!flag)
			{
				flag = Settings.ApiAccessList.Any((string x) => string.Equals(x, client.AppPath, StringComparison.InvariantCultureIgnoreCase));
			}
			if (!flag)
			{
				string directoryName = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
				string directoryName2 = Path.GetDirectoryName(client.AppPath);
				flag = string.Equals(directoryName, directoryName2, StringComparison.InvariantCultureIgnoreCase);
			}
			_logger.LogDebug("SvcCliCnnCheck: res={res}", flag);
			client.IsElevationRequired = !flag;
			return flag;
		}
		catch (System.Exception exception)
		{
			_logger.LogError(exception, "SvcCliCnnCheck failed");
			return false;
		}
	}

	public void Elevate(string clientId)
	{
		ClientContext currentClient = _comm.GetCurrentClient();
		bool isElevationRequired = currentClient.IsElevationRequired;
		currentClient.IsElevationRequired = true;
		CheckAccess(Rights.Control);
		currentClient.IsElevationRequired = isElevationRequired;
		(_comm.Clients.FirstOrDefault((ClientContext x) => x.ClientId == clientId) ?? throw new NLException("Client not found")).IsElevationRequired = false;
	}

	public static RegistryKey GetV4RegCodeRegistryKey()
	{
		char directorySeparatorChar = Path.DirectorySeparatorChar;
		return Registry.LocalMachine.OpenSubKey("Software\\Locktime Software\\NetLimiter\\5".Replace($"{directorySeparatorChar}5", $"{directorySeparatorChar}4") + $"{directorySeparatorChar}RegData", writable: true);
	}

	public static bool TryGetV4RegCode(out string regCode)
	{
		regCode = null;
		try
		{
			using RegistryKey registryKey = GetV4RegCodeRegistryKey();
			if (!(registryKey.GetValue("RegCode", null) is byte[] encryptedData))
			{
				return false;
			}
			byte[] optionalEntropy = new byte[15]
			{
				37, 140, 90, 214, 143, 68, 44, 166, 233, 119,
				40, 194, 156, 217, 155
			};
			regCode = Encoding.Unicode.GetString(ProtectedData.Unprotect(encryptedData, optionalEntropy, DataProtectionScope.CurrentUser));
			return !string.IsNullOrWhiteSpace(regCode);
		}
		catch
		{
			return false;
		}
	}

	public static void DeleteV4RegCode()
	{
		try
		{
			using RegistryKey registryKey = GetV4RegCodeRegistryKey();
			registryKey.DeleteValue("RegCode");
		}
		catch
		{
		}
	}

	public ClientInfo GetClientInfo(string clientId)
	{
		ClientContext currentClient = _comm.GetCurrentClient();
		ClientContext clientContext;
		if (clientId != null)
		{
			clientContext = _comm.Clients.FirstOrDefault((ClientContext x) => x.ClientId == clientId);
			if (clientContext == null)
			{
				throw new NLException("Client not found: clientId=" + clientId);
			}
			if (clientContext != currentClient)
			{
				CheckAccess(Rights.Control);
			}
		}
		else
		{
			clientContext = currentClient;
		}
		return new ClientInfo
		{
			AppName = clientContext.AppName,
			AppPath = clientContext.AppPath,
			IsElevationRequired = IsElevationRequired(clientContext)
		};
	}

	private bool IsElevationRequired(ClientContext client)
	{
		if (!client.IsElevationRequired)
		{
			return false;
		}
		using (_comm.Impersonate())
		{
			WindowsIdentity.GetCurrent();
			return !ProcessHelper.IsAdmin();
		}
	}

	private void OnQuotaOverflow(uint ruleId, uint overflowCount)
	{
		_logger.LogInformation("OnQuotaOverflow: ruleId={ruleId}, count={count}", ruleId, overflowCount);
		if (Settings.Rules.FirstOrDefault((Rule x) => x.InternalId == ruleId) is QuotaRule quotaRule)
		{
			bool flag = overflowCount != 0;
			if (quotaRule.IsOverflow == flag)
			{
				return;
			}
			quotaRule.IsOverflow = flag;
			quotaRule.IsAnswered = false;
			foreach (string id in quotaRule.OnOverflowRules)
			{
				RuleSchedulerItem ruleSchedulerItem = RuleScheduler.Items.FirstOrDefault((RuleSchedulerItem x) => x.Rule.Id == id);
				if (ruleSchedulerItem != null)
				{
					ruleSchedulerItem.Rule.IsEnabled = flag;
					RuleScheduler.UpdateRule(ruleSchedulerItem.Rule);
				}
			}
			RuleUpdateInfo ruleUpdateInfo = new RuleUpdateInfo(quotaRule, hasChange: true);
			ruleUpdateInfo.Flags |= RuleUpdateFlags.QuotaOverflow;
			Callback.OnRuleUpdated(ruleUpdateInfo);
			_logger.LogInformation("OnQuotaOverflow: IsOverflow={value}", quotaRule.IsOverflow);
		}
		else
		{
			_logger.LogWarning("OnQuotaOverflow: Quota not found: {id}", ruleId);
		}
	}

	private bool TryFindFilter(string id, out Filter filter)
	{
		filter = Settings.Filters.FirstOrDefault((Filter x) => x.Id == id);
		return filter != null;
	}

	private Filter FindFilter(string id)
	{
		if (TryFindFilter(id, out var filter))
		{
			return filter;
		}
		throw new FilterNotFoundException(id);
	}

	private bool TryFindRule(string id, out Rule rule)
	{
		rule = Settings.Rules.FirstOrDefault((Rule x) => x.Id == id);
		return rule != null;
	}

	private Rule FindRule(string id)
	{
		if (TryFindRule(id, out var rule))
		{
			return rule;
		}
		throw new RuleNotFoundException(id);
	}

	private bool TryFindRule(uint id, out Rule rule)
	{
		rule = Settings.Rules.FirstOrDefault((Rule x) => x.InternalId == id);
		return rule != null;
	}

	private Rule FindRule(uint id)
	{
		if (TryFindRule(id, out var rule))
		{
			return rule;
		}
		throw new RuleNotFoundException(id.ToString());
	}

	private void OnRuleChanged(uint ruleId, bool enabled)
	{
		_logger.LogDebug("Rule changed: ruleId={ruleId}, enabled={enabled}", ruleId, enabled);
		if (TryFindRule(ruleId, out var rule) && rule.IsEnabled != enabled)
		{
			try
			{
				rule.IsEnabled = enabled;
				RuleScheduler.UpdateRule(rule);
			}
			catch (System.Exception exception)
			{
				_logger.LogError(exception, "Failed to update rule state");
			}
		}
	}

	public void UpdateAppInfo(AppId appId, AppInfo newAppInfo)
	{
		AppInfo appInfo = Settings.AppInfos.FirstOrDefault((AppInfo x) => x.AppId.Equals(appId));
		if (appInfo != null)
		{
			appInfo.CompanyName = newAppInfo.CompanyName;
			appInfo.FileDescription = newAppInfo.FileDescription;
			appInfo.FileVersion = newAppInfo.FileVersion;
			appInfo.ProductName = newAppInfo.ProductName;
			appInfo.ProductVersion = newAppInfo.ProductVersion;
			appInfo.Tags = newAppInfo.Tags;
			_driver.SendAppTagsToDriver(appInfo);
			Callback.OnAppUpdated(appInfo);
			OnSettingsChanged();
		}
	}

	private string GetClientInstPath()
	{
		string directoryName = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
		if (File.Exists(Path.Combine(directoryName, "nlclientapp.exe")))
		{
			return directoryName;
		}
		throw new System.Exception("Client installation path can't be located");
	}

	private void StartClient(Rights requiredRights = Rights.Control)
	{
		_logger.LogTrace("StartClient");
		string clientInstPath = GetClientInstPath();
		_logger.LogTrace("StartClient: instPath={instPath}", clientInstPath);
		clientInstPath = Path.Combine(clientInstPath, "nlclientapp.exe");
		if (!File.Exists(clientInstPath))
		{
			_logger.LogWarning("Failed to start client: path not found: path={path}", clientInstPath);
			throw new System.Exception("Client not found: " + clientInstPath);
		}
		WindowsIdentity interactiveUser = ProcessHelper.GetInteractiveUser();
		using (interactiveUser.Impersonate())
		{
			_securityManager.CheckAccess(requiredRights);
			ProcessHelper.StartProcessAsUser(interactiveUser, clientInstPath, "/minimized /localcnn");
		}
	}

	public void SubscribeAsFwReqeustHandler(bool subscribe)
	{
		CheckAccess(Rights.Control);
		ClientContext currentClient = _comm.GetCurrentClient();
		currentClient.IsHandlingFwrs = subscribe;
		_logger.LogDebug("Subscribe as fwr handler: sid={sid}, subscribe={value}", currentClient.SessionId, subscribe);
	}

	public List<ServiceNotification> GetNotifications()
	{
		if (Settings.Notifications == null)
		{
			return new List<ServiceNotification>();
		}
		return Settings.Notifications;
	}

	public ServiceNotification AddNotification(NotificationSeverity severity, string message, string desc, string id = null, string helpLink = null, string appLink = null, bool isManaged = false, Action<ServiceNotification> initializer = null)
	{
		ServiceNotification serviceNotification = new ServiceNotification
		{
			Severity = severity,
			Message = message,
			Description = desc,
			HelpLink = helpLink,
			AppLink = appLink,
			IsManaged = isManaged
		};
		initializer?.Invoke(serviceNotification);
		if (id != null)
		{
			serviceNotification.Id = id;
		}
		AddNotification(serviceNotification);
		return serviceNotification;
	}

	public void ClearNotifications()
	{
		int num = Settings.Notifications.Count();
		Settings.Notifications.RemoveAll((ServiceNotification x) => !x.IsManaged);
		if (Settings.Notifications.Count != num)
		{
			Callback.OnNotification(NLServiceNotificationType.NotificationsChanged);
			OnSettingsChanged();
		}
	}

	public void RemoveNotification(string id)
	{
		if (Settings != null)
		{
			int count = Settings.Notifications.Count;
			ServiceNotification item;
			while ((item = Settings.Notifications.FirstOrDefault((ServiceNotification x) => x.Id == id)) != null)
			{
				Settings.Notifications.Remove(item);
			}
			if (count != Settings.Notifications.Count)
			{
				Callback.OnNotification(NLServiceNotificationType.NotificationsChanged);
				OnSettingsChanged();
			}
		}
	}

	public void AddNotification(ServiceNotification notif)
	{
		if (Settings != null)
		{
			RemoveNotification(notif.Id);
			notif.Created = DateTime.UtcNow;
			Settings.Notifications.Add(notif);
			Callback.OnNotification(NLServiceNotificationType.NotificationsChanged);
		}
	}

	public void MuteNotifications(string[] notifIds)
	{
		foreach (string id in notifIds)
		{
			ServiceNotification serviceNotification = Settings.Notifications.FirstOrDefault((ServiceNotification x) => x.Id == id);
			if (serviceNotification != null)
			{
				serviceNotification.IsMuted = true;
				_logger.LogDebug("Notification muted: id={id}", serviceNotification.Id);
				OnSettingsChanged();
			}
		}
	}

	private void InitLicense()
	{
		string licensePath = GetLicensePath();
		_logger.LogInformation("RegData path: {path}", licensePath);
		RegData regData = null;
		if (File.Exists(licensePath))
		{
			string value = File.ReadAllText(licensePath);
			try
			{
				regData = JsonConvert.DeserializeObject<RegData>(value);
				/* EDIT
				 * if (!VerifyRegData(regData))
				{
					regData = null;
				}*/
			}
			catch (System.Exception exception)
			{
				_logger.LogError(exception, "Failed to init existing license: {path}", licensePath);
			}
		}
		if (regData != null)
		{
			License = new NLLicense(regData);
			_logger.LogInformation("License found: expiration={expiration}", License.Expiration);
		}
		else
		{
			DateTime installTime = GetInstallTime();
			DateTime expiration = installTime.AddDays(28.0);
			License = new NLLicense(expiration);
			_logger.LogInformation("License not found: expiration={expiration}, installTime={installTime}", License.Expiration, installTime);
		}
		Callback.OnLicenseChange(License);
	}

	private async Task CheckLicenseExpiration(CheckLicenseExpirationType reloadType)
	{
		if (License == null)
		{
			_logger.LogWarning("CheckLicenseExpiration: License is null (not initialized).");
			return;
		}
		bool flag = false;
		string regCode = (License.IsRegistered ? License.RegCodeHash : null);
		bool v4found = false;
		switch (reloadType)
		{
		case CheckLicenseExpirationType.ReloadAlways:
			flag = true;
			break;
		case CheckLicenseExpirationType.ReloadIfExpired:
			flag = License.IsRegistered && License.DaysLeftRaw < 0 && License.ExtendedDaysLeft > 0;
			break;
		}
		if (regCode == null && !License.IsTestingVersion)
		{
			bool flag2;
			v4found = (flag2 = TryGetV4RegCode(out regCode));
			flag = flag2;
		}
		if (flag && regCode != null)
		{
			try
			{
				_logger.LogDebug("Try to check license online...");
				RegData regData = await NLClient.CheckLicenseAsync(GetHWCodeHash(), regCode);
				_logger.LogDebug("Reg data obtained: Cancelled={cancelled}, EndTime={endTime}", regData.IsCancelled, regData.EndTime);
				SetRegistrationDataInternal(regData, checkExpiration: false);
				if (v4found)
				{
					DeleteV4RegCode();
				}
			}
			catch (System.Exception ex)
			{
				AddNotification(NotificationSeverity.Normal, "Failed to load subscription from licensing server.", ex.Message);
				_logger.LogError(ex, "Failed to load subscription from licensing server: {message}", ex.Message);
			}
		}
		if (License.IsCancelled)
		{
			RemoveNotification("notifid-lic-expiration");
			AddNotification(NotificationSeverity.High, "Subscription cancelled", "The license is no longer valid, the subscription has been cancelled.", "notifid-subscription-cancelled", Common.RegHelpLink, null, isManaged: true, delegate(ServiceNotification x)
			{
				x.SetParam("License", License);
			});
			return;
		}
		RemoveNotification("notifid-subscription-cancelled");
		if ((License.IsRegistered && License.DaysLeftRaw < 0) || (!License.IsRegistered && License.DaysLeft <= 7))
		{
			AddNotification(NotificationSeverity.High, "NetLimiter expiration", $"NetLimiter expiration days left: {License.DaysLeft}", "notifid-lic-expiration", Common.RegHelpLink, null, isManaged: true, delegate(ServiceNotification x)
			{
				x.SetParam("License", License);
			});
		}
		else
		{
			RemoveNotification("notifid-lic-expiration");
		}
	}

	private void OnNewDayTimer(object sender, EventArgs e)
	{
		CheckLicenseExpirationType reloadType = CheckLicenseExpirationType.ReloadIfExpired;
		if (new Random().Next(1, 15) == 1)
		{
			reloadType = CheckLicenseExpirationType.ReloadAlways;
		}
		CheckLicenseExpiration(reloadType);
	}

	public DateTime GetInstallTime()
	{
		DateTime instTime = DateTime.UtcNow;
		Action<DateTime> action = delegate(DateTime tm)
		{
			if (tm != default(DateTime) && tm != DateTime.MinValue && tm != DateTime.MaxValue && tm < instTime)
			{
				instTime = tm;
			}
		};
		foreach (Network network in Settings.Networks)
		{
			action(network.TimeCreated);
		}
		foreach (Filter filter in Settings.Filters)
		{
			action(filter.CreatedTime);
			action(filter.UpdatedTime);
		}
		foreach (Rule rule in Settings.Rules)
		{
			action(rule.CreatedTime);
			action(rule.UpdatedTime);
		}
		return instTime;
	}

	public NLLicense RemoveRegistrationData()
	{
		CheckAccess(Rights.Control);
		string licensePath = GetLicensePath();
		if (File.Exists(licensePath))
		{
			File.Delete(licensePath);
		}
		InitLicense();
		_driver.SendLicenseInfoToDrv();
		CheckLicenseExpiration(CheckLicenseExpirationType.NoReload);
		return License;
	}

	public NLLicense SetRegistrationData(RegData regData)
	{
		CheckAccess(Rights.Control);
		SetRegistrationDataInternal(regData, checkExpiration: true);
		return License;
	}

	private void SetRegistrationDataInternal(RegData regData, bool checkExpiration)
	{
		if (regData == null)
		{
			throw new ArgumentNullException("regData");
		}
		if (false )//!VerifyRegData(regData))
		{
			throw new System.Exception("Failed to verify registration data: invalid signature");
		}
		if (regData.IsCancelled && License?.RegCodeHash != regData.RegCodeHash)
		{
			throw new SubscriptionCancelledException();
		}
		string licensePath = GetLicensePath();
		string contents = JsonConvert.SerializeObject(regData, Formatting.Indented);
		File.WriteAllText(licensePath, contents);
		InitLicense();
		if (!License.SupporetedFeatures.IsSupported(ProductFeatures.Stats))
		{
			_service.IsStatsEnabled = false;
		}
		_driver.SendLicenseInfoToDrv();
		if (checkExpiration)
		{
			CheckLicenseExpiration(CheckLicenseExpirationType.NoReload);
		}
	}

	private bool VerifyRegData(RegData regData)
	{
		RSAParameters rSAParameters = JsonConvert.DeserializeObject<RSAParameters>(Encoding.UTF8.GetString(Convert.FromBase64String(_licPubKey)));
		JsonConvert.SerializeObject(rSAParameters);
		using RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider(2048);
		rSACryptoServiceProvider.ImportParameters(rSAParameters);
		byte[] buffer = regData.ToBytes();
		return rSACryptoServiceProvider.VerifyData(buffer, new SHA256CryptoServiceProvider(), Convert.FromBase64String(regData.Signature));
	}

	public static string GetSHA256(string data)
	{
		using SHA256 sHA = SHA256.Create();
		return Convert.ToBase64String(sHA.ComputeHash(Encoding.UTF8.GetBytes(data)));
	}

	public string GetHWCodeHash()
	{
		string text = "null";
		try
		{
			string fileName = Process.GetCurrentProcess().MainModule.FileName;
			foreach (ManagementObject item in new ManagementObjectSearcher("SELECT * FROM Win32_Volume").Get())
			{
				string text2 = item["DriveLetter"]?.ToString();
				if (text2 != null && fileName.StartsWith(text2, StringComparison.InvariantCultureIgnoreCase))
				{
					text = item["SerialNumber"]?.ToString();
				}
			}
		}
		catch
		{
		}
		//File.WriteAllText(@"c:\temp\GetHWCodeHash", "f353d64ed8594af4a1aefd9cbfc07289" + text);
		return GetSHA256("f353d64ed8594af4a1aefd9cbfc07289" + text);
	}

	private void OnDnsCacheItemsAdded(object sender, DnsEventArgs e)
	{
		_driver.OnDnsItemsAdded(e.Items);
	}

	private void OnDnsCacheItemsRemoved(object sender, DnsEventArgs e)
	{
		_driver.OnDnsItemsRemoved(e.Items);
	}

	public void RefreshDns()
	{
		_dnsCache?.Refresh();
	}

	private void OnReloadLicense()
	{
		_logger.LogInformation("Reload licence timer fired");
		try
		{
			CheckLicenseExpiration(CheckLicenseExpirationType.ReloadAlways);
		}
		catch (System.Exception exception)
		{
			_logger.LogError(exception, "Failed to reload license");
		}
	}

	public void RemoveRule(string ruleId)
	{
		_logger.LogInformation("Removing rule: " + ruleId);
		CheckAccess(Rights.Control);
		Rule rule = FindRule(ruleId);
		if (rule.IsPredefined)
		{
			throw new NLException("Can't delete predefined rule.");
		}
		if (rule is QuotaRule quotaRule && quotaRule.OnOverflowRules.Any())
		{
			foreach (string item in quotaRule.OnOverflowRules.ToList())
			{
				if (!(rule.Id == item))
				{
					RemoveRule(item);
				}
			}
		}
		List<Rule> list = null;
		foreach (QuotaRule item2 in Settings.Rules.OfType<QuotaRule>())
		{
			if (item2.OnOverflowRules.Remove(ruleId))
			{
				list = list ?? new List<Rule>();
				list.Add(item2);
			}
		}
		if (list != null)
		{
			foreach (Rule item3 in list)
			{
				UpdateRule(item3, force: true);
			}
		}
		RuleScheduler.RemoveRule(rule);
	}

	public void InitRules()
	{
		List<Rule> list = null;
		foreach (Rule rule2 in Settings.Rules)
		{
			try
			{
				InitRule(rule2, null);
			}
			catch (System.Exception exception)
			{
				_logger.LogError(exception, "Failed to init rule: id={id}", rule2.Id);
				list = list ?? new List<Rule>();
				_logger.LogError("Removing faulty rule: id={id}", rule2.Id);
				list.Add(rule2);
			}
		}
		if (list != null)
		{
			foreach (Rule item in list)
			{
				Settings.Rules.Remove(item);
			}
		}
		foreach (QuotaRule item2 in from x in Settings.Rules.OfType<QuotaRule>()
			where x.OnOverflowRules.Any()
			select x)
		{
			List<string> list2 = null;
			foreach (string onOverflowRule in item2.OnOverflowRules)
			{
				if (TryFindRule(onOverflowRule, out var rule))
				{
					rule.IsEnabled = false;
					continue;
				}
				list2 = list2 ?? new List<string>();
				list2.Add(onOverflowRule);
			}
			if (list2 == null)
			{
				continue;
			}
			foreach (string item3 in list2)
			{
				item2.OnOverflowRules.Remove(item3);
			}
		}
		RuleScheduler.Initialize(Settings.Rules);
	}

	public uint GetNextRuleId()
	{
		return ++_nextRuleId;
	}

	public void InitRule(Rule rule, Rule ruleOld)
	{
		if (ruleOld != null)
		{
			rule.InternalId = ruleOld.InternalId;
		}
		else
		{
			rule.InternalId = GetNextRuleId();
		}
		Filter filter = FindFilter(rule.FilterId);
		NLSvcHelper.SetInternalFilterId(rule, NLSvcHelper.GetInternalId(filter));
	}

	public RuleUpdateInfo UpdateRule(Rule rule, bool force = false)
	{
		CheckAccess(Rights.Control);
		if (rule.IsEnabled)
		{
			CheckLicense();
			CheckFeatureCreate(rule);
		}
		Rule rule2 = FindRule(rule.Id);
		if (rule.Revision < rule2.Revision)
		{
			throw new RevisionException(rule);
		}
		if (!force && rule.IsCloneOf(rule2))
		{
			return new RuleUpdateInfo(rule, hasChange: false);
		}
		rule.CreatedTime = rule2.CreatedTime;
		rule.UpdatedTime = DateTime.UtcNow;
		InitRule(rule, rule2);
		RuleScheduler.UpdateRule(rule);
		return new RuleUpdateInfo(rule, hasChange: true);
	}

	private void CheckFeatureCreate(Rule rule)
	{
		if (!License.SupporetedFeatures.IsCreateRuleSupported(rule))
		{
			throw new FeatureNotAvailableException(License);
		}
	}

	private void ForEachCallback(Action<INLServiceCallback> action)
	{
		if (_comm == null)
		{
			return;
		}
		foreach (ClientContext client in _comm.Clients)
		{
			ThreadPool.QueueUserWorkItem(delegate
			{
				try
				{
					action(client.Callback);
				}
				catch (System.Exception exception)
				{
					_logger.LogError(exception, "Callback failed: sessionId={sessionId}", client.SessionId);
				}
			});
		}
	}

	void INLServiceCallback.OnAppAdded(AppInfo info)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnAppAdded(info);
		});
	}

	public void OnAppUpdated(AppInfo info)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnAppUpdated(info);
		});
	}

	void INLServiceCallback.OnAppRemoved(AppId appId)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnAppRemoved(appId);
		});
	}

	void INLServiceCallback.OnCnnEventLogUpdate(CnnEventLogUpdateEventArgs args)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnCnnEventLogUpdate(args);
		});
	}

	void INLServiceCallback.OnFilterAdded(Filter filter)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnFilterAdded(filter);
		});
	}

	void INLServiceCallback.OnFilterRemoved(string fltId)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnFilterRemoved(fltId);
		});
	}

	void INLServiceCallback.OnFilterUpdated(FilterUpdateInfo info)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnFilterUpdated(info);
		});
	}

	void INLServiceCallback.OnLicenseChange(NLLicense license)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnLicenseChange(license);
		});
	}

	void INLServiceCallback.OnNotification(NLServiceNotificationType type)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnNotification(type);
		});
	}

	void INLServiceCallback.OnOptionsChange(OptionsChangeType type, object value)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnOptionsChange(type, value);
		});
	}

	void INLServiceCallback.OnRuleAdded(Rule rule)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnRuleAdded(rule);
		});
	}

	void INLServiceCallback.OnRuleRemoved(string ruleId)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnRuleRemoved(ruleId);
		});
	}

	void INLServiceCallback.OnRuleUpdated(RuleUpdateInfo info)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnRuleUpdated(info);
		});
	}

	void INLServiceCallback.OnStatsQueryUpdate(StatsQueryUpdateEventArgs args)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnStatsQueryUpdate(args);
		});
	}

	void INLServiceCallback.OnVTReport(VTReportEventArgs args)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnVTReport(args);
		});
	}

	void INLServiceCallback.OnStatsToolsCompleted(StatsToolsResult result)
	{
		ForEachCallback(delegate(INLServiceCallback x)
		{
			x.OnStatsToolsCompleted(result);
		});
	}
}}
