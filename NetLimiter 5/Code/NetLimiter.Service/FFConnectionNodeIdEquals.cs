﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFConnectionNodeIdEquals : FilterFunctionT<ulong>
{
	public FFConnectionNodeIdEquals()
	{
	}

	public FFConnectionNodeIdEquals(ulong id)
	{
		base.Values.Add(id);
	}
}}
