﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Threading;
using Microsoft.Extensions.Logging;
using NetLimiter.Service.Comm;

namespace NetLimiter.Service{

internal class NLWcf : INLCommService, IDisposable, IErrorHandler, IServiceBehavior, IChannelInitializer
{
	public static readonly string NpAddress = "127.0.0.1";

	public static readonly string NpName = "NetLimiterPipe0";

	private static readonly ILogger<NLWcf> _logger = Logging.LoggerFactory.CreateLogger<NLWcf>();

	private ServiceHost _svcHost;

	private INLService _svc;

	private List<ClientContext> _clients = new List<ClientContext>();

	private SynchronizationContext _synCtx = SynchronizationContext.Current ?? new SynchronizationContext();

	public ReadOnlyCollection<ClientContext> Clients
	{
		get
		{
			lock (this)
			{
				return _clients.AsReadOnly();
			}
		}
	}

	public event EventHandler<ConnectionEventArgs> Disconnected;

	public event EventHandler<ConnectionEventArgs> ClientCreated;

	~NLWcf()
	{
		Dispose(dispose: false);
	}

	public void Start(INLService svc)
	{
		_logger.LogInformation("Starting WCF...");
		if (svc == null)
		{
			throw new ArgumentNullException("Svc must not be null");
		}
		_svc = svc;
		List<Uri> list = new List<Uri>();
		string text = null;
		string text2 = null;
		if (_svc.UseNpBinding)
		{
			text = GetNpUri();
			list.Add(new Uri(text));
			_logger.LogInformation("Use named pipe: {0}", text);
		}
		if (_svc.UseTcpBinding)
		{
			text2 = GetTcpUri(_svc.TcpAddress, _svc.TcpPort);
			list.Add(new Uri(text2));
			_logger.LogInformation("Use tcp: {0}", text2);
		}
		_svcHost = new ServiceHost(svc, list.ToArray());
		if (_svc.UseNpBinding)
		{
			NetNamedPipeBinding netNamedPipeBinding = new NetNamedPipeBinding();
			netNamedPipeBinding.Name = NpName;
			netNamedPipeBinding.ReceiveTimeout = TimeSpan.MaxValue;
			netNamedPipeBinding.MaxReceivedMessageSize = 1048576L;
			netNamedPipeBinding.Security.Mode = NetNamedPipeSecurityMode.Transport;
			netNamedPipeBinding.Security.Transport.ProtectionLevel = ProtectionLevel.Sign;
			_svcHost.AddServiceEndpoint(typeof(INLService), netNamedPipeBinding, text);
		}
		if (_svc.UseTcpBinding)
		{
			NetTcpBinding netTcpBinding = new NetTcpBinding();
			netTcpBinding.ReceiveTimeout = TimeSpan.MaxValue;
			netTcpBinding.MaxReceivedMessageSize = 1048576L;
			netTcpBinding.Security.Mode = SecurityMode.Transport;
			netTcpBinding.Security.Transport.ProtectionLevel = ProtectionLevel.EncryptAndSign;
			netTcpBinding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;
			_svcHost.AddServiceEndpoint(typeof(INLService), netTcpBinding, text2);
		}
		_svcHost.Description.Behaviors.Add(this);
		_svcHost.Open();
		_logger.LogInformation("WCF started");
	}

	public void Stop()
	{
		if (_svcHost == null)
		{
			return;
		}
		try
		{
			_svcHost.Close(TimeSpan.FromMilliseconds(0.0));
			_logger.LogInformation("Wcf closed");
		}
		catch
		{
			_svcHost.Abort();
			_logger.LogWarning("Wcf aborted");
		}
		finally
		{
			_svcHost = null;
		}
		_svc = null;
	}

	public static string GetNpUri()
	{
		return $"net.pipe://{NpAddress}/{NpName}";
	}

	public static string GetTcpUri(string addr, ushort port)
	{
		return $"net.tcp://{addr}:{port}";
	}

	public ClientContext GetCurrentClient()
	{
		string sessionId = OperationContext.Current.SessionId;
		ClientContext client;
		lock (this)
		{
			client = _clients.FirstOrDefault((ClientContext x) => x.SessionId == sessionId);
		}
		if (client == null)
		{
			EndpointDispatcher endpointDispatcher = OperationContext.Current.EndpointDispatcher;
			client = new ClientContext(callback: OperationContext.Current.GetCallbackChannel<INLServiceCallback>(), sessionId: sessionId);
			if (endpointDispatcher.EndpointAddress.Uri.Scheme.Equals("net.tcp", StringComparison.InvariantCultureIgnoreCase))
			{
				RemoteEndpointMessageProperty remoteEndpointMessageProperty = OperationContext.Current.IncomingMessageProperties.Values.OfType<RemoteEndpointMessageProperty>().FirstOrDefault();
				client.IsRemote = IsRemote(remoteEndpointMessageProperty?.Address);
				_logger.LogInformation("Tcpip client: Session={sessionId}, IsRemote={isRemote}", client.SessionId, client.IsRemote);
			}
			OperationContext.Current.Channel.Closed += delegate
			{
				OnClientClosed(client);
			};
			lock (this)
			{
				_clients.Add(client);
			}
			this.ClientCreated?.Invoke(this, new ConnectionEventArgs(client));
		}
		return client;
	}

	private bool IsRemote(string remoteName)
	{
		if (remoteName == null)
		{
			return false;
		}
		IPAddress[] hostAddresses;
		try
		{
			hostAddresses = Dns.GetHostAddresses(Dns.GetHostName());
		}
		catch (Exception exception)
		{
			_logger.LogError(exception, "Failed to get local addrs");
			return false;
		}
		IPAddress[] hostAddresses2;
		try
		{
			hostAddresses2 = Dns.GetHostAddresses(remoteName);
		}
		catch (Exception exception2)
		{
			_logger.LogError(exception2, "Failed to get remote addrs");
			return false;
		}
		IPAddress[] array = hostAddresses2;
		foreach (IPAddress ra in array)
		{
			if (IPAddress.IsLoopback(ra))
			{
				return false;
			}
			if (hostAddresses.Any((IPAddress la) => la.Equals(ra)))
			{
				return false;
			}
		}
		return true;
	}

	private void OnClientClosed(ClientContext client)
	{
		_logger.LogInformation("Client disconnected: id={id}", client.SessionId);
		lock (this)
		{
			_clients.Remove(client);
		}
		_synCtx.Post(delegate
		{
			this.Disconnected?.Invoke(this, new ConnectionEventArgs(client));
		}, null);
	}

	public void Dispose()
	{
		Dispose(dispose: true);
		GC.SuppressFinalize(this);
	}

	private void Dispose(bool dispose)
	{
		Stop();
	}

	public WindowsImpersonationContext Impersonate()
	{
		return ServiceSecurityContext.Current.WindowsIdentity.Impersonate();
	}

	bool IErrorHandler.HandleError(Exception error)
	{
		return true;
	}

	void IErrorHandler.ProvideFault(Exception error, MessageVersion version, ref Message fault)
	{
		FaultException ex = new FaultException<NLFaultContract>(new NLFaultContract(error), error.Message);
		MessageFault fault2 = ex.CreateMessageFault();
		fault = Message.CreateMessage(version, fault2, ex.Action);
	}

	void IServiceBehavior.AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
	{
	}

	void IServiceBehavior.ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
	{
		foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
		{
			channelDispatcher.ErrorHandlers.Add(this);
			channelDispatcher.ChannelInitializers.Add(this);
		}
	}

	void IServiceBehavior.Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
	{
	}

	void IChannelInitializer.Initialize(IClientChannel channel)
	{
		_logger.LogInformation("Client connecting: id={id}", channel.SessionId);
	}
}}
