﻿using System;
using System.Runtime.ExceptionServices;
using NLInterop;

namespace NetLimiter.Service{

public abstract class NLNode
{
	private int[] _updTickTime = new int[2] { -1, -1 };

	private NLNodeFlags _flags;

	private NLFlowValue<uint> _rate = new NLFlowValue<uint>();

	private NLFlowValue<ulong> _total = new NLFlowValue<ulong>();

	private static NLNodeChangeFlags[] _flagsTotalChanged = new NLNodeChangeFlags[2]
	{
		NLNodeChangeFlags.TotalInChanged,
		NLNodeChangeFlags.TotalOutChanged
	};

	private static NLNodeChangeFlags[] _flagsRateChanged = new NLNodeChangeFlags[2]
	{
		NLNodeChangeFlags.RateInChanged,
		NLNodeChangeFlags.RateOutChanged
	};

	private static NLNodeChangeFlags[] _flagsTotalTimeoutChanged = new NLNodeChangeFlags[2]
	{
		NLNodeChangeFlags.TotalInTimeout,
		NLNodeChangeFlags.TotalOutTimeout
	};

	private static NLNodeFlags[] _flagsTotalTimeout = new NLNodeFlags[2]
	{
		NLNodeFlags.TotalInTimeout,
		NLNodeFlags.TotalOutTimeout
	};

	public ulong Id { get; private set; }

	public ulong ParentId
	{
		get
		{
			if (Parent == null)
			{
				return 0uL;
			}
			return Parent.Id;
		}
	}

	public NLNode Parent { get; protected set; }

	public INLFlowValue<uint> TransferRate => _rate;

	public INLFlowValue<ulong> Transferred => _total;

	public bool IsClosed => _flags.HasFlag(NLNodeFlags.Closed);

	public bool IsTotalInTimeout => _flags.HasFlag(NLNodeFlags.TotalInTimeout);

	public bool IsTotalOutTimeout => _flags.HasFlag(NLNodeFlags.TotalOutTimeout);

	public int LastTickTimeIn => _updTickTime[0];

	public int LastTickTimeOut => _updTickTime[1];

	private bool IsTotalTimeout(int dir)
	{
		return _flags.HasFlag(_flagsTotalTimeout[dir]);
	}

	protected void SetClosed()
	{
		_flags |= NLNodeFlags.Closed;
	}

	internal NLNode(NodeDataWrapper data)
	{
		Id = data.GetNodeId();
	}

	internal abstract void Update(NodeLoader ldr, NodeDataWrapper data);

	internal abstract void OnRemoved(NodeLoader ldr);

	[HandleProcessCorruptedStateExceptions]
	internal void Update(NodeLoader ldr, NodeDataWrapper data, NLNodeEventArgs args)
	{
		try
		{
			args.Init(this);
			Update(ldr, data, args, 0);
			Update(ldr, data, args, 1);
		}
		catch (Exception innerException)
		{
			throw new Exception("Failed to update node data.", innerException);
		}
	}

	internal void Update(NodeLoader ldr, NodeDataWrapper data, NLNodeEventArgs args, int dir)
	{
		if (data.GetTotal(dir) == _total[dir])
		{
			long num = (int)(ldr.SvcTickTime / 10000000) - _updTickTime[dir];
			if (!IsTotalTimeout(dir) && num > 1)
			{
				_flags |= _flagsTotalTimeout[dir];
				args.Flags |= _flagsTotalTimeoutChanged[dir];
			}
			if (_rate[dir] != 0 && num > ldr.TransferRateTimeout)
			{
				_rate[dir] = 0u;
				args.Flags |= _flagsRateChanged[dir];
			}
			return;
		}
		_updTickTime[dir] = data.GetUpdTickTime(dir);
		if (IsTotalTimeout(dir))
		{
			_flags &= ~_flagsTotalTimeout[dir];
			args.Flags |= _flagsTotalTimeoutChanged[dir];
		}
		if (_total[dir] != data.GetTotal(dir))
		{
			_total[dir] = data.GetTotal(dir);
			args.Flags |= _flagsTotalChanged[dir];
		}
		if (_rate[dir] != data.GetRate(dir))
		{
			_rate[dir] = data.GetRate(dir);
			args.Flags |= _flagsRateChanged[dir];
		}
	}
}}
