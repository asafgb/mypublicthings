﻿using System;
using System.Runtime.Serialization;
using CoreLib;

namespace NetLimiter.Service{

[DataContract]
internal class NLFaultContract
{
	[DataMember]
	public byte[] ExceptionBytes { get; private set; }

	public NLFaultContract(System.Exception ex)
	{
		try
		{
			ExceptionBytes = ObjectCloner.ToBytes(ex);
		}
		catch
		{
		}
	}

	public System.Exception ToException()
	{
		try
		{
			return (System.Exception)ObjectCloner.FromBytes(ExceptionBytes);
		}
		catch
		{
			return new NLException("Failed to process the exception");
		}
	}
}}
