﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract(Name = "appTag")]
public class AppTagFilterValue
{
	[DataMember(Name = "tag")]
	private string _tag;

	public string Tag => _tag;

	public AppTagFilterValue(string tag)
	{
		if (tag.Length >= 40)
		{
			throw new ArgumentException($"Tag length greater then MAX_TAG");
		}
		_tag = tag;
	}

	public static implicit operator AppTagFilterValue(string tag)
	{
		return new AppTagFilterValue(tag);
	}

	public static implicit operator string(AppTagFilterValue tag)
	{
		return tag.Tag;
	}
}}
