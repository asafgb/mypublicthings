﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public enum StatsGroupByMethod
{
	[EnumMember]
	Time,
	[EnumMember]
	Apps,
	[EnumMember]
	IPs,
	[EnumMember]
	IPsbyApps,
	[EnumMember]
	IPsbyAppsbyPorts,
	[EnumMember]
	Connections,
	[EnumMember]
	Users,
	[EnumMember]
	Countries,
	[EnumMember]
	Histogram24Hours
}}
