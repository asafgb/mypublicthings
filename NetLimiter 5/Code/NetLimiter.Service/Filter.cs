﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
[DataContract(Name = "filter")]
[KnownType(typeof(PackageFilter))]
[KnownType(typeof(StorePackageFilter))]
[KnownType(typeof(SvcPackageFilter))]
public class Filter : NLVersioned<Filter>
{
	[DataMember(Name = "InternalId")]
	internal uint _internalId;

	[DataMember(Name = "FilterType")]
	protected FilterType _type;

	[DataMember(Name = "FunctionList")]
	protected FilterFunCollection _funs;

	[DataMember(Name = "Name")]
	internal string _name = "";

	[DataMember(Name = "BaseFilters")]
	protected List<string> _baseFilters;

	public FilterType Type
	{
		get
		{
			return _type;
		}
		set
		{
			if (_type != value)
			{
				if (_type == FilterType.Composite || value == FilterType.Composite)
				{
					throw new Exception("Can't change type from or to composite.");
				}
				_type = value;
			}
		}
	}

	[DataMember]
	public FilterPerType PerType { get; set; }

	[DataMember]
	public DateTime CreatedTime { get; set; }

	[DataMember]
	public DateTime UpdatedTime { get; set; }

	public uint InternalId => _internalId;

	public FilterFunCollection Functions => _funs;

	public int Weight { get; set; }

	public int EffectiveWeight
	{
		get
		{
			if (Weight != 0)
			{
				return Weight;
			}
			return GetDefaultWeight();
		}
	}

	public List<string> BaseFilters => _baseFilters;

	public bool IsTemporary => Functions.Exists((FilterFunction x) => x is FFConnectionNodeIdEquals || x is FFProcessIdEqual);

	public bool IsPredefined
	{
		get
		{
			if (string.Compare(base.Id, "{c053e57e-d554-49cb-ac70-e0ce95302faa}") != 0 && string.Compare(base.Id, "{2bc7c021-b058-45dc-b22f-73d8e10e3fef}") != 0 && string.Compare(base.Id, "{cbe59154-31dc-4cfb-a12c-7fffb87ff400}") != 0 && string.Compare(base.Id, "039A6E06-4A18-42C6-AEF3-9C77A960CE11") != 0)
			{
				return string.Compare(base.Id, "187C7C4F-4243-4CDF-A98C-9EA55199C196") == 0;
			}
			return true;
		}
	}

	public string Name
	{
		get
		{
			return _name;
		}
		set
		{
			if (value.Length > 600)
			{
				throw new ArgumentException(Langs.MsgFltNameTooLong);
			}
			_name = value;
		}
	}

	public int GetDefaultWeight()
	{
		if (_funs.Count == 0 && (_baseFilters == null || _baseFilters.Count == 0))
		{
			return 1;
		}
		if (Type == FilterType.Zone)
		{
			return 2;
		}
		return 3;
	}

	[OnDeserialized]
	internal void OnDeserialized(StreamingContext ctx)
	{
		Init();
	}

	protected Filter()
	{
	}

	public Filter(string name)
	{
		Init(FilterType.Filter, name);
	}

	public Filter(FilterType type, string name)
	{
		Init(type, name);
	}

	protected void Init(FilterType type, string name)
	{
		_type = type;
		name = ((name != null) ? name.Trim() : "");
		Name = name;
		Init();
	}

	protected void Init()
	{
		if (CreatedTime == default(DateTime) || UpdatedTime == default(DateTime))
		{
			DateTime createdTime = (UpdatedTime = DateTime.UtcNow);
			CreatedTime = createdTime;
		}
		if (_funs == null)
		{
			_funs = new FilterFunCollection();
		}
		if (Type == FilterType.Composite)
		{
			if (_baseFilters == null)
			{
				_baseFilters = new List<string>();
			}
		}
		else
		{
			_baseFilters = null;
		}
		if (Weight > Rule.MaxRuleWeight)
		{
			Weight = Rule.MaxRuleWeight;
		}
		if (Weight < 0)
		{
			Weight = 0;
		}
	}
}}
