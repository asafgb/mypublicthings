﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NetLimiter.Service{

public class NLProtoIp
{
	private static List<NLProtoIp> _protocols;

	public static readonly NLProtoIp ProtoNone;

	public static readonly NLProtoIp ProtoIcmpV4;

	public static readonly NLProtoIp ProtoIcmpV6;

	public static readonly NLProtoIp ProtoIgmp;

	public static readonly NLProtoIp ProtoTcp;

	public static readonly NLProtoIp ProtoUdp;

	public int Number { get; protected set; }

	public string Name { get; protected set; }

	public static IEnumerable<NLProtoIp> Protocols => _protocols;

	static NLProtoIp()
	{
		_protocols = new List<NLProtoIp>();
		ProtoNone = new NLProtoIp(0, "None");
		ProtoIcmpV4 = new NLProtoIp(1, "ICMPv4");
		ProtoIcmpV6 = new NLProtoIp(58, "ICMPv6");
		ProtoIgmp = new NLProtoIp(2, "IGMP");
		ProtoTcp = new NLProtoIp(6, "TCP");
		ProtoUdp = new NLProtoIp(17, "UDP");
		foreach (FieldInfo item2 in from x in typeof(NLProtoIp).GetFields(BindingFlags.Static | BindingFlags.Public)
			where x.FieldType == typeof(NLProtoIp)
			select x)
		{
			if (item2.GetValue(null) is NLProtoIp item)
			{
				_protocols.Add(item);
			}
		}
	}

	public NLProtoIp(int number, string name)
	{
		Number = number;
		Name = name;
	}

	public override string ToString()
	{
		return Name;
	}
}}
