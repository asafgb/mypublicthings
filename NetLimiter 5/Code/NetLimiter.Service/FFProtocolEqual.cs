﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FFProtocolEqual : FilterFunctionT<ushort>
{
	public FFProtocolEqual()
	{
	}

	public FFProtocolEqual(ushort id)
	{
		base.Values.Add(id);
	}
}}
