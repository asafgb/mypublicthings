﻿namespace NetLimiter.Service{

public class StatsResultValueY
{
	public ulong DataIn { get; set; }

	public ulong DataOut { get; set; }

	public ulong DataInOH { get; set; }

	public ulong DataOutOH { get; set; }
}}
