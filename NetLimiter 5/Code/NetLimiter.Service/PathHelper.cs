﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace NetLimiter.Service{

public static class PathHelper
{
	public const string StoreAppPattern = "?:\\program files\\windowsapps\\*_*_*";

	public static bool IsEqualVersionIgnore(string path1, string path2)
	{
		if (!IsStorePath(path1, out var ver1ix, out var ver2ix))
		{
			return string.Equals(path1, path2, StringComparison.InvariantCultureIgnoreCase);
		}
		if (!IsStorePath(path2, out var ver1ix2, out var ver2ix2))
		{
			return false;
		}
		if (ver1ix != ver1ix2)
		{
			return false;
		}
		int num = 0;
		int num2 = 0;
		bool flag;
		bool flag2;
		do
		{
			if (char.ToLowerInvariant(path1[num++]) != char.ToLowerInvariant(path2[num2++]))
			{
				return false;
			}
			if (num == ver1ix)
			{
				num = ver2ix + 1;
				num2 = ver2ix2 + 1;
			}
			flag = path1.Length == num;
			flag2 = path2.Length == num2;
		}
		while (!(flag || flag2));
		return flag && flag2;
	}

	public static string ReplaceVersionWithAsterisk(string path)
	{
		if (IsStorePath(path, out var ver1ix, out var ver2ix))
		{
			StringBuilder stringBuilder = new StringBuilder(path.Length - (ver2ix - ver1ix));
			for (int i = 0; i < path.Length; i++)
			{
				if (i == ver1ix)
				{
					stringBuilder.Append('*');
				}
				if (i < ver1ix || i > ver2ix)
				{
					stringBuilder.Append(path[i]);
				}
			}
			return stringBuilder.ToString();
		}
		return path;
	}

	public static bool IsStorePath(string path, out Version ver)
	{
		if (IsStorePath(path, out var ver1ix, out var ver2ix) && Version.TryParse(path.Substring(ver1ix, ver2ix - ver1ix + 1), out ver))
		{
			return true;
		}
		ver = null;
		return false;
	}

	public static bool IsStorePath(string path)
	{
		int ver1ix;
		int ver2ix;
		return IsStorePath(path, out ver1ix, out ver2ix);
	}

	public static bool IsStorePath(string path, out int ver1ix, out int ver2ix)
	{
		ver1ix = (ver2ix = -1);
		if (!IsWildEqual(path, "?:\\program files\\windowsapps\\*_*_*"))
		{
			return false;
		}
		for (int i = 0; i < path.Length; i++)
		{
			if (path[i] == '_')
			{
				if (ver1ix == -1)
				{
					ver1ix = i + 1;
				}
				else if (ver2ix == -1)
				{
					ver2ix = i - 1;
					return ver1ix <= ver2ix;
				}
			}
		}
		return false;
	}

	public static bool IsWildEqual(string path, string pattern)
	{
		if (path == null || pattern == null)
		{
			return false;
		}
		int num = -1;
		int num2 = -1;
		int num3 = 0;
		int i = 0;
		while (num3 < path.Length)
		{
			bool flag = pattern.Length == i;
			if (!flag && (char.ToLowerInvariant(path[num3]) == char.ToLowerInvariant(pattern[i]) || pattern[i] == '?'))
			{
				num3++;
				i++;
				continue;
			}
			if (!flag && pattern[i] == '*')
			{
				num = num3;
				num2 = ++i;
				continue;
			}
			if (num >= 0)
			{
				num3 = ++num;
				i = num2;
				continue;
			}
			return false;
		}
		for (; i < pattern.Length && pattern[i] == '*'; i++)
		{
		}
		return i == pattern.Length;
	}

	public static bool ContainsAny(string path, params string[] parts)
	{
		return parts.Any((string part) => Contains(path, part));
	}

	public static bool Contains(string path, string part)
	{
		if (path == null || part == null)
		{
			return path == part;
		}
		return 0 <= path.IndexOf(part, 0, StringComparison.InvariantCultureIgnoreCase);
	}

	public static string GetFileName(string path)
	{
		if (path == null)
		{
			return null;
		}
		int num = path.LastIndexOf(Path.DirectorySeparatorChar);
		if (num == -1)
		{
			return path;
		}
		if (num == path.Length - 1)
		{
			return "";
		}
		return path.Substring(num + 1);
	}
}}
