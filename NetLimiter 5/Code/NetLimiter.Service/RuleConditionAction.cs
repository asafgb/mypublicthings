﻿namespace NetLimiter.Service{

public enum RuleConditionAction
{
	Stop,
	Start,
	Delete,
	Reset
}}
