﻿using System;

namespace NetLimiter.Service{

[Serializable]
public enum OptionsChangeType
{
	StatsEnabled,
	FwEnabled,
	LimiterEnabled,
	DefaultFwAction,
	PrioritiesEnabled
}}
