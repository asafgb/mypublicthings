﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[CollectionDataContract(Name = "filterValues", ItemName = "value")]
public class FilterValueCollection<T> : List<T>
{
}}
