﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class EmptyFilterNameException : NLException
{
	public EmptyFilterNameException()
		: base(string.Format(Langs.MsgEmptyFilterName))
	{
	}

	public EmptyFilterNameException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
