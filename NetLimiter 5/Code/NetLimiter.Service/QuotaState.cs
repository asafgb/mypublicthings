﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class QuotaState
{
	[DataMember]
	public string QuotaRuleId { get; set; }

	[DataMember]
	public int IsOverflowCount { get; set; }

	public bool IsOverflow => IsOverflowCount > 0;

	[DataMember]
	public bool IsAnswered { get; set; }

	[DataMember]
	public long Total { get; set; }
}}
