﻿namespace NetLimiter.Service{

public interface INLFlowValue<T>
{
	T this[NLFlowDir dir] { get; }

	T this[int dir] { get; }

	T In { get; }

	T Out { get; }
}}
