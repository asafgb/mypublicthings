﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2ResultDataUser
{
	[DataMember]
	public ulong In;

	[DataMember]
	public ulong Out;

	[DataMember]
	public string Sid;

	[DataMember]
	public ushort InternalId;
}}
