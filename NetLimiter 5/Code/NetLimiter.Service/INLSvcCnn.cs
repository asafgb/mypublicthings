﻿using System;

namespace NetLimiter.Service{

internal interface INLSvcCnn : IDisposable
{
	INLService Svc { get; }

	string SvcAddress { get; }

	NLServiceState State { get; }

	Exception ConnectError { get; }

	event EventHandler<NLServiceStateChangedEventArgs> StateChanged;

	ServiceInfo Connect();

	void Close();
}}
