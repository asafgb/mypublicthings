﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class ChartData
{
	[DataMember]
	private uint[][] _data;

	[DataMember]
	public DateTime UtcTime { get; set; }

	[DataMember]
	public long TimeSinceStart { get; set; }

	public uint[][] Data => _data;

	public ChartData()
	{
		_data = new uint[2][];
	}

	public uint[] GetData(NLFlowDir? dir = null)
	{
		if (!dir.HasValue)
		{
			return _data[0];
		}
		return _data[(int)dir.Value];
	}
}}
