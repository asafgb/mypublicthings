﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class StatsBadRowsListInfo
{
	[DataMember]
	public List<StatsBadRows> BadRowsListV4 = new List<StatsBadRows>();

	[DataMember]
	public List<StatsBadRows> BadRowsListV6 = new List<StatsBadRows>();

	[DataMember]
	public string Path = "";

	public int Count()
	{
		return BadRowsListV4.Count + BadRowsListV6.Count;
	}

	public void Clear()
	{
		if (BadRowsListV4 != null)
		{
			BadRowsListV4.Clear();
		}
		if (BadRowsListV6 != null)
		{
			BadRowsListV6.Clear();
		}
		Path = "";
	}
}}
