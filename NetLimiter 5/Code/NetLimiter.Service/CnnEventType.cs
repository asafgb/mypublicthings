﻿namespace NetLimiter.Service{

public enum CnnEventType
{
	NoEvent,
	Connect,
	Accept,
	Listen,
	Close
}}
