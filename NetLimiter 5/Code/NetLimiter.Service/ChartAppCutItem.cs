﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class ChartAppCutItem
{
	[DataMember]
	public AppId AppId { get; set; }

	[DataMember]
	public ulong DataIn { get; set; }

	[DataMember]
	public ulong DataOut { get; set; }
}}
