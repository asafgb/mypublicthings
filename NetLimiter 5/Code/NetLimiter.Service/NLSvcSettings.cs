﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Linq;
using CoreLib;
using Microsoft.Extensions.Logging;
using NetLimiter.Service.AppList;
using NetLimiter.Service.Notifications;

namespace NetLimiter.Service{

[DataContract]
public class NLSvcSettings
{
	public const int CurrentVersion = 21;

	private static readonly ILogger<NLSvcSettings> _logger = Logging.LoggerFactory.CreateLogger<NLSvcSettings>();

	[DataMember]
	public int Version = 21;

	[DataMember]
	public List<Filter> Filters;

	[DataMember]
	public List<Rule> Rules;

	[DataMember]
	public bool UseTcpBinding;

	[DataMember]
	public string TcpAddress;

	[DataMember]
	public short TcpPort;

	[DataMember]
	public bool UseNpBinding;

	[DataMember]
	public string VTAPIKey;

	[DataMember]
	public bool UseVTFileChecks;

	[DataMember]
	public bool IsStatsEnabled;

	[DataMember]
	public bool? StatsSaveInternet;

	[DataMember]
	public bool? StatsSaveLoopback;

	[DataMember]
	public bool? StatsSaveLocal;

	[DataMember]
	public int StatsUpdateTime;

	[DataMember]
	public bool IsFwEnabled;

	[DataMember]
	public bool IsLimiterEnabled;

	[DataMember]
	public bool IsPriorityEnabled;

	[DataMember]
	public bool UseDnsFiltering;

	[DataMember]
	public string StatsFolderName;

	[DataMember]
	public string StatsFolderNameNL4;

	[DataMember]
	public bool SaveOnExit;

	[DataMember]
	public List<Network> Networks;

	[DataMember]
	public FwAction DefaultFwActionIn;

	[DataMember]
	public FwAction DefaultFwActionOut;

	[DataMember]
	public PrioritySettings PrioritySettings;

	[DataMember]
	public List<AppInfo> AppInfos;

	[DataMember]
	public bool RequireElevationLocal;

	[DataMember]
	public List<string> ApiAccessList;

	[DataMember]
	public bool RunClientOnFwRequest;

	[DataMember]
	public List<ServiceNotification> Notifications;

	public string Path { get; set; }

	internal bool UpgradeToNewestVersion()
	{
		bool result = false;
		for (int i = Version + 1; i <= 21; i++)
		{
			MethodInfo method = GetType().GetMethod($"UpgradeToVersion{i}", BindingFlags.Instance | BindingFlags.NonPublic);
			if ((object)method != null)
			{
				_logger.LogInformation("Upgrading settings: targetVersion={version}", i);
				try
				{
					method.Invoke(this, null);
					result = true;
				}
				catch
				{
					_logger.LogError("Failed to upgrade settings: version={version}", i);
					throw;
				}
				_logger.LogInformation("Settings upgraded: version={version}", i);
			}
		}
		Version = 21;
		return result;
	}

	internal static bool UpgradeXml(XElement xml, int version)
	{
		bool result = false;
		for (int i = version + 1; i <= 21; i++)
		{
			MethodInfo method = typeof(NLSvcSettings).GetMethod($"UpgradeToXmlVersion{i}", BindingFlags.Static | BindingFlags.NonPublic);
			if ((object)method != null)
			{
				_logger.LogInformation("Upgrading settings xml: targetVersion={version}", i);
				try
				{
					object[] parameters = new XElement[1] { xml };
					method.Invoke(null, parameters);
				}
				catch
				{
					_logger.LogError("Failed to upgrade settings xml: version={version}", i);
					throw;
				}
				result = true;
				_logger.LogInformation("Settings xml upgraded: version={version}", i);
			}
		}
		return result;
	}

	private static void UpgradeToXmlVersion4(XElement xml)
	{
		foreach (XElement item in from el in xml.Descendants()
			where el.Name.LocalName == "Priority"
			where el.Parent.Name.LocalName == "rule"
			select el)
		{
			if (int.TryParse(item.Value, out var result))
			{
				if (result <= 5)
				{
					item.Value = FlowPriority.High.ToString();
				}
				else
				{
					item.Value = FlowPriority.Critical.ToString();
				}
			}
		}
	}

	private static void UpgradeToXmlVersion7(XElement xml)
	{
		foreach (XElement item in from el in xml.Descendants()
			where el.Name.LocalName == "Weight"
			where el.Parent.Name.LocalName == "rule"
			select el)
		{
			if (string.Compare(item.Value, "none", ignoreCase: true) == 0)
			{
				item.Value = "0";
			}
			else if (string.Compare(item.Value, "one", ignoreCase: true) == 0)
			{
				item.Value = "1";
			}
			else if (string.Compare(item.Value, "two", ignoreCase: true) == 0)
			{
				item.Value = "2";
			}
			else if (string.Compare(item.Value, "three", ignoreCase: true) == 0)
			{
				item.Value = "3";
			}
			else if (string.Compare(item.Value, "four", ignoreCase: true) == 0)
			{
				item.Value = "4";
			}
			else if (string.Compare(item.Value, "five", ignoreCase: true) == 0)
			{
				item.Value = "5";
			}
		}
	}

	private void UpgradeToVersion2()
	{
		foreach (Filter filter in Filters)
		{
			if (filter.TryGetAppPath(out var path) && path != null)
			{
				filter.Name = FilterExtensions.CreateFilterNameFromPath(path);
			}
		}
	}

	private void UpgradeToVersion3()
	{
		if (!string.IsNullOrEmpty(Path))
		{
			try
			{
				string text = File.ReadAllText(Path);
				if (text.Contains("<DefaultFwAction>Ask</DefaultFwAction>"))
				{
					DefaultFwActionIn = (DefaultFwActionOut = FwAction.Ask);
				}
				else if (text.Contains("<DefaultFwAction>Deny</DefaultFwAction>"))
				{
					DefaultFwActionIn = (DefaultFwActionOut = FwAction.Deny);
				}
			}
			catch
			{
			}
		}
		Filter oldFltAny = Filters.FirstOrDefault((Filter x) => x.Name == "any");
		if (oldFltAny == null)
		{
			return;
		}
		foreach (Rule item in Rules.Where((Rule x) => x.FilterId == oldFltAny.Id))
		{
			item.FilterId = "{c053e57e-d554-49cb-ac70-e0ce95302faa}";
		}
		foreach (Filter item2 in Filters.Where((Filter x) => x.BaseFilters != null && x.BaseFilters.Any((string y) => y == oldFltAny.Id)))
		{
			item2.BaseFilters.Remove(oldFltAny.Id);
			item2.BaseFilters.Insert(0, "{c053e57e-d554-49cb-ac70-e0ce95302faa}");
		}
		Filters.Remove(oldFltAny);
	}

	private void UpgradeToVersion4()
	{
	}

	private void UpgradeToVersion5()
	{
		IEnumerable<AppListStoreItem> source = from x in AppListCreator.GetStoreApps()
			where !string.IsNullOrEmpty(x.Folder)
			select x;
		foreach (Filter filter in Filters)
		{
			if (filter.TryGetAppPath(out var path) && !string.IsNullOrEmpty(path))
			{
				AppListStoreItem appListStoreItem = source.FirstOrDefault((AppListStoreItem x) => path.StartsWith(x.Folder, StringComparison.InvariantCultureIgnoreCase));
				if (appListStoreItem != null)
				{
					AppId id = new AppId(path, appListStoreItem.Sid);
					filter.Functions.Clear();
					filter.Functions.Add(new FFAppIdEqual(id));
				}
				else
				{
					AppId id2 = new AppId(path);
					filter.Functions.Clear();
					filter.Functions.Add(new FFAppIdEqual(id2));
				}
			}
		}
	}

	private void UpgradeToVersion6()
	{
		foreach (Filter filter in Filters)
		{
			foreach (FwRule item in (from x in UpgradeToVersion6_GetFwRules(filter)
				where x.Action == FwAction.Block
				select x).ToList())
			{
				Rules.Remove(item);
			}
			UpgradeToVersion6_FixRuleList(filter, RuleDir.In);
			UpgradeToVersion6_FixRuleList(filter, RuleDir.Out);
			UpgradeToVersion6_FixRuleList(filter, RuleDir.Both);
		}
	}

	private IEnumerable<FwRule> UpgradeToVersion6_GetFwRules(Filter flt)
	{
		return from x in Rules.OfType<FwRule>().Where(delegate(FwRule x)
			{
				List<RuleCondition> conditions = x.Conditions;
				return conditions != null && conditions.Count == 0;
			})
			where string.Equals(x.FilterId, flt.Id, StringComparison.InvariantCultureIgnoreCase)
			select x;
	}

	private void UpgradeToVersion6_FixRuleList(Filter flt, RuleDir dir)
	{
		List<FwRule> list = (from x in UpgradeToVersion6_GetFwRules(flt)
			where x.Dir == dir
			select x).ToList();
		if (list.Count <= 1)
		{
			return;
		}
		list.RemoveAt(list.Count - 1);
		foreach (FwRule item in list)
		{
			Rules.Remove(item);
		}
	}

	private void UpgradeToVersion8()
	{
		List<Network> list = new List<Network>();
		foreach (Network network in Networks)
		{
			if (!Filters.Any((Filter flt) => flt.IsNtwFilter(network)))
			{
				list.Add(network);
			}
			else
			{
				network.CompareFlags = NetworkCompareFlags.Ssid | NetworkCompareFlags.IfcName;
			}
		}
		foreach (Network item in list)
		{
			Networks.Remove(item);
		}
		foreach (AppInfo item2 in AppInfos.Where((AppInfo x) => x.IsDead).ToList())
		{
			AppInfos.Remove(item2);
		}
		foreach (AppInfo appInfo in AppInfos)
		{
			appInfo.UpdateInfo();
		}
		foreach (StorePackageFilter item3 in Filters.OfType<StorePackageFilter>())
		{
			try
			{
				item3.UpdateInfo();
			}
			catch (ManifestNotFoundException)
			{
			}
			catch (PackageNotFoundException)
			{
			}
			catch (System.Exception exception)
			{
				_logger.LogError(exception, "Failed to update store package");
			}
		}
		foreach (Filter item4 in Filters.Where((Filter x) => x.IsDead()).ToList())
		{
			Filters.Remove(item4);
		}
	}

	private void UpgradeToVersion9()
	{
		RemoveLostCompositeFilters();
		RemoveLostRules();
	}

	private void UpgradeToVersion10()
	{
		RequireElevationLocal = true;
		RunClientOnFwRequest = true;
	}

	private void UpgradeToVersion11()
	{
		UseDnsFiltering = true;
	}

	private void UpgradeToVersion20()
	{
		if (string.IsNullOrEmpty(StatsFolderName))
		{
			return;
		}
		string text = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Locktime\\NetLimiter\\5\\Ip2Loc");
		string path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Locktime\\NetLimiter\\4\\Ip2Loc");
		try
		{
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			DirectoryInfo directoryInfo = new DirectoryInfo(path);
			if (!directoryInfo.Exists)
			{
				throw new DirectoryNotFoundException("Source directory not found: " + directoryInfo.FullName);
			}
			directoryInfo.GetDirectories();
			Directory.CreateDirectory(text);
			FileInfo[] files = directoryInfo.GetFiles();
			foreach (FileInfo fileInfo in files)
			{
				string destFileName = System.IO.Path.Combine(text, fileInfo.Name);
				fileInfo.CopyTo(destFileName);
			}
		}
		catch (System.Exception ex)
		{
			_logger.LogError("Cant copy files to Ip2Loc folder ({0}); Error: {1}", text, ex.Message);
		}
		_logger.LogInformation("Stats Folder:{path}; new path ({ppp}) {esists}", StatsFolderName, text, Directory.Exists(text) ? "exists" : "not exists");
		if (!StatsFolderName.Contains("Locktime\\NetLimiter\\4"))
		{
			_logger.LogInformation("Stats Folder {0} copied to StatsFolderNameNL4.", StatsFolderName);
			StatsFolderNameNL4 = StatsFolderName;
		}
		else
		{
			_logger.LogInformation("Stats Folder set to default (null).");
			StatsFolderName = null;
		}
	}

	private void UpgradeToVersion21()
	{
		UseDnsFiltering = true;
	}

	public NLSvcSettings()
	{
		UseNpBinding = true;
		UseTcpBinding = false;
		TcpPort = 4045;
		TcpAddress = "localhost";
		SaveOnExit = true;
		IsStatsEnabled = true;
		IsFwEnabled = true;
		IsLimiterEnabled = true;
		IsPriorityEnabled = true;
		RequireElevationLocal = true;
		RunClientOnFwRequest = true;
		UseDnsFiltering = true;
		InitInternal();
	}

	public static string GetDefaultPath()
	{
		return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Locktime\\NetLimiter\\5", "nl_settings.xml");
	}

	public static string GetLogPath()
	{
		return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Locktime\\NetLimiter\\5", "logs");
	}

	public void Save(string path)
	{
		lock (this)
		{
			_logger.LogTrace("Saving to: {path}", path);
			DCSerializer.Save(this, path);
		}
	}

	public static NLSvcSettings Load(string path)
	{
		XElement xElement;
		try
		{
			xElement = XElement.Load(path);
		}
		catch
		{
			_logger.LogError("Failed to load settings xml: {0}", path);
			throw;
		}
		string s = (from el in xElement.Elements()
			where el.Name.LocalName == "Version"
			select el).FirstOrDefault()?.Value;
		int result = 0;
		if (int.TryParse(s, out result) && result < 21)
		{
			string text = path + $".v{result}.backup";
			try
			{
				File.Copy(path, text, overwrite: true);
				_logger.LogInformation("Old settings backuped: version={version}, path={path}", result, text);
			}
			catch (System.Exception exception)
			{
				_logger.LogError(exception, "Failed to save copy of settings on upgrade: path={path}", text);
			}
			if (UpgradeXml(xElement, result))
			{
				try
				{
					xElement.Save(path);
				}
				catch
				{
					_logger.LogError("Failed to save upgraded settings xml: {0}", path);
					throw;
				}
			}
		}
		NLSvcSettings nLSvcSettings;
		try
		{
			nLSvcSettings = Deserialize(path);
		}
		catch
		{
			_logger.LogError("Failed to deserialize settings xml: {0}", path);
			throw;
		}
		if (nLSvcSettings.UpgradeToNewestVersion())
		{
			nLSvcSettings.Save(path);
		}
		return nLSvcSettings;
	}

	private static NLSvcSettings Deserialize(string path)
	{
		using FileStream stream = new FileStream(path, FileMode.Open);
		NLSvcSettings obj = (NLSvcSettings)new DataContractSerializer(typeof(NLSvcSettings)).ReadObject(stream);
		obj.Path = path;
		obj.InitInternal();
		return obj;
	}

	public NLSvcSettings Clone()
	{
		using MemoryStream memoryStream = new MemoryStream();
		DataContractSerializer dataContractSerializer = new DataContractSerializer(typeof(NLSvcSettings));
		dataContractSerializer.WriteObject(memoryStream, this);
		memoryStream.Seek(0L, SeekOrigin.Begin);
		return (NLSvcSettings)dataContractSerializer.ReadObject(memoryStream);
	}

	private void InitInternal()
	{
		_logger.LogTrace(">>");
		if (!StatsSaveInternet.HasValue)
		{
			StatsSaveInternet = true;
		}
		if (!StatsSaveLocal.HasValue)
		{
			StatsSaveLocal = false;
		}
		if (!StatsSaveLoopback.HasValue)
		{
			StatsSaveLoopback = false;
		}
		if (Filters == null)
		{
			Filters = new List<Filter>();
		}
		if (StatsUpdateTime == 0)
		{
			StatsUpdateTime = 180;
		}
		if (StatsFolderName == null)
		{
			StatsFolderName = "";
		}
		StatsFolderName = StatsFolderName.Trim();
		if (Networks == null)
		{
			Networks = new List<Network>();
		}
		if (Rules == null)
		{
			Rules = new List<Rule>();
		}
		if (DefaultFwActionIn == FwAction.None)
		{
			DefaultFwActionIn = FwAction.Allow;
		}
		if (DefaultFwActionOut == FwAction.None)
		{
			DefaultFwActionOut = FwAction.Allow;
		}
		if (PrioritySettings == null)
		{
			PrioritySettings = new PrioritySettings();
		}
		if (AppInfos == null)
		{
			AppInfos = new List<AppInfo>();
		}
		if (ApiAccessList == null)
		{
			ApiAccessList = new List<string>();
		}
		if (Notifications == null)
		{
			Notifications = new List<ServiceNotification>();
		}
		else
		{
			Notifications.RemoveAll((ServiceNotification x) => x.IsManaged);
		}
		AddDefaultZones();
		AddDefaultFiltersAndRules();
		try
		{
			UpdateStorePackges();
		}
		catch (System.Exception exception)
		{
			_logger.LogError(exception, "Failed to update store packages");
		}
		_logger.LogTrace("<<");
	}

	public void AddDefaultFiltersAndRules()
	{
		if (!Filters.Exists((Filter x) => string.Compare(x.Id, "039A6E06-4A18-42C6-AEF3-9C77A960CE11", ignoreCase: true) == 0))
		{
			Filter filter = new Filter(FilterType.Filter, "Ignore Traffic");
			filter._id = "039A6E06-4A18-42C6-AEF3-9C77A960CE11";
			FFTagEqual item = new FFTagEqual("Ignore Traffic");
			filter.Functions.Add(item);
			Filters.Add(filter);
			_logger.LogTrace("Ignore Traffic default filter added.");
		}
		if (!Rules.Exists((Rule x) => string.Compare(x.Id, "518A6EE0-FF49-4515-A804-53E110F45AAD", ignoreCase: true) == 0))
		{
			IgnoreRule ignoreRule = new IgnoreRule(RuleDir.Both);
			ignoreRule.IgnoreData = true;
			ignoreRule._id = "518A6EE0-FF49-4515-A804-53E110F45AAD";
			ignoreRule.FilterId = "039A6E06-4A18-42C6-AEF3-9C77A960CE11";
			Rules.Add(ignoreRule);
			_logger.LogTrace("Ignore Traffic default rule added.");
		}
		if (!Filters.Exists((Filter x) => string.Compare(x.Id, "187C7C4F-4243-4CDF-A98C-9EA55199C196", ignoreCase: true) == 0))
		{
			Filter filter2 = new Filter(FilterType.Filter, "Ignore Limits");
			filter2._id = "187C7C4F-4243-4CDF-A98C-9EA55199C196";
			FFTagEqual item2 = new FFTagEqual("Ignore Limits");
			filter2.Functions.Add(item2);
			Filters.Add(filter2);
			_logger.LogTrace("Ignore Limits default filter added.");
		}
		if (!Rules.Exists((Rule x) => string.Compare(x.Id, "DDFACCF7-9CA5-4C81-B9EE-4788B1274597", ignoreCase: true) == 0))
		{
			IgnoreRule ignoreRule2 = new IgnoreRule(RuleDir.Both);
			ignoreRule2.IgnoreLimit = true;
			ignoreRule2._id = "DDFACCF7-9CA5-4C81-B9EE-4788B1274597";
			ignoreRule2.FilterId = "187C7C4F-4243-4CDF-A98C-9EA55199C196";
			Rules.Add(ignoreRule2);
			_logger.LogTrace("Ignore Limits default rule added.");
		}
	}

	public void AddDefaultZones()
	{
		if (!Filters.Exists((Filter x) => string.Compare(x.Id, "{c053e57e-d554-49cb-ac70-e0ce95302faa}", ignoreCase: true) == 0))
		{
			Filter filter = new Filter(FilterType.Filter, "Any");
			filter._id = "{c053e57e-d554-49cb-ac70-e0ce95302faa}";
			Filters.Insert(0, filter);
		}
		if (!Filters.Exists((Filter x) => string.Compare(x.Id, "{2bc7c021-b058-45dc-b22f-73d8e10e3fef}", ignoreCase: true) == 0))
		{
			Filter filter2 = new Filter(FilterType.Zone, "Internet");
			filter2._id = "{2bc7c021-b058-45dc-b22f-73d8e10e3fef}";
			filter2.Functions.Add(new FFIsInternetTraffic());
			Filters.Insert(0, filter2);
		}
		if (!Filters.Exists((Filter x) => string.Compare(x.Id, "{cbe59154-31dc-4cfb-a12c-7fffb87ff400}", ignoreCase: false) == 0))
		{
			Filter filter3 = new Filter(FilterType.Zone, "LocalNetwork");
			filter3._id = "{cbe59154-31dc-4cfb-a12c-7fffb87ff400}";
			filter3.Functions.Add(new FFIsLocalNetworkTraffic());
			Filters.Insert(0, filter3);
		}
	}

	public void UpdateRule(Rule rule)
	{
		Rule item = Rules.FirstOrDefault((Rule x) => x.Id == rule.Id);
		Rules[Rules.IndexOf(item)] = rule;
	}

	public Filter ConvertFilter(Filter flt)
	{
		if (flt == null)
		{
			return null;
		}
		Filter filter = flt;
		if (flt.GetAppFilterAppId(out var appId))
		{
			if (appId.IsStorePackage)
			{
				if (Filters.OfType<StorePackageFilter>().Any((StorePackageFilter x) => appId.Sid.Equals(x.Sid)))
				{
					return flt;
				}
				filter = new StorePackageFilter(appId)
				{
					_id = flt.Id
				};
				if (!string.IsNullOrEmpty(flt.Name))
				{
					filter.Name = flt.Name;
				}
			}
			else if (appId.IsSvcPackage)
			{
				if (Filters.OfType<SvcPackageFilter>().Any((SvcPackageFilter x) => appId.Sid.Equals(x.Sid)))
				{
					return flt;
				}
				filter = new SvcPackageFilter(appId.Sid)
				{
					_id = flt.Id
				};
				if (!string.IsNullOrEmpty(flt.Name))
				{
					filter.Name = flt.Name;
				}
			}
		}
		return filter;
	}

	public void CheckCanDeleteFilter(Filter flt)
	{
		if (!(flt is StorePackageFilter storePackageFilter))
		{
			return;
		}
		try
		{
			if (StoreAppHelper.GetStoreApp(storePackageFilter.Sid) == null)
			{
				return;
			}
		}
		catch
		{
			return;
		}
		throw new System.Exception("Can't delete Store package which is still installed.");
	}

	public bool RemoveApps(AppId appId)
	{
		if (appId == null)
		{
			return false;
		}
		List<AppInfo> list = AppInfos.Where((AppInfo x) => appId.Equals(x.AppId)).ToList();
		foreach (AppInfo item in list)
		{
			AppInfos.Remove(item);
		}
		return list.Any();
	}

	private void UpdateStorePackges()
	{
		foreach (StorePackageFilter item in Filters.OfType<StorePackageFilter>())
		{
			if (item.PackageFolder == null || Directory.Exists(item.PackageFolder))
			{
				continue;
			}
			foreach (string item2 in Directory.EnumerateDirectories(System.IO.Path.GetDirectoryName(item.PackageFolder)))
			{
				if (PathHelper.IsEqualVersionIgnore(item2, item.PackageFolder))
				{
					item.PackageFolder = item2;
					try
					{
						item.UpdateInfo(new AppInfoCtx());
					}
					catch (System.Exception exception)
					{
						_logger.LogTrace(exception, "Failed to update store package: {path}", item2);
					}
				}
			}
		}
	}

	internal void RemoveLostCompositeFilters()
	{
		List<Filter> list = new List<Filter>();
		foreach (Filter item in Filters.Where(delegate(Filter x)
		{
			List<string> baseFilters = x.BaseFilters;
			return baseFilters != null && baseFilters.Count > 0;
		}))
		{
			foreach (string baseFltId in item.BaseFilters)
			{
				if (!Filters.Any((Filter x) => x.Id == baseFltId))
				{
					list.Add(item);
					break;
				}
			}
		}
		foreach (Filter item2 in list)
		{
			Filters.Remove(item2);
		}
	}

	internal void RemoveLostRules()
	{
		List<Rule> list = new List<Rule>();
		foreach (Rule rule in Rules)
		{
			if (!Filters.Any((Filter x) => x.Id == rule.FilterId))
			{
				list.Add(rule);
			}
		}
		foreach (Rule item in list)
		{
			Rules.Remove(item);
		}
	}
}}
