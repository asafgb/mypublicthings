﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
public class FilterUpdateInfo
{
	[DataMember]
	public Filter Filter { get; private set; }

	[DataMember]
	public bool HasChange { get; set; }

	public FilterUpdateInfo(Filter filter, bool hasChange)
	{
		if (filter == null)
		{
			throw new ArgumentNullException();
		}
		Filter = filter;
		HasChange = hasChange;
	}
}}
