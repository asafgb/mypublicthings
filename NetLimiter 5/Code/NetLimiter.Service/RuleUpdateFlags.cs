﻿using System;

namespace NetLimiter.Service{

[Flags]
public enum RuleUpdateFlags
{
	None = 0,
	QuotaOverflow = 1
}}
