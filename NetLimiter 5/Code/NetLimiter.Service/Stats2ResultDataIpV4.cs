﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2ResultDataIpV4
{
	[DataMember]
	public ulong In;

	[DataMember]
	public ulong Out;
}}
