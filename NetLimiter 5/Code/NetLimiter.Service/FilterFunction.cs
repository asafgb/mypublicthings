﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[DataContract]
[KnownType(typeof(FFPathEqual))]
[KnownType(typeof(FFPathContains))]
[KnownType(typeof(FFLocalAddressInRange))]
[KnownType(typeof(FFRemoteAddressInRange))]
[KnownType(typeof(FFLocalPortInRange))]
[KnownType(typeof(FFRemotePortInRange))]
[KnownType(typeof(FFProcessIdEqual))]
[KnownType(typeof(FFConnectionNodeIdEquals))]
[KnownType(typeof(FFNetworkIxEquals))]
[KnownType(typeof(FFIsInternetTraffic))]
[KnownType(typeof(FFIsLocalNetworkTraffic))]
[KnownType(typeof(FFIsLoopbackTraffic))]
[KnownType(typeof(FFProtocolEqual))]
[KnownType(typeof(FFUserEqual))]
[KnownType(typeof(FFAppIdEqual))]
[KnownType(typeof(FFTagEqual))]
[KnownType(typeof(FFDomainNameEqual))]
[KnownType(typeof(FFIsForwardTraffic))]
public class FilterFunction
{
	[DataMember(Name = "match")]
	public bool IsMatch = true;
}}
