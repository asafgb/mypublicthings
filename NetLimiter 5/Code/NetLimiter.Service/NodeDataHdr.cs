﻿using System;

namespace NetLimiter.Service{

[Serializable]
internal class NodeDataHdr
{
	public const uint DefRetBufferSize = 1048576u;

	public uint MaxRetBufferSize = 1048576u;

	public ulong ReqId;

	public NodeDataGrpIntr Tops = new NodeDataGrpIntr();

	public NodeDataGrpIntr Flts = new NodeDataGrpIntr();

	public NodeDataGrpIntr Apps = new NodeDataGrpIntr();

	public NodeDataGrpIntr Insts = new NodeDataGrpIntr();

	public NodeDataGrpIntr Cnns = new NodeDataGrpIntr();

	public bool IsComplete;

	public uint Transferred;

	public uint TotalSize;

	public long TickTime;

	public bool IsBfeRunning;

	public void Reset()
	{
		Transferred = 0u;
		TotalSize = 0u;
		IsComplete = false;
		IsBfeRunning = false;
		Flts.Reset();
		Apps.Reset();
		Insts.Reset();
		Cnns.Reset();
	}
}}
