﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace NetLimiter.Service{

[DebuggerDisplay("Count={_list.Count}")]
public class NodeLoaderNodes<TNode, TEventArgs> : IEnumerable<TNode>, IEnumerable where TNode : NLNode where TEventArgs : NLNodeEventArgs, new()
{
	internal TEventArgs _args = new TEventArgs();

	internal LinkedList<TNode> _list = new LinkedList<TNode>();

	private NodeLoader _ldr;

	public NodeLoader NodeLoader => _ldr;

	public event EventHandler<TEventArgs> Created;

	public event EventHandler<TEventArgs> Updated;

	public event EventHandler<TEventArgs> Removed;

	internal NodeLoaderNodes(NodeLoader ldr)
	{
		_ldr = ldr;
	}

	internal void AddNode(TNode node, LinkedListNode<TNode> before)
	{
		if (before != null)
		{
			_list.AddBefore(before, node);
		}
		else
		{
			_list.AddLast(node);
		}
		if (this.Created != null)
		{
			this.Created(_ldr, _args);
		}
	}

	internal void OnNodeUpdated()
	{
		if (this.Updated != null)
		{
			this.Updated(_ldr, _args);
		}
	}

	private void OnNodeRemoved(TNode node)
	{
		_args.Init(node);
		if (this.Removed != null)
		{
			this.Removed(_ldr, _args);
		}
		node.OnRemoved(_ldr);
	}

	internal void Remove(LinkedListNode<TNode> node)
	{
		OnNodeRemoved(node.Value);
		_list.Remove(node);
	}

	internal void RemoveLast()
	{
		OnNodeRemoved(_list.Last.Value);
		_list.RemoveLast();
	}

	internal void Clear()
	{
		while (_list.Any())
		{
			RemoveLast();
		}
	}

	public IEnumerator<TNode> GetEnumerator()
	{
		return _list.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return _list.GetEnumerator();
	}
}}
