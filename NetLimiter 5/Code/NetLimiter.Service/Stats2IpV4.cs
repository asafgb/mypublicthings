﻿using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2IpV4
{
	[DataMember]
	public uint Ip;

	[DataMember]
	public string Domain;
}}
