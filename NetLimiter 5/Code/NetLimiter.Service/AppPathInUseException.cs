﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class AppPathInUseException : NLException
{
	public string Path { get; set; }

	public AppPathInUseException(string path)
		: base(string.Format(Langs.MsgAppPathInUse, path))
	{
		Path = path;
	}

	public AppPathInUseException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
