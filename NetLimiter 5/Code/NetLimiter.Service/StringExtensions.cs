﻿using System.Linq;
using System.Text;
using System.Xml;

namespace NetLimiter.Service{

public static class StringExtensions
{
	public static string MakeXmlValid(this string str)
	{
		if (str.Any((char ch) => !XmlConvert.IsXmlChar(ch)))
		{
			StringBuilder stringBuilder = new StringBuilder(str.Length);
			foreach (char item in str.Where((char x) => XmlConvert.IsXmlChar(x)))
			{
				stringBuilder.Append(item);
			}
			str = stringBuilder.ToString();
		}
		return str;
	}
}}
