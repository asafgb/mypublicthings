﻿namespace NetLimiter.Service{

public class TopNodeEventArgs : NLNodeEventArgs
{
	public new TopNode Node
	{
		get
		{
			return base.Node as TopNode;
		}
		internal set
		{
			base.Node = value;
		}
	}
}}
