﻿namespace NetLimiter.Service{

public class IpProtocolName
{
	private static IpProtocolName[] _protocols;

	public int Number { get; protected set; }

	public string Name { get; protected set; }

	public string Desc { get; protected set; }

	public static IpProtocolName GetName(int protocolNumber)
	{
		return _protocols[protocolNumber];
	}

	static IpProtocolName()
	{
		_protocols = new IpProtocolName[256];
		_protocols[0] = new IpProtocolName(0, "HOPOPT", "IPv6 Hop-by-Hop Option");
		_protocols[1] = new IpProtocolName(1, "ICMP", "Internet Control Message");
		_protocols[2] = new IpProtocolName(2, "IGMP", "Internet Group Management");
		_protocols[3] = new IpProtocolName(3, "GGP", "Gateway-to-Gateway");
		_protocols[4] = new IpProtocolName(4, "IPv4", "IPv4 encapsulation");
		_protocols[5] = new IpProtocolName(5, "ST", "Stream");
		_protocols[6] = new IpProtocolName(6, "TCP", "Transmission Control");
		_protocols[7] = new IpProtocolName(7, "CBT", "CBT");
		_protocols[8] = new IpProtocolName(8, "EGP", "Exterior Gateway Protocol");
		_protocols[9] = new IpProtocolName(9, "IGP", "Interior gateway Protocol");
		_protocols[10] = new IpProtocolName(10, "BBN-RCC-MON", "BBN RCC Monitoring");
		_protocols[11] = new IpProtocolName(11, "NVP-II", "Network Voice Protocol");
		_protocols[12] = new IpProtocolName(12, "PUP", "PUP");
		_protocols[14] = new IpProtocolName(14, "EMCON", "EMCON");
		_protocols[15] = new IpProtocolName(15, "XNET", "Cross Net Debugger");
		_protocols[16] = new IpProtocolName(16, "CHAOS", "Chaos");
		_protocols[17] = new IpProtocolName(17, "UDP", "User Datagram");
		_protocols[18] = new IpProtocolName(18, "MUX", "Multiplexing");
		_protocols[19] = new IpProtocolName(19, "DCN-MEAS", "DCN Measurement Subsystems");
		_protocols[20] = new IpProtocolName(20, "HMP", "Host Monitoring");
		_protocols[21] = new IpProtocolName(21, "PRM", "Packet Radio Measurement");
		_protocols[22] = new IpProtocolName(22, "XNS-IDP", "XEROX NS IDP");
		_protocols[23] = new IpProtocolName(23, "TRUNK-1", "Trunk-1");
		_protocols[24] = new IpProtocolName(24, "TRUNK-2", "Trunk-2");
		_protocols[25] = new IpProtocolName(25, "LEAF-1", "Leaf-1");
		_protocols[26] = new IpProtocolName(26, "LEAF-2", "Leaf-2");
		_protocols[27] = new IpProtocolName(27, "RDP", "Reliable Data Protocol");
		_protocols[28] = new IpProtocolName(28, "IRTP", "Internet Reliable Transaction");
		_protocols[29] = new IpProtocolName(29, "ISO-TP4", "ISO Transport Protocol Class 4");
		_protocols[30] = new IpProtocolName(30, "NETBLT", "Bulk Data Transfer Protocol");
		_protocols[31] = new IpProtocolName(31, "MFE-NSP", "Protocol");
		_protocols[32] = new IpProtocolName(32, "MERIT-INP", "MERIT Internodal Protocol");
		_protocols[33] = new IpProtocolName(33, "DCCP", "Datagram Congestion Control");
		_protocols[34] = new IpProtocolName(34, "3PC", "Third Party Connect Protocol");
		_protocols[35] = new IpProtocolName(35, "IDPR", "Inter-Domain Policy Routing");
		_protocols[36] = new IpProtocolName(36, "XTP", "XTP");
		_protocols[37] = new IpProtocolName(37, "DDP", "Datagram Delivery Protocol");
		_protocols[38] = new IpProtocolName(38, "IDPR-CMTP", "IDPR Control Message");
		_protocols[39] = new IpProtocolName(39, "TP++", "TP++ Transport Protocol");
		_protocols[40] = new IpProtocolName(40, "IL", "IL Transport Protocol");
		_protocols[41] = new IpProtocolName(41, "IPv6", "IPv6 encapsulation");
		_protocols[42] = new IpProtocolName(42, "SDRP", "Source Demand Routing Protocol");
		_protocols[43] = new IpProtocolName(43, "IPv6-Route", "Routing Header for IPv6");
		_protocols[44] = new IpProtocolName(44, "IPv6-Frag", "Fragment Header for IPv6");
		_protocols[45] = new IpProtocolName(45, "IDRP", "Inter-Domain Routing Protocol");
		_protocols[46] = new IpProtocolName(46, "RSVP", "Reservation Protocol");
		_protocols[47] = new IpProtocolName(47, "GRE", "Generic Routing Encapsulation");
		_protocols[48] = new IpProtocolName(48, "DSR", "Dynamic Source Routing Protocol");
		_protocols[49] = new IpProtocolName(49, "BNA", "BNA");
		_protocols[50] = new IpProtocolName(50, "ESP", "Encap Security Payload");
		_protocols[51] = new IpProtocolName(51, "AH", "Authentication Header");
		_protocols[52] = new IpProtocolName(52, "I-NLSP", "Integrated Net Layer Security TUBA");
		_protocols[54] = new IpProtocolName(54, "NARP", "NBMA Address Resolution Protocol");
		_protocols[55] = new IpProtocolName(55, "MOBILE", "IP Mobility Transport Layer Security");
		_protocols[56] = new IpProtocolName(56, "TLSP", "Protocol using Kryptonet key management");
		_protocols[57] = new IpProtocolName(57, "SKIP", "SKIP");
		_protocols[58] = new IpProtocolName(58, "IPv6-ICMP", "ICMP for IPv6");
		_protocols[59] = new IpProtocolName(59, "IPv6-NoNxt", "No Next Header for IPv6");
		_protocols[60] = new IpProtocolName(60, "IPv6-Opts", "Destination Options for IPv6");
		_protocols[62] = new IpProtocolName(62, "CFTP", "CFTP");
		_protocols[64] = new IpProtocolName(64, "SAT-EXPAK", "SATNET and Backroom EXPAK");
		_protocols[65] = new IpProtocolName(65, "KRYPTOLAN", "Kryptolan");
		_protocols[66] = new IpProtocolName(66, "RVD", "MIT Remote Virtual Disk Protocol");
		_protocols[67] = new IpProtocolName(67, "IPPC", "Internet Pluribus Packet  Core");
		_protocols[69] = new IpProtocolName(69, "SAT-MON", "SATNET Monitoring");
		_protocols[70] = new IpProtocolName(70, "VISA", "VISA Protocol");
		_protocols[71] = new IpProtocolName(71, "IPCV", "Internet Packet Core Utility");
		_protocols[72] = new IpProtocolName(72, "CPNX", "Computer Protocol Network Executive");
		_protocols[73] = new IpProtocolName(73, "CPHB", "Computer Protocol Heart Beat");
		_protocols[74] = new IpProtocolName(74, "WSN", "Wang Span Network");
		_protocols[75] = new IpProtocolName(75, "PVP", "Packet Video Protocol");
		_protocols[76] = new IpProtocolName(76, "BR-SAT-MON", "Backroom SATNET Monitoring");
		_protocols[77] = new IpProtocolName(77, "SUN-ND", "SUN ND PROTOCOL-Temporary");
		_protocols[78] = new IpProtocolName(78, "WB-MON", "WIDEBAND Monitoring");
		_protocols[79] = new IpProtocolName(79, "WB-EXPAK", "WIDEBAND EXPAK");
		_protocols[80] = new IpProtocolName(80, "ISO-IP", "ISO Internet Protocol");
		_protocols[81] = new IpProtocolName(81, "VMTP", "VMTP");
		_protocols[82] = new IpProtocolName(82, "SECURE-VMTP", "SECURE-VMTP");
		_protocols[83] = new IpProtocolName(83, "VINES", "VINES");
		_protocols[84] = new IpProtocolName(84, "TTP", "Transaction Transport Protocol");
		_protocols[84] = new IpProtocolName(84, "IPTM", "Internet Protocol Traffic Manager");
		_protocols[85] = new IpProtocolName(85, "NSFNET-IGP", "NSFNET-IGP");
		_protocols[86] = new IpProtocolName(86, "DGP", "Dissimilar Gateway Protocol");
		_protocols[87] = new IpProtocolName(87, "TCF", "TCF");
		_protocols[88] = new IpProtocolName(88, "EIGRP", "EIGRP");
		_protocols[89] = new IpProtocolName(89, "OSPFIGP", "OSPFIGP");
		_protocols[90] = new IpProtocolName(90, "Sprite-RPC", "Sprite RPC Protocol");
		_protocols[91] = new IpProtocolName(91, "LARP", "Locus Address Resolution Protocol");
		_protocols[92] = new IpProtocolName(92, "MTP", "Multicast Transport Protocol");
		_protocols[93] = new IpProtocolName(93, "AX.25", "AX.25 Frames");
		_protocols[94] = new IpProtocolName(94, "IPIP", "IP-within-IP Encapsulation Protocol");
		_protocols[96] = new IpProtocolName(96, "SCC-SP", "Semaphore Communications Sec. Pro.");
		_protocols[97] = new IpProtocolName(97, "ETHERIP", "Ethernet-within-IP Encapsulation");
		_protocols[98] = new IpProtocolName(98, "ENCAP", "Encapsulation Header any private encryption scheme");
		_protocols[100] = new IpProtocolName(100, "GMTP", "GMTP");
		_protocols[101] = new IpProtocolName(101, "IFMP", "Ipsilon Flow Management Protocol");
		_protocols[102] = new IpProtocolName(102, "PNNI", "PNNI over IP");
		_protocols[103] = new IpProtocolName(103, "PIM", "Protocol Independent Multicast");
		_protocols[104] = new IpProtocolName(104, "ARIS", "ARIS");
		_protocols[105] = new IpProtocolName(105, "SCPS", "SCPS");
		_protocols[106] = new IpProtocolName(106, "QNX", "QNX");
		_protocols[107] = new IpProtocolName(107, "A/N", "Active Networks");
		_protocols[108] = new IpProtocolName(108, "IPComp", "IP Payload Compression Protocol");
		_protocols[109] = new IpProtocolName(109, "SNP", "Sitara Networks Protocol");
		_protocols[110] = new IpProtocolName(110, "Compaq-Peer", "Compaq Peer Protocol");
		_protocols[111] = new IpProtocolName(111, "IPX-in-IP", "IPX in IP");
		_protocols[112] = new IpProtocolName(112, "VRRP", "Virtual Router Redundancy Protocol");
		_protocols[113] = new IpProtocolName(113, "PGM", "PGM Reliable Transport Protocol");
		_protocols[115] = new IpProtocolName(115, "L2TP", "Layer Two Tunneling Protocol");
		_protocols[116] = new IpProtocolName(116, "DDX", "D-II Data Exchange (DDX)");
		_protocols[117] = new IpProtocolName(117, "IATP", "Interactive Agent Transfer Protocol");
		_protocols[118] = new IpProtocolName(118, "STP", "Schedule Transfer Protocol");
		_protocols[119] = new IpProtocolName(119, "SRP", "SpectraLink Radio Protocol");
		_protocols[120] = new IpProtocolName(120, "UTI", "UTI");
		_protocols[121] = new IpProtocolName(121, "SMP", "Simple Message Protocol");
		_protocols[123] = new IpProtocolName(123, "PTP", "Performance Transparency Protocol");
		_protocols[124] = new IpProtocolName(124, "ISIS over IPv4", "");
		_protocols[125] = new IpProtocolName(125, "FIRE", "");
		_protocols[126] = new IpProtocolName(126, "CRTP", "Combat Radio Transport Protocol");
		_protocols[127] = new IpProtocolName(127, "CRUDP", "Combat Radio User Datagram");
		_protocols[128] = new IpProtocolName(128, "SSCOPMCE", "");
		_protocols[129] = new IpProtocolName(129, "IPLT", "");
		_protocols[130] = new IpProtocolName(130, "SPS", "Secure Packet Shield");
		_protocols[131] = new IpProtocolName(131, "PIPE", "Private IP Encapsulation within IP");
		_protocols[132] = new IpProtocolName(132, "SCTP", "Stream Control Transmission Protocol");
		_protocols[133] = new IpProtocolName(133, "FC", "Fibre Channel");
		_protocols[134] = new IpProtocolName(134, "RSVP-E2E-IGNORE", "");
		_protocols[135] = new IpProtocolName(135, "Mobility Header", "");
		_protocols[136] = new IpProtocolName(136, "UDPLite", "");
		_protocols[137] = new IpProtocolName(137, "MPLS-in-IP", "");
		_protocols[138] = new IpProtocolName(138, "manet", "MANET Protocols");
		_protocols[139] = new IpProtocolName(139, "HIP", "Host Identity Protocol");
		_protocols[140] = new IpProtocolName(140, "Shim6", "Shim6 Protocol");
		_protocols[141] = new IpProtocolName(141, "WESP", "Wrapped Encapsulating Security Payload");
		_protocols[142] = new IpProtocolName(142, "ROHC", "Robust Header Compression");
		for (int i = 0; i < _protocols.Length; i++)
		{
			if (_protocols[i] == null)
			{
				_protocols[i] = new IpProtocolName(i, null, null);
			}
		}
	}

	protected IpProtocolName(int num, string name, string desc)
	{
		Number = num;
		if (string.IsNullOrEmpty(name))
		{
			Name = num.ToString();
		}
		else
		{
			Name = $"{name}({num})";
		}
		Desc = ((desc == null) ? "" : desc);
	}

	public override string ToString()
	{
		return Name;
	}
}}
