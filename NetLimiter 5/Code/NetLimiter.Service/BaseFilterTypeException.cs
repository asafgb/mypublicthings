﻿using System;
using System.Runtime.Serialization;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class BaseFilterTypeException : NLException
{
	public BaseFilterTypeException(Filter fltDst)
		: base(string.Format(Langs.MsgBaseFilterType, fltDst.Name, fltDst.Id))
	{
	}

	public BaseFilterTypeException(SerializationInfo info, StreamingContext ctx)
		: base(info, ctx)
	{
	}
}}
