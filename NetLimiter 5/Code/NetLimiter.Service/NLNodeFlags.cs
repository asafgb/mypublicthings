﻿using System;

namespace NetLimiter.Service{

[Flags]
internal enum NLNodeFlags
{
	Closed = 1,
	TotalInTimeout = 2,
	TotalOutTimeout = 4
}}
