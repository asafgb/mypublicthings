﻿using System;

namespace NetLimiter.Service{

public class AppInfoEventArgs : EventArgs
{
	public AppInfo Info { get; internal set; }

	public AppInfoEventArgs(AppInfo info)
	{
		Info = info;
	}
}}
