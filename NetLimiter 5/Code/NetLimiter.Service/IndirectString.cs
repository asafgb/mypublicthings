﻿using System;
using NLInterop;

namespace NetLimiter.Service{

public static class IndirectString
{
	public static string Translate(string str, string def = null)
	{
		if (string.IsNullOrEmpty(str))
		{
			return def;
		}
		if (str[0] != '@')
		{
			return str;
		}
		try
		{
			str = AppIdHelper.LoadIndirectString(str);
		}
		catch
		{
		}
		if (!string.IsNullOrEmpty(str))
		{
			return str;
		}
		return def;
	}

	public static string Translate(string packageId, string packageName, string str, string def)
	{
		if (string.IsNullOrEmpty(str))
		{
			return def;
		}
		if (str.Length > "ms-resource:".Length && str.StartsWith("ms-resource:", StringComparison.InvariantCultureIgnoreCase))
		{
			if (str["ms-resource:".Length] == '/')
			{
				str = "@{" + packageId + "?" + str + "}";
			}
			else
			{
				str = str.Substring("ms-resource:".Length);
				str = ((0 > str.IndexOf("/resources/", StringComparison.InvariantCultureIgnoreCase)) ? ("@{" + packageId + "?ms-resource://" + packageName + "/resources/" + str + "}") : ("@{" + packageId + "?ms-resource://" + packageName + "/" + str + "}"));
			}
		}
		if (str[0] != '@')
		{
			return str;
		}
		try
		{
			str = AppIdHelper.LoadIndirectString(str);
			return string.IsNullOrEmpty(str) ? def : str;
		}
		catch
		{
			return def;
		}
	}
}}
