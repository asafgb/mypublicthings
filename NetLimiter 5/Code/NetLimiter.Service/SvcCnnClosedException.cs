﻿using System;
using NetLimiter.Properties;

namespace NetLimiter.Service{

[Serializable]
public class SvcCnnClosedException : Exception
{
	public SvcCnnClosedException()
		: base(Langs.MsgSvcCnnClosed)
	{
	}
}}
