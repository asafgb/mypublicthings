﻿using System;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[DataContract]
public class Stats2ResultDataAppIpPort
{
	[DataMember]
	public ulong In;

	[DataMember]
	public ulong Out;

	[DataMember]
	public ushort AppId;

	[DataMember]
	public uint IpIx;

	[DataMember]
	public ushort Port;

	[DataMember]
	public DateTime First;

	[DataMember]
	public DateTime Last;
}}
