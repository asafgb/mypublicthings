﻿namespace NetLimiter.Service{

public class StatsQueryHelperDictKeyV4
{
	public uint IpV4 { get; set; }

	public ushort Port { get; set; }

	public ushort AppId { get; set; }
}}
