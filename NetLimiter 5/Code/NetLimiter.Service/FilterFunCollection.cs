﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service{

[Serializable]
[CollectionDataContract(Name = "filterFunctions", ItemName = "function")]
public class FilterFunCollection : List<FilterFunction>
{
}}
