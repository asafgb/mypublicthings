﻿namespace NetLimiter{

public static class Common
{
	public static readonly string ApiHostUrl;

	public static readonly string WebHostUrl;

	public const ushort DefaultPort = 4045;

	public const string ProductName = "NetLimiter";

	public const string CompanyShortName = "Locktime";

	public const string CompanyLongName = "Locktime Software";

	public const string AssemblyVersion = "5.3.6.0";

	public const string FileVersion = "5.3.6.0";

	public const string Copyright = "(c) Locktime Software.  All rights reserved.";

	public static string BuyPageUrl;

	public static readonly string RegHelpLink;

	public static readonly string FltEdDnsNameHelpLink;

	public static readonly string FltEdPathHelpLink;

	public static string RenewLicenseUrl(string regCodeHash)
	{
		return WebHostUrl + "/buy/upgrade/" + regCodeHash + "/renew";
	}

	public static string VerChkUrl(string product, string version)
	{
		return ApiHostUrl + "/versionchecker?product=" + product + "&version=" + version;
	}

	static Common()
	{
		ApiHostUrl = "https://netlimiter.com";
		WebHostUrl = "https://netlimiter.com";
		BuyPageUrl = WebHostUrl + "/buy";
		RegHelpLink = WebHostUrl + "/docs/installation/purchase-and-registration";
		FltEdDnsNameHelpLink = WebHostUrl + "/docs/basic-concepts/filters#dns-name";
		FltEdPathHelpLink = WebHostUrl + "/docs/basic-concepts/filters#file-path-eq";
	}
}}
