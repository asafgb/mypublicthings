﻿using CoreLib;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace NetLimiter{

public static class Logging
{
	private static ILoggerFactory _loggerFactory = NullLoggerFactory.Instance;

	public static ILoggerFactory LoggerFactory
	{
		get
		{
			return _loggerFactory;
		}
		set
		{
			CoreLib.Logging.LoggerFactory = value;
			_loggerFactory = value ?? NullLoggerFactory.Instance;
		}
	}
}}
