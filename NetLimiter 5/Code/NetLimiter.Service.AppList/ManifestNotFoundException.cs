﻿using System;

namespace NetLimiter.Service.AppList{

internal class ManifestNotFoundException : Exception
{
	public string Path { get; protected set; }

	internal ManifestNotFoundException(string path)
	{
		Path = path;
	}
}}
