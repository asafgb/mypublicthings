﻿using System.Runtime.Serialization;

namespace NetLimiter.Service.AppList{

[DataContract]
public class AppListSvcItem
{
	[DataMember]
	public string Name { get; set; }

	[DataMember]
	public string DisplayName { get; set; }

	[DataMember]
	public string Description { get; set; }

	[DataMember]
	public Sid Sid { get; set; }

	[DataMember]
	public string Path { get; set; }
}}
