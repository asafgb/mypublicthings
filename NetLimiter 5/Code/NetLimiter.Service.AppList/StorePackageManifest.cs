﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace NetLimiter.Service.AppList{

public class StorePackageManifest
{
	public const string ManifestFileName = "AppxManifest.xml";

	private XElement _doc;

	private string _publisher;

	private string _displayName;

	private string _description;

	private string _logo;

	private string _appIcon44;

	private string _firstAppIcon44;

	public string AppName { get; protected set; }

	public string Xml { get; protected set; }

	public string StoreLogo { get; protected set; }

	public string Folder { get; protected set; }

	public string Name { get; protected set; }

	public string Version { get; protected set; }

	public string ProcessorArchitecture { get; protected set; }

	public string ResourceId { get; protected set; }

	public string PublisherId { get; protected set; }

	public string PackageId => Name + "_" + Version + "_" + ProcessorArchitecture + "_" + ResourceId + "_" + PublisherId;

	public string Publisher
	{
		get
		{
			string obj = _publisher ?? (from x in _doc.Descendants()
				where x.Name.LocalName == "PublisherDisplayName"
				select x).FirstOrDefault()?.Value;
			string result = obj;
			_publisher = obj;
			return result;
		}
	}

	public string DisplayName
	{
		get
		{
			string obj = _displayName ?? (from x in _doc.Descendants()
				where x.Name.LocalName == "DisplayName"
				select x).FirstOrDefault()?.Value;
			string result = obj;
			_displayName = obj;
			return result;
		}
	}

	public string Description
	{
		get
		{
			string obj = _description ?? (from x in _doc.Descendants()
				where x.Name.LocalName == "Description"
				select x).FirstOrDefault()?.Value;
			string result = obj;
			_description = obj;
			return result;
		}
	}

	public string Logo
	{
		get
		{
			string obj = _logo ?? (from x in _doc.Descendants()
				where x.Name.LocalName == "Logo"
				select x).FirstOrDefault()?.Value;
			string result = obj;
			_logo = obj;
			return result;
		}
	}

	public string AppIcon44
	{
		get
		{
			if (_appIcon44 == null)
			{
				IEnumerable<XElement> source = from x in _doc.Descendants()
					where x.Name.LocalName == "Application"
					select x;
				if (!string.IsNullOrEmpty(AppName))
				{
					source = source.Where((XElement x) => IsAttrEqual(x.Attribute("Executable"), AppName));
				}
				XElement xElement = source.FirstOrDefault();
				if (xElement != null)
				{
					XElement xElement2 = xElement.Descendants().FirstOrDefault((XElement x) => x.Name.LocalName == "VisualElements");
					_appIcon44 = (string)xElement2.Attribute("Square44x44Logo");
				}
			}
			return _appIcon44;
		}
	}

	public string FirstAppIcon44
	{
		get
		{
			if (_firstAppIcon44 == null)
			{
				XElement xElement = _doc.Descendants().FirstOrDefault((XElement x) => x.Name.LocalName == "Application");
				if (xElement != null)
				{
					XElement xElement2 = xElement.Descendants().FirstOrDefault((XElement x) => x.Name.LocalName == "VisualElements");
					_firstAppIcon44 = (string)xElement2.Attribute("Square44x44Logo");
				}
			}
			return _firstAppIcon44;
		}
	}

	public StorePackageManifest(AppId appId)
	{
		Load(appId);
	}

	public StorePackageManifest(string folder)
	{
		Load(folder);
	}

	public void Load(AppId appId)
	{
		if ((object)appId?.Sid == null)
		{
			throw new ArgumentNullException("Can't load store app manifest: the SID is null");
		}
		string packgeFolder = GetPackgeFolder(appId);
		if (packgeFolder == null)
		{
			throw new ArgumentException("Failed to get package folder: $" + appId.Sid.ToString());
		}
		Load(packgeFolder);
	}

	public void Load(string folder)
	{
		if (folder == null)
		{
			throw new ArgumentNullException("Can't load package manifest. The folder is null.");
		}
		if (folder[folder.Length - 1] == '\\')
		{
			folder = folder.Substring(0, folder.Length - 1);
		}
		Folder = folder;
		string text = Path.Combine(Folder, "AppxManifest.xml");
		if (!File.Exists(text))
		{
			throw new ManifestNotFoundException(Folder);
		}
		LoadManifest(text);
		int num = Folder.LastIndexOf('_');
		if (num <= 0 || num == Folder.Length - 1)
		{
			throw new Exception("Bad store app path: " + folder);
		}
		PublisherId = Folder.Substring(num + 1);
	}

	public static string GetPackgeFolder(AppId appId)
	{
		if (appId == null)
		{
			return null;
		}
		if (!appId.IsAppEmpty)
		{
			try
			{
				string directoryName = Path.GetDirectoryName(appId.Path);
				if (File.Exists(Path.Combine(directoryName, "AppxManifest.xml")))
				{
					return directoryName;
				}
			}
			catch
			{
			}
		}
		if (appId.IsSidEmpty)
		{
			throw new PackageNotFoundException();
		}
		return (StoreAppHelper.GetStoreApp(appId.Sid) ?? throw new PackageNotFoundException(appId.Sid)).Folder;
	}

	public void LoadManifest(string manifestPath)
	{
		_doc = XElement.Load(manifestPath);
		InitIdentity();
	}

	private void InitIdentity()
	{
		XElement xElement = (from x in _doc.Descendants()
			where x.Name.LocalName == "Identity"
			select x).FirstOrDefault();
		if (xElement != null)
		{
			Name = (string)xElement.Attribute("Name");
			Version = (string)xElement.Attribute("Version");
			ProcessorArchitecture = (string)xElement.Attribute("ProcessorArchitecture");
			ResourceId = (string)xElement.Attribute("ResourceId");
		}
	}

	private bool IsAttrEqual(XAttribute attr, string value)
	{
		if (attr != null)
		{
			return string.Equals((string)attr, value, StringComparison.InvariantCultureIgnoreCase);
		}
		return false;
	}
}}
