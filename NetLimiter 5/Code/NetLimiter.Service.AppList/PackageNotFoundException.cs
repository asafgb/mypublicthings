﻿using System;

namespace NetLimiter.Service.AppList{

public class PackageNotFoundException : Exception
{
	public Sid Sid { get; protected set; }

	public PackageNotFoundException()
	{
	}

	public PackageNotFoundException(Sid sid)
	{
		Sid = sid;
	}
}}
