﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceProcess;
using Microsoft.Win32;
using NLInterop;

namespace NetLimiter.Service.AppList{

public static class AppListCreator
{
	public static List<AppListStoreItem> GetStoreApps()
	{
		return StoreAppHelper.GetStoreApps();
	}

	public static List<AppListSvcItem> GetServices()
	{
		List<AppListSvcItem> list = new List<AppListSvcItem>();
		try
		{
			ServiceController[] services = ServiceController.GetServices();
			foreach (ServiceController serviceController in services)
			{
				try
				{
					AppListSvcItem item = CreateServiceItem(Sid.FromServiceName(serviceController.ServiceName), serviceController.ServiceName);
					list.Add(item);
				}
				catch
				{
				}
			}
		}
		catch
		{
		}
		return list;
	}

	public static AppListSvcItem GetSvcItem(Sid sid)
	{
		if (!sid.IsServiceSid)
		{
			return null;
		}
		string svcNameFromSid = AppIdHelper.GetSvcNameFromSid(sid.Bytes);
		if (svcNameFromSid == null)
		{
			return null;
		}
		return CreateServiceItem(sid, svcNameFromSid);
	}

	private static AppListSvcItem CreateServiceItem(Sid sid, string svcName)
	{
		AppListSvcItem appListSvcItem = new AppListSvcItem();
		using RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);
		using RegistryKey registryKey2 = registryKey.OpenSubKey("System\\CurrentControlSet\\Services\\" + svcName);
		appListSvcItem.Name = svcName;
		appListSvcItem.Sid = sid;
		appListSvcItem.DisplayName = IndirectString.Translate(registryKey2.GetValue("DisplayName") as string, svcName);
		appListSvcItem.Description = IndirectString.Translate(registryKey2.GetValue("Description") as string);
		appListSvcItem.Path = Environment.ExpandEnvironmentVariables(registryKey2.GetValue("ImagePath") as string);
		return appListSvcItem;
	}

	public static List<AppId> GetRemovedApps(List<AppInfo> apps)
	{
		List<AppId> list = new List<AppId>();
		foreach (AppInfo app in apps)
		{
			try
			{
				if (app.AppId.Path.Equals("system", StringComparison.InvariantCultureIgnoreCase) || File.Exists(app.AppId.Path))
				{
					continue;
				}
				goto IL_0048;
			}
			catch
			{
				goto IL_0048;
			}
			IL_0048:
			list.Add(app.AppId);
		}
		return list;
	}
}}
