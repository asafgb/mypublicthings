﻿using System.Runtime.Serialization;

namespace NetLimiter.Service.AppList{

[DataContract]
public class AppListStoreItem
{
	[DataMember]
	public string PackageId { get; set; }

	[DataMember]
	public string Name { get; set; }

	[DataMember]
	public string DisplayName { get; set; }

	[DataMember]
	public string Folder { get; set; }

	[DataMember]
	public Sid Sid { get; set; }
}}
