﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32;

namespace NetLimiter.Service.AppList{

public class StoreAppHelper
{
	public const string StoreAppsRegKeyName = "Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppContainer\\Mappings";

	public const string RepositoryPackagesKeyName = "Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\Repository\\Packages";

	private static string GetParentSid(string sid)
	{
		int num = 0;
		for (int i = 0; i < sid.Length; i++)
		{
			if (sid[i] == '-' && ++num == 11)
			{
				return sid.Substring(0, i);
			}
		}
		return sid;
	}

	public static void ForEachPackageRegKey(Func<RegistryKey, string, bool> handler)
	{
		try
		{
			using RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.Users, RegistryView.Default);
			if (registryKey == null)
			{
				return;
			}
			foreach (string item in from x in registryKey.GetSubKeyNames()
				where x.EndsWith("_classes", StringComparison.InvariantCultureIgnoreCase)
				select x)
			{
				try
				{
					using RegistryKey registryKey2 = registryKey.OpenSubKey(item + "\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\Repository\\Packages");
					string[] subKeyNames = registryKey2.GetSubKeyNames();
					foreach (string text in subKeyNames)
					{
						using RegistryKey arg = registryKey2.OpenSubKey(text);
						try
						{
							if (!handler(arg, text))
							{
								return;
							}
						}
						catch
						{
						}
					}
				}
				catch
				{
				}
			}
		}
		catch
		{
		}
	}

	public static List<AppListStoreItem> GetStoreApps()
	{
		List<AppListStoreItem> items = new List<AppListStoreItem>();
		ForEachPackageRegKey(delegate(RegistryKey rk, string rkName)
		{
			AppListStoreItem item = new AppListStoreItem();
			InitPkgDetails(rk, item);
			items.Add(item);
			return true;
		});
		return items;
	}

	public static AppListStoreItem GetStoreApp(Sid sid)
	{
		AppListStoreItem item = null;
		ForEachPackageRegKey(delegate(RegistryKey rk, string rkName)
		{
			if (rk.GetValue("PackageSid") is byte[] bytes && Sid.FromBytes(bytes) == sid)
			{
				item = new AppListStoreItem();
				InitPkgDetails(rk, item);
				return false;
			}
			return true;
		});
		return item;
	}

	internal static void InitPkgDetails(RegistryKey rkPkg, AppListStoreItem item)
	{
		item.PackageId = rkPkg.GetValue("PackageID") as string;
		if (string.IsNullOrEmpty(item.PackageId))
		{
			throw new Exception("Failed to init store package item");
		}
		byte[] bytes = rkPkg.GetValue("PackageSid") as byte[];
		item.Sid = Sid.FromBytes(bytes);
		item.DisplayName = rkPkg.GetValue("DisplayName") as string;
		item.Name = GetPackageNameFromId(item.PackageId);
		item.Folder = rkPkg.GetValue("PackageRootFolder") as string;
	}

	public static string GetPackageNameFromId(string packageId)
	{
		if (packageId == null)
		{
			return null;
		}
		string[] array = packageId.Split('_');
		string text = array[0];
		if (array.Length > 1)
		{
			text = text + "_" + array[array.Length - 1];
		}
		return text;
	}
}}
