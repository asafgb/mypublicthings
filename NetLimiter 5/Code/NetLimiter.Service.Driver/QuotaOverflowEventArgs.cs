﻿using System;

namespace NetLimiter.Service.Driver{

internal class QuotaOverflowEventArgs : EventArgs
{
	public uint RuleId { get; }

	public uint OverflowCount { get; }

	public QuotaOverflowEventArgs(uint ruleId, uint overflowCount)
	{
		RuleId = ruleId;
		OverflowCount = overflowCount;
	}
}}
