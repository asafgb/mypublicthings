﻿using System;

namespace NetLimiter.Service.Driver{

internal class RuleChangedEventArgs : EventArgs
{
	public uint RuleId { get; }

	public bool IsEnabled { get; }

	public RuleChangedEventArgs(uint ruleId, bool isEnabled)
	{
		RuleId = ruleId;
		IsEnabled = isEnabled;
	}
}}
