﻿using System;

namespace NetLimiter.Service.Driver{

internal class FwrCountChangedEventArgs : EventArgs
{
	public uint Count { get; }

	public FwrCountChangedEventArgs(uint count)
	{
		Count = count;
	}
}}
