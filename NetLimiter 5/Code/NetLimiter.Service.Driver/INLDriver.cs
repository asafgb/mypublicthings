﻿using System;
using System.Collections.Generic;
using NetLimiter.DnsCache;

namespace NetLimiter.Service.Driver{

internal interface INLDriver
{
	event EventHandler<AppAddedEventArgs> AppAdded;

	event EventHandler<FwrCountChangedEventArgs> FwrCountChanged;

	event EventHandler<QuotaOverflowEventArgs> QuotaOverflow;

	event EventHandler<RuleChangedEventArgs> RuleChanged;

	event EventHandler<BfeChangedEventArgs> BfeChanged;

	void AddFilterToDriver(Filter filter);

	void RemoveAppFromDriver(AppId appId);

	uint SvcCliCnnCheck(string code);

	void OnPowerStateChange(bool resume);

	void SendAppTagsToDriver(AppInfo appInfo);

	void SendLicenseInfoToDrv();

	void OnDnsItemsAdded(LinkedList<DnsItem> items);

	void OnDnsItemsRemoved(LinkedList<DnsItem> items);
}}
