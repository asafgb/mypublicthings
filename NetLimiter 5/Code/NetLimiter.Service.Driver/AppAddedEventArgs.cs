﻿using System;

namespace NetLimiter.Service.Driver{

internal class AppAddedEventArgs : EventArgs
{
	public AppId AppId { get; protected set; }

	public AppAddedEventArgs(AppId appId)
	{
		AppId = appId ?? throw new ArgumentNullException("appId");
	}
}}
