﻿using System;

namespace NetLimiter.Service.Driver{

internal class BfeChangedEventArgs : EventArgs
{
	public bool IsRunning { get; }

	public BfeChangedEventArgs(bool isRunning)
	{
		IsRunning = isRunning;
	}
}}
