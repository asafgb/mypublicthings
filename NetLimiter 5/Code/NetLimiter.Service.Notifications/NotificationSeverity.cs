﻿namespace NetLimiter.Service.Notifications{

public enum NotificationSeverity
{
	Low,
	Normal,
	High
}}
