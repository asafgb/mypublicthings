﻿namespace NetLimiter.Service.Notifications{

public static class KnownNotifications
{
	public const string BfeNotRunningId = "notifid-BFE-not-running";

	public const string LicenseExpiration = "notifid-lic-expiration";

	public const string SubscriptionCancelled = "notifid-subscription-cancelled";
}}
