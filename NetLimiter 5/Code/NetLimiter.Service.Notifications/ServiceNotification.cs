﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NetLimiter.Service.Notifications{

[DataContract]
[KnownType(typeof(NLLicense))]
public class ServiceNotification
{
	[DataMember]
	public string Id { get; set; } = Guid.NewGuid().ToString();


	[DataMember]
	public DateTime Created { get; set; } = DateTime.UtcNow;


	[DataMember]
	public NotificationSeverity Severity { get; set; }

	[DataMember]
	public bool IsMuted { get; set; }

	[DataMember]
	public bool IsManaged { get; set; }

	[DataMember]
	public string Message { get; set; }

	[DataMember]
	public string Description { get; set; }

	[DataMember]
	public string HelpLink { get; set; }

	[DataMember]
	public string AppLink { get; set; }

	[DataMember]
	public Dictionary<string, object> Params { get; set; }

	public void SetParam(string key, object value)
	{
		if (Params == null)
		{
			Params = new Dictionary<string, object>();
		}
		Params.Add(key, value);
	}

	public T GetParam<T>(string key, T def = default(T))
	{
		if (Params == null)
		{
			return def;
		}
		if (Params.TryGetValue(key, out var value))
		{
			if (value is T)
			{
				return (T)value;
			}
			try
			{
				if (value is IConvertible convertible)
				{
					return (T)convertible.ToType(typeof(T), null);
				}
			}
			catch
			{
			}
		}
		return def;
	}
}}
