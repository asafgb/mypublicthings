﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Versioning;

[assembly: AssemblyTitle("NetLimiter API")]
[assembly: AssemblyDescription("NetLimiter API")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Locktime Software")]
[assembly: AssemblyProduct("NetLimiter")]
[assembly: AssemblyCopyright("(c) Locktime Software.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: InternalsVisibleTo("NetLimiter.Runtime")]
[assembly: InternalsVisibleTo("NLCppTest")]
[assembly: InternalsVisibleTo("NetLimiter.clr.Test")]
[assembly: ContractNamespace("nlsettings", ClrNamespace = "NetLimiter.Service")]
[assembly: ComVisible(false)]
[assembly: Guid("4d3ad743-f083-4e81-b832-7597dc1a6785")]
[assembly: AssemblyFileVersion("5.3.6.0")]
[assembly: AssemblyVersion("5.3.6.0")]
