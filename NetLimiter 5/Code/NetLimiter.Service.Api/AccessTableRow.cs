﻿using System.Runtime.Serialization;
using NetLimiter.Service.Security;

namespace NetLimiter.Service.Api{

[DataContract]
public class AccessTableRow
{
	[DataMember]
	public string Name { get; set; }

	[DataMember]
	public string Sid { get; set; }

	[DataMember]
	public Rights AllowedRights { get; set; }

	[DataMember]
	public Rights DeniedRights { get; set; }

	public AccessTableRow()
	{
	}

	public AccessTableRow(string name, string sid)
	{
		Name = name;
		Sid = sid;
	}
}}
