﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;

namespace LicenseKeeper.Models{

public class RegData
{
	public RegDataVersion Version { get; set; }

	public string ProductId { get; set; }

	public string EditionId { get; set; }

	public string LicenseId { get; set; }

	public string PlanId { get; set; }

	public bool IsRecurring { get; set; }

	public bool IsCancelled { get; set; }

	public DateTime EndTime { get; set; }

	public int Quantity { get; set; }

	public string RegName { get; set; }

	public string RegCodeHash { get; set; }

	public string HWCodeHash { get; set; }

	public string Signature { get; set; }

	public byte[] ToBytes()
	{
		return Encoding.UTF8.GetBytes(ToKeyString());
	}

	public string ToKeyString()
	{
		return Version switch
		{
			RegDataVersion.V0 => ToKeyStringV0(), 
			RegDataVersion.V1 => ToKeyStringV1(), 
			_ => throw new Exception($"Invalid RegData version requested: {Version}"), 
		};
	}

	private string ToKeyStringV0()
	{
		return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}", ProductId, EditionId, LicenseId, PlanId, IsRecurring, IsCancelled, EndTime.ToString("yyyyMMdd"), Quantity, RegName ?? "", RegCodeHash, HWCodeHash).ToLower();
	}

	private string ToKeyStringV1()
	{
		CultureInfo cultureInfo = new CultureInfo("en-US");
		string text = EndTime.ToString("yyyyMMdd", cultureInfo);
		return FormattableStringFactory.Create("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}", (int)Version, ProductId, EditionId, LicenseId, PlanId, IsRecurring, IsCancelled, text, Quantity, RegName ?? "", RegCodeHash, HWCodeHash).ToString(cultureInfo).ToLower(cultureInfo)
			.Normalize();
	}
}}
