﻿namespace LicenseKeeper.Api.Server{

public class ApiResponse
{
	public const int Status500InternalServerError = 500;

	public bool Success { get; set; }

	public string Message { get; set; }

	public string Details { get; set; }

	public int StatusCode { get; set; }

	public object Result { get; set; }

	public ApiResponse()
	{
		Success = true;
	}

	public ApiResponse(string message, int statusCode = 500)
	{
		Success = false;
		StatusCode = statusCode;
		Message = message;
	}
}
public class ApiResponse<T> : ApiResponse
{
	public new T Result
	{
		get
		{
			object result = base.Result;
			if (result is T)
			{
				return (T)result;
			}
			return default(T);
		}
		set
		{
			base.Result = value;
		}
	}

	public ApiResponse()
	{
	}

	public ApiResponse(T result)
	{
		base.Success = true;
		Result = result;
	}

	public ApiResponse(string message, int statusCode = 500)
		: base(message, statusCode)
	{
	}
}}
