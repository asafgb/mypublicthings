﻿namespace NetLimiter.DnsCache{

public enum DnsItemState
{
	New,
	Added,
	Active,
	ToDelete,
	Deleted
}}
