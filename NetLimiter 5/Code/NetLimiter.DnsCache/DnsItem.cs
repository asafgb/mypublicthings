﻿namespace NetLimiter.DnsCache{

public class DnsItem
{
	public ulong RuntimeId { get; internal set; }

	public DnsItemState State { get; internal set; }

	public bool IsIpv6 { get; set; }

	public string Address { get; set; }

	public string Name { get; set; }
}}
