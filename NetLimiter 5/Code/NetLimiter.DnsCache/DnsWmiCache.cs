﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Threading;
using Microsoft.Extensions.Logging;

namespace NetLimiter.DnsCache{

public class DnsWmiCache
{
	private static readonly ILogger<DnsWmiCache> _logger = Logging.LoggerFactory.CreateLogger<DnsWmiCache>();

	private Thread _thread;

	private AutoResetEvent _refreshEvent = new AutoResetEvent(initialState: false);

	private ManualResetEvent _exitEvent = new ManualResetEvent(initialState: false);

	private ManagementScope _scope = new ManagementScope("\\\\.\\ROOT\\StandardCimv2");

	private ObjectQuery _query = new ObjectQuery("SELECT * FROM MSFT_DNSClientCache");

	private ManagementObjectSearcher _searcher;

	private Dictionary<string, LinkedListNode<DnsItem>> _items = new Dictionary<string, LinkedListNode<DnsItem>>();

	private Timer _timer;

	private DnsEventArgs _eventArgs = new DnsEventArgs();

	private LinkedList<DnsItem> _addedItems = new LinkedList<DnsItem>();

	private LinkedList<DnsItem> _deletedItems = new LinkedList<DnsItem>();

	private int _minRefreshInternval;

	private ulong _runtimeId;

	public event EventHandler<DnsEventArgs> DnsItemsAdded;

	public event EventHandler<DnsEventArgs> DnsItemsRemoved;

	private void CreateSearcher()
	{
		_logger.LogDebug("Create dns searcher");
		try
		{
			_searcher?.Dispose();
			_searcher = new ManagementObjectSearcher(_scope, _query);
		}
		catch (Exception exception)
		{
			_logger.LogError(exception, "Failed to create dns searcher");
		}
	}

	public void Start(TimeSpan? autoRefreshInterval = null, int minRefreshInternval = 0)
	{
		_logger.LogInformation("Starting dns cache");
		_refreshEvent.Set();
		_exitEvent.Reset();
		_thread = new Thread(ThreadProc);
		_thread.Start();
		_minRefreshInternval = minRefreshInternval;
		if (autoRefreshInterval.HasValue && autoRefreshInterval.GetValueOrDefault().Ticks > 0)
		{
			_timer = new Timer(OnRefreshTimer, null, autoRefreshInterval.Value, autoRefreshInterval.Value);
		}
	}

	public void Stop()
	{
		_logger.LogInformation("Stopping dns cache...");
		if (_timer != null)
		{
			_timer.Dispose();
			_timer = null;
		}
		if (_thread != null)
		{
			_exitEvent.Set();
			_thread.Join();
			_thread = null;
		}
		_logger.LogInformation("Dns cache stopped");
	}

	public void Refresh()
	{
		_refreshEvent.Set();
	}

	private void OnRefreshTimer(object state)
	{
		Refresh();
	}

	private void ThreadProc()
	{
		_logger.LogInformation("Dns cache thread runnning");
		CreateSearcher();
		WaitHandle[] array = new WaitHandle[2] { _exitEvent, _refreshEvent };
		while (array[WaitHandle.WaitAny(array)] != _exitEvent)
		{
			foreach (LinkedListNode<DnsItem> value2 in _items.Values)
			{
				value2.Value.State = DnsItemState.ToDelete;
			}
			try
			{
				if (_searcher == null)
				{
					continue;
				}
				foreach (ManagementObject item in _searcher.Get())
				{
					ushort? num;
					try
					{
						num = item["Type"] as ushort?;
						if (!num.HasValue)
						{
							continue;
						}
					}
					catch
					{
						continue;
					}
					if (num != 1 && num != 28)
					{
						continue;
					}
					LinkedListNode<DnsItem> value = null;
					string text = null;
					string text2 = null;
					try
					{
						text = item["Data"] as string;
						if (text != null)
						{
							text2 = item["Entry"] as string;
							if (text2 != null)
							{
								goto IL_019c;
							}
						}
					}
					catch
					{
					}
					continue;
					IL_019c:
					if (_items.TryGetValue(text, out value))
					{
						if (value.Value.State == DnsItemState.New)
						{
							continue;
						}
						if (value.Value.Name != text2)
						{
							value.Value.State = DnsItemState.Deleted;
							_deletedItems.AddLast(value);
							value = null;
						}
						else
						{
							value.Value.State = DnsItemState.Active;
						}
					}
					if (value == null)
					{
						value = new LinkedListNode<DnsItem>(new DnsItem
						{
							RuntimeId = ++_runtimeId,
							State = DnsItemState.New,
							Name = text2,
							Address = text,
							IsIpv6 = (num == 28)
						});
						_items[text] = value;
						_addedItems.AddLast(value);
					}
				}
				goto IL_02f5;
			}
			catch (Exception ex)
			{
				_logger.LogDebug("Failed to get dns entries: {message}, type={type}", ex.Message, ex.GetType().Name);
				CreateSearcher();
				goto IL_02f5;
			}
			IL_02f5:
			foreach (LinkedListNode<DnsItem> item2 in _items.Values.Where((LinkedListNode<DnsItem> x) => x.Value.State == DnsItemState.ToDelete))
			{
				_deletedItems.AddLast(item2);
			}
			foreach (DnsItem deletedItem in _deletedItems)
			{
				if (deletedItem.State == DnsItemState.ToDelete)
				{
					_items.Remove(deletedItem.Address);
					deletedItem.State = DnsItemState.Deleted;
				}
			}
			if (_deletedItems.Any())
			{
				_eventArgs.Items = _deletedItems;
				this.DnsItemsRemoved?.Invoke(this, _eventArgs);
				_deletedItems.Clear();
			}
			if (_addedItems.Any())
			{
				_eventArgs.Items = _addedItems;
				this.DnsItemsAdded?.Invoke(this, _eventArgs);
				_addedItems.Clear();
			}
			if (_exitEvent.WaitOne(_minRefreshInternval))
			{
				break;
			}
		}
		_logger.LogInformation("Dns cache thread leaving");
	}

	private void Trace(ManagementObjectCollection coll)
	{
		foreach (ManagementObject item in coll)
		{
			Console.WriteLine("--------------------------------------------");
			foreach (PropertyData property in item.Properties)
			{
				Console.WriteLine($"{property.Name} = {property.Value} [{property.Value?.GetType().Name}]");
			}
		}
	}
}}
