﻿using System;
using System.Collections.Generic;

namespace NetLimiter.DnsCache{

public class DnsEventArgs : EventArgs
{
	public LinkedList<DnsItem> Items { get; internal set; }
}}
