﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace NetLimiter.Properties{

[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
[DebuggerNonUserCode]
[CompilerGenerated]
internal class Langs
{
	private static ResourceManager resourceMan;

	private static CultureInfo resourceCulture;

	[EditorBrowsable(EditorBrowsableState.Advanced)]
	internal static ResourceManager ResourceManager
	{
		get
		{
			if (resourceMan == null)
			{
				resourceMan = new ResourceManager("NetLimiter.Properties.Langs", typeof(Langs).Assembly);
			}
			return resourceMan;
		}
	}

	[EditorBrowsable(EditorBrowsableState.Advanced)]
	internal static CultureInfo Culture
	{
		get
		{
			return resourceCulture;
		}
		set
		{
			resourceCulture = value;
		}
	}

	internal static string MsgAccessDenied => ResourceManager.GetString("MsgAccessDenied", resourceCulture);

	internal static string MsgAccessDeniedRightUser => ResourceManager.GetString("MsgAccessDeniedRightUser", resourceCulture);

	internal static string MsgAccessLoss => ResourceManager.GetString("MsgAccessLoss", resourceCulture);

	internal static string MsgAdminRequired => ResourceManager.GetString("MsgAdminRequired", resourceCulture);

	internal static string MsgAlreadyConnected => ResourceManager.GetString("MsgAlreadyConnected", resourceCulture);

	internal static string MsgAppPathInUse => ResourceManager.GetString("MsgAppPathInUse", resourceCulture);

	internal static string MsgBaseFilterType => ResourceManager.GetString("MsgBaseFilterType", resourceCulture);

	internal static string MsgEmptyFilterName => ResourceManager.GetString("MsgEmptyFilterName", resourceCulture);

	internal static string MsgFeatureNotAvailable => ResourceManager.GetString("MsgFeatureNotAvailable", resourceCulture);

	internal static string MsgFilterAlreadyExists => ResourceManager.GetString("MsgFilterAlreadyExists", resourceCulture);

	internal static string MsgFilterInUse => ResourceManager.GetString("MsgFilterInUse", resourceCulture);

	internal static string MsgFilterNotFound => ResourceManager.GetString("MsgFilterNotFound", resourceCulture);

	internal static string MsgFltNameTooLong => ResourceManager.GetString("MsgFltNameTooLong", resourceCulture);

	internal static string MsgFltRevision => ResourceManager.GetString("MsgFltRevision", resourceCulture);

	internal static string MsgInvalidRegNameOrCode => ResourceManager.GetString("MsgInvalidRegNameOrCode", resourceCulture);

	internal static string MsgNtwInFilterUse => ResourceManager.GetString("MsgNtwInFilterUse", resourceCulture);

	internal static string MsgRuleNotFound => ResourceManager.GetString("MsgRuleNotFound", resourceCulture);

	internal static string MsgRuleRevision => ResourceManager.GetString("MsgRuleRevision", resourceCulture);

	internal static string MsgServiceVersionTooLow => ResourceManager.GetString("MsgServiceVersionTooLow", resourceCulture);

	internal static string MsgSubscCancelled => ResourceManager.GetString("MsgSubscCancelled", resourceCulture);

	internal static string MsgSvcClosed => ResourceManager.GetString("MsgSvcClosed", resourceCulture);

	internal static string MsgSvcCnnClosed => ResourceManager.GetString("MsgSvcCnnClosed", resourceCulture);

	internal static string MsgTestingExpired => ResourceManager.GetString("MsgTestingExpired", resourceCulture);

	internal static string MsgTrialExpired => ResourceManager.GetString("MsgTrialExpired", resourceCulture);

	internal static string MsgUserNotFound => ResourceManager.GetString("MsgUserNotFound", resourceCulture);

	internal Langs()
	{
	}
}}
