﻿using System;

namespace NetLimiter.Service.Comm{

internal class ClientContext
{
	public string SessionId { get; protected set; }

	public INLServiceCallback Callback { get; protected set; }

	public string ClientId { get; internal set; }

	public bool IsElevationRequired { get; internal set; } = true;


	public bool IsHandlingFwrs { get; set; }

	public string AppName { get; internal set; }

	public string AppPath { get; internal set; }

	public bool IsRemote { get; internal set; }

	public ClientContext(string sessionId, INLServiceCallback callback)
	{
		SessionId = sessionId ?? throw new ArgumentNullException("sessionId");
		Callback = callback ?? throw new ArgumentNullException("callback");
	}
}}
