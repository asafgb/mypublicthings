﻿using System;
using System.Collections.ObjectModel;
using System.Security.Principal;

namespace NetLimiter.Service.Comm{

internal interface INLCommService : IDisposable
{
	ReadOnlyCollection<ClientContext> Clients { get; }

	event EventHandler<ConnectionEventArgs> Disconnected;

	event EventHandler<ConnectionEventArgs> ClientCreated;

	ClientContext GetCurrentClient();

	WindowsImpersonationContext Impersonate();
}}
