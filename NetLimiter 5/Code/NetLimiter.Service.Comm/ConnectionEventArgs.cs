﻿using System;

namespace NetLimiter.Service.Comm{

internal class ConnectionEventArgs : EventArgs
{
	public ClientContext Client { get; }

	public ConnectionEventArgs(ClientContext client)
	{
		Client = client;
	}
}}
