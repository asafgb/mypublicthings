﻿using System.Collections.Generic;
using System.Linq;
using CoreLib.Net;
using NetLimiter.Service;

namespace NLClientApp.Core{

public class FilterSearcher
{
	private int _valueCnt;

	private string _path;

	private AppId _appId;

	private IPAddress _localAddress;

	private IPAddress _remoteAddress;

	private ushort? _localPort;

	private ushort? _remotePort;

	private ulong? _processId;

	private ushort? _ipProtocol;

	public string Path
	{
		get
		{
			return _path;
		}
		set
		{
			if (_path != null)
			{
				_valueCnt--;
			}
			_path = value;
			if (_path != null)
			{
				_valueCnt++;
			}
		}
	}

	public AppId AppId
	{
		get
		{
			return _appId;
		}
		set
		{
			if (_appId != null)
			{
				_valueCnt--;
			}
			_appId = value;
			if (_appId != null)
			{
				_valueCnt++;
			}
		}
	}

	public IPAddress LocalAddress
	{
		get
		{
			return _localAddress;
		}
		set
		{
			if (_localAddress != null)
			{
				_valueCnt--;
			}
			_localAddress = value;
			if (_localAddress != null)
			{
				_valueCnt++;
			}
		}
	}

	public IPAddress RemoteAddress
	{
		get
		{
			return _remoteAddress;
		}
		set
		{
			if (_remoteAddress != null)
			{
				_valueCnt--;
			}
			_remoteAddress = value;
			if (_remoteAddress != null)
			{
				_valueCnt++;
			}
		}
	}

	public ushort? LocalPort
	{
		get
		{
			return _localPort;
		}
		set
		{
			if (_localPort.HasValue)
			{
				_valueCnt--;
			}
			_localPort = value;
			if (_localPort.HasValue)
			{
				_valueCnt++;
			}
		}
	}

	public ushort? RemotePort
	{
		get
		{
			return _remotePort;
		}
		set
		{
			if (_remotePort.HasValue)
			{
				_valueCnt--;
			}
			_remotePort = value;
			if (_remotePort.HasValue)
			{
				_valueCnt++;
			}
		}
	}

	public ulong? ProcessId
	{
		get
		{
			return _processId;
		}
		set
		{
			if (_processId.HasValue)
			{
				_valueCnt--;
			}
			_processId = value;
			if (_processId.HasValue)
			{
				_valueCnt++;
			}
		}
	}

	public ushort? IpProtocol
	{
		get
		{
			return _ipProtocol;
		}
		set
		{
			if (_ipProtocol.HasValue)
			{
				_valueCnt--;
			}
			_ipProtocol = value;
			if (_ipProtocol.HasValue)
			{
				_valueCnt++;
			}
		}
	}

	public IEnumerable<Filter> Search(IEnumerable<Filter> flts)
	{
		foreach (Filter flt in flts)
		{
			if (flt.Functions.Count == _valueCnt && (_path == null || (from FFPathEqual fn in flt.Functions.Where((FilterFunction fn) => fn is FFPathEqual && fn.IsMatch)
				where fn.Values.Count == 1 && string.Compare(fn.Values[0].Path, _path, ignoreCase: true) == 0
				select fn).Any()) && (_appId == null || (from FFAppIdEqual fn in flt.Functions.Where((FilterFunction fn) => fn is FFAppIdEqual && fn.IsMatch)
				where fn.Values.Count == 1 && fn.Values[0].Equals(_appId)
				select fn).Any()) && (!_localPort.HasValue || (from FFLocalPortInRange fn in flt.Functions.Where((FilterFunction fn) => fn is FFLocalPortInRange && fn.IsMatch)
				where fn.Values.Count == 1 && fn.Values[0].Start == fn.Values[0].End && fn.Values[0].End == _localPort
				select fn).Any()) && (!_remotePort.HasValue || (from FFRemotePortInRange fn in flt.Functions.Where((FilterFunction fn) => fn is FFRemotePortInRange && fn.IsMatch)
				where fn.Values.Count == 1 && fn.Values[0].Start == fn.Values[0].End && fn.Values[0].End == _remotePort
				select fn).Any()) && (!(_localAddress != null) || (from FFLocalAddressInRange fn in flt.Functions.Where((FilterFunction fn) => fn is FFLocalAddressInRange && fn.IsMatch)
				where fn.Values.Count == 1 && fn.Values[0].Range.Start == fn.Values[0].Range.End && fn.Values[0].Range.End == _localAddress
				select fn).Any()) && (!(_remoteAddress != null) || (from FFRemoteAddressInRange fn in flt.Functions.Where((FilterFunction fn) => fn is FFRemoteAddressInRange && fn.IsMatch)
				where fn.Values.Count == 1 && fn.Values[0].Range.Start == fn.Values[0].Range.End && fn.Values[0].Range.End == _remoteAddress
				select fn).Any()) && (!_processId.HasValue || (from FFProcessIdEqual fn in flt.Functions.Where((FilterFunction fn) => fn is FFProcessIdEqual && fn.IsMatch)
				where fn.Values.Count == 1 && fn.Values[0] == _processId
				select fn).Any()) && (!_ipProtocol.HasValue || (from FFProtocolEqual fn in flt.Functions.Where((FilterFunction fn) => fn is FFProtocolEqual && fn.IsMatch)
				where fn.Values.Count == 1 && fn.Values[0] == _ipProtocol
				select fn).Any()))
			{
				yield return flt;
			}
		}
	}

	public Filter Create()
	{
		string text = "";
		Filter filter = new Filter("");
		if (Path != null)
		{
			filter.Functions.Add(new FFPathEqual(Path));
			text += FilterExtensions.CreateFilterNameFromPath(Path);
		}
		if (AppId != null)
		{
			filter.Functions.Add(new FFAppIdEqual(AppId));
			text += "{appid_name}";
		}
		if (ProcessId.HasValue)
		{
			filter.Functions.Add(new FFProcessIdEqual(ProcessId.Value));
			text = text + "Pid " + ProcessId;
		}
		if (LocalAddress != null)
		{
			filter.Functions.Add(new FFLocalAddressInRange(new IPRangeFilterValue(LocalAddress)));
			text = text + "-" + LocalAddress;
		}
		if (LocalPort.HasValue)
		{
			filter.Functions.Add(new FFLocalPortInRange(new PortRangeFilterValue(LocalPort.Value)));
			text = text + ":" + LocalPort;
		}
		if (RemoteAddress != null)
		{
			filter.Functions.Add(new FFRemoteAddressInRange(new IPRangeFilterValue(RemoteAddress)));
			text = text + "-" + RemoteAddress;
		}
		if (RemotePort.HasValue)
		{
			filter.Functions.Add(new FFRemotePortInRange(new PortRangeFilterValue(RemotePort.Value)));
			text = text + ":" + RemotePort;
		}
		if (_ipProtocol.HasValue)
		{
			filter.Functions.Add(new FFProtocolEqual(_ipProtocol.Value));
			text = text + ":" + IpProtocolName.GetName(_ipProtocol.Value).Name;
		}
		filter.Name = text;
		return filter;
	}
}}
