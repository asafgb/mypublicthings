﻿using LicenseKeeper.Models;

namespace LicenseKeeper.Api.Server.License{

public class CheckLicenseRequest
{
	public RegDataVersion Version { get; set; }

	public string RegCode { get; set; }

	public string HWCodeHash { get; set; }
}}
