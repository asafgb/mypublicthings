﻿using System;
using System.Security.Principal;
using Microsoft.Extensions.Logging;

namespace NetLimiter.Service.Security{

public class AccessTableRow
{
	private static readonly ILogger<AccessTableRow> _logger = Logging.LoggerFactory.CreateLogger<AccessTableRow>();

	public SecurityIdentifier Sid { get; protected set; }

	public Rights AllowedRights { get; set; }

	public Rights DeniedRights { get; set; }

	public AccessTableRow(IdentityReference identity)
	{
		SetIdentity(identity);
	}

	public void SetIdentity(IdentityReference identity)
	{
		if ((object)identity == null)
		{
			throw new ArgumentNullException("identity");
		}
		try
		{
			Sid = (SecurityIdentifier)identity.Translate(typeof(SecurityIdentifier));
		}
		catch (Exception ex)
		{
			_logger.LogError(ex, "SetIdentity failed");
			throw new NLException("Failed to resolve the identity " + identity.Value, ex);
		}
	}
}}
