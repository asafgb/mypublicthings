﻿using System;

namespace NetLimiter.Service.Security{

[Serializable]
[Flags]
public enum Rights
{
	Monitor = 1,
	Control = 2,
	RemoteConnect = 4
}}
