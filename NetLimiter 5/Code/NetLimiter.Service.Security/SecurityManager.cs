﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Microsoft.Extensions.Logging;
using NetLimiter.Service.Api;

namespace NetLimiter.Service.Security{

public class SecurityManager
{
	private static readonly ILogger<SecurityManager> _logger = Logging.LoggerFactory.CreateLogger<SecurityManager>();

	private List<AccessTableRow> _table;

	public SecurityManager(List<AccessTableRow> table)
	{
		_table = table;
	}

	public void CheckAccess(Rights right)
	{
		WindowsIdentity current = WindowsIdentity.GetCurrent();
		bool? flag = CheckAccess(current.User, right);
		if (flag == false)
		{
			ThrowDeny(right);
		}
		foreach (IdentityReference group in current.Groups)
		{
			SecurityIdentifier sid = (SecurityIdentifier)group.Translate(typeof(SecurityIdentifier));
			bool? flag2 = CheckAccess(sid, right);
			if (flag2 == false)
			{
				ThrowDeny(right);
			}
			else if (flag2 == true)
			{
				flag = true;
			}
		}
		if (flag != true)
		{
			ThrowDeny(right);
		}
	}

	private bool? CheckAccess(SecurityIdentifier sid, Rights right)
	{
		AccessTableRow accessTableRow = _table.FirstOrDefault((AccessTableRow x) => x.Sid == sid);
		if (accessTableRow != null)
		{
			if (accessTableRow.AllowedRights.HasFlag(right))
			{
				return true;
			}
			if (accessTableRow.DeniedRights.HasFlag(right))
			{
				return false;
			}
		}
		return null;
	}

	public void ThrowDeny(Rights right)
	{
		WindowsIdentity current = WindowsIdentity.GetCurrent();
		throw new AccessDeniedException(right, current.Name)
		{
			RemoteAccess = right.HasFlag(Rights.RemoteConnect)
		};
	}

	internal void SetAccessTable(List<AccessTableRow> table)
	{
		_table = table;
	}

	public static List<NetLimiter.Service.Api.AccessTableRow> CreateDefaultAccessTable()
	{
		List<NetLimiter.Service.Api.AccessTableRow> list = new List<NetLimiter.Service.Api.AccessTableRow>();
		AccessTableAddWellKnownSid(list, WellKnownSidType.BuiltinUsersSid, Rights.Monitor | Rights.Control, (Rights)0);
		AccessTableAddWellKnownSid(list, WellKnownSidType.BuiltinAdministratorsSid, Rights.Monitor | Rights.Control, (Rights)0);
		return list;
	}

	private static void AccessTableAddWellKnownSid(List<NetLimiter.Service.Api.AccessTableRow> accessTable, WellKnownSidType sidType, Rights allow, Rights deny)
	{
		try
		{
			SecurityIdentifier securityIdentifier = new SecurityIdentifier(sidType, null);
			NTAccount nTAccount = (NTAccount)securityIdentifier.Translate(typeof(NTAccount));
			accessTable.Add(new NetLimiter.Service.Api.AccessTableRow(nTAccount.Value, securityIdentifier.ToString())
			{
				AllowedRights = allow,
				DeniedRights = deny
			});
		}
		catch (Exception exception)
		{
			_logger.LogError(exception, "Failed to initialize access table: {sidType}", sidType.ToString());
		}
	}
}}
