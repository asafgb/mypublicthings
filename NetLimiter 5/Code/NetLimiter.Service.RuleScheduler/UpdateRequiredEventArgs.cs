﻿using System;
using System.Collections.Generic;

namespace NetLimiter.Service.RuleScheduler{

public class UpdateRequiredEventArgs : EventArgs
{
	public IEnumerable<RuleSchedulerItem> Items { get; set; }
}}
