﻿using System;

namespace NetLimiter.Service.RuleScheduler{

public class QuotaInterval
{
	public RuleSchedulerItem Item { get; set; }

	public DateTime StartTime { get; set; }

	public DateTime EndTime { get; set; }
}}
