﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using CoreLib;
using Microsoft.Extensions.Logging;

namespace NetLimiter.Service.RuleScheduler{

public class RuleScheduler
{
	private static readonly ILogger<RuleScheduler> _logger = Logging.LoggerFactory.CreateLogger<RuleScheduler>();

	private System.Timers.Timer _timer = new System.Timers.Timer();

	private SynchronizationContext _synCtx;

	private RuleSchedulerItem _nextItem;

	private List<RuleSchedulerItem> _items = new List<RuleSchedulerItem>();

	public RuleSchedulerItem NextItem => _nextItem;

	public List<RuleSchedulerItem> Items => _items;

	public event EventHandler<UpdateRequiredEventArgs> UpdateRequired;

	public RuleScheduler()
	{
		_synCtx = SynchronizationContext.Current;
		_timer.AutoReset = false;
		_timer.Elapsed += _timer_Elapsed;
		MessageLoop.TimeChanged += MessageLoop_TimeChanged;
		MessageLoop.PowerModeChanged += MessageLoop_PowerModeChanged;
	}

	private void _timer_Elapsed(object sender, ElapsedEventArgs e)
	{
		_synCtx.Send(delegate
		{
			OnTimerElapsed();
		}, null);
	}

	private void OnTimerElapsed()
	{
		try
		{
			UpdateItems();
		}
		catch (System.Exception exception)
		{
			_logger.LogError(exception, "Failed to update items from timer");
		}
	}

	private void MessageLoop_PowerModeChanged(object sender, PowerModeEventArgs e)
	{
		if (e.ChangeType == PowerModeChangeType.Resume)
		{
			_logger.LogInformation("PowerModeChanged event: resuming from sleep");
			FireTimerPostponed();
		}
	}

	private void MessageLoop_TimeChanged(object sender, EventArgs e)
	{
		_logger.LogInformation("TimeChanged event");
		FireTimerPostponed();
	}

	private void FireTimerPostponed()
	{
		_timer.Interval = 1000.0;
		_timer.Start();
	}

	public void Initialize(IEnumerable<Rule> rules)
	{
		foreach (Rule rule in rules)
		{
			AddRuleInternal(rule).Flags |= RuleSchedulerUpdateFlags.Loaded;
		}
	}

	public RuleSchedulerItem AddRule(Rule rule)
	{
		RuleSchedulerItem ruleSchedulerItem = _items.FirstOrDefault((RuleSchedulerItem x) => x.Rule.Id == rule.Id);
		if (ruleSchedulerItem != null)
		{
			throw new System.Exception("Can't add rule to scheduler: the rule is already added.");
		}
		ruleSchedulerItem = AddRuleInternal(rule);
		ruleSchedulerItem.Flags |= RuleSchedulerUpdateFlags.Added;
		OnUpdateRequired(new RuleSchedulerItem[1] { ruleSchedulerItem });
		return ruleSchedulerItem;
	}

	private RuleSchedulerItem AddRuleInternal(Rule rule)
	{
		RuleSchedulerItem ruleSchedulerItem = new RuleSchedulerItem(rule);
		ruleSchedulerItem.RuleScheduler = this;
		_items.Add(ruleSchedulerItem);
		ruleSchedulerItem.Update();
		OnItemUpdated(ruleSchedulerItem);
		return ruleSchedulerItem;
	}

	public RuleSchedulerItem UpdateRule(Rule rule)
	{
		return UpdateRule(rule, reset: false);
	}

	public RuleSchedulerItem UpdateRule(Rule rule, bool reset)
	{
		RuleSchedulerItem ruleSchedulerItem = _items.FirstOrDefault((RuleSchedulerItem x) => x.Rule.Id == rule.Id);
		if (ruleSchedulerItem == null)
		{
			throw new ArgumentException("RuleScheduler can't update the rule. Rule not found.");
		}
		ruleSchedulerItem.Update(rule);
		if (reset)
		{
			ruleSchedulerItem.Flags |= RuleSchedulerUpdateFlags.Reset;
		}
		OnItemUpdated(ruleSchedulerItem);
		OnUpdateRequired(new RuleSchedulerItem[1] { ruleSchedulerItem });
		return ruleSchedulerItem;
	}

	public RuleSchedulerItem RemoveRule(Rule rule)
	{
		RuleSchedulerItem ruleSchedulerItem = null;
		ruleSchedulerItem = _items.FirstOrDefault((RuleSchedulerItem x) => x.Rule == rule);
		if (ruleSchedulerItem != null)
		{
			RemoveItem(ruleSchedulerItem);
		}
		return ruleSchedulerItem;
	}

	private void RemoveItem(RuleSchedulerItem item)
	{
		if (item == null)
		{
			throw new ArgumentNullException("item");
		}
		_items.Remove(item);
		item.OnRemoved();
		if (item == _nextItem)
		{
			_nextItem = null;
			_timer.Interval = 1.0;
		}
		OnUpdateRequired(new RuleSchedulerItem[1] { item });
	}

	private void OnItemUpdated(RuleSchedulerItem item)
	{
		if (item.Next != null && (_nextItem == null || _nextItem == item || _nextItem.Next.NextTime > item.Next.NextTime))
		{
			_nextItem = item;
			InitInternval();
		}
	}

	private void InitInternval()
	{
		if (_timer == null)
		{
			return;
		}
		if (_nextItem != null && _nextItem.Next.NextTime < DateTime.MaxValue)
		{
			double num = (_nextItem.Next.NextTime - DateTime.UtcNow).TotalMilliseconds;
			if (num > 2147483647.0)
			{
				num = 2147483647.0;
			}
			_timer.Interval = ((num <= 0.0) ? 1.0 : num);
			_timer.Start();
		}
		else
		{
			_timer.Stop();
		}
	}

	protected void UpdateItems()
	{
		_logger.LogInformation("Updating items");
		_nextItem = null;
		List<RuleSchedulerItem> list = new List<RuleSchedulerItem>();
		foreach (RuleSchedulerItem item in _items)
		{
			item.Update();
			OnItemUpdated(item);
			if (item.Flags.HasFlag(RuleSchedulerUpdateFlags.Removed))
			{
				list.Add(item);
			}
		}
		foreach (RuleSchedulerItem item2 in list)
		{
			RemoveItem(item2);
		}
		OnUpdateRequired(Items);
	}

	public void OnUpdateRequired()
	{
		OnUpdateRequired(Items);
	}

	public void OnUpdateRequired(IEnumerable<RuleSchedulerItem> items)
	{
		this.UpdateRequired?.Invoke(this, new UpdateRequiredEventArgs
		{
			Items = items
		});
	}

	public void Stop()
	{
		MessageLoop.TimeChanged -= MessageLoop_TimeChanged;
		MessageLoop.PowerModeChanged -= MessageLoop_PowerModeChanged;
		if (_timer != null)
		{
			_timer.Stop();
		}
		_items.Clear();
	}
}}
