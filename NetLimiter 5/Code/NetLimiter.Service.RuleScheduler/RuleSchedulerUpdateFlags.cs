﻿using System;

namespace NetLimiter.Service.RuleScheduler{

[Flags]
public enum RuleSchedulerUpdateFlags
{
	None = 0,
	Loaded = 1,
	Added = 2,
	Updated = 4,
	Removed = 8,
	ActionChanged = 0x10,
	SaveRequired = 0x40,
	Reset = 0x80
}}
