﻿using System;

namespace NetLimiter.Service.RuleScheduler{

public class ConditionItem
{
	private DateTime _prevTime;

	private DateTime _nextTime;

	public TimeCondition Condition { get; protected set; }

	public DateTime PrevTime => _prevTime;

	public DateTime NextTime => _nextTime;

	public ConditionItem(TimeCondition condition, DateTime tmNow)
	{
		if (condition == null)
		{
			throw new ArgumentNullException("condition");
		}
		Condition = condition;
		condition.GetExecutionTime(out _prevTime, out _nextTime, tmNow);
	}
}}
