﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NetLimiter.Service.RuleScheduler{

public class RuleSchedulerItem
{
	private Rule _rule;

	public RuleScheduler RuleScheduler { get; internal set; }

	public Rule Rule
	{
		get
		{
			return _rule;
		}
		protected set
		{
			if (value == null)
			{
				throw new ArgumentNullException("Rule", "Rule can't be null");
			}
			_rule = value;
		}
	}

	public ConditionItem Prev { get; protected set; }

	public ConditionItem PrevReset { get; protected set; }

	public ConditionItem PrevStartStop { get; protected set; }

	public ConditionItem Next { get; protected set; }

	public ConditionItem NextStartStop { get; protected set; }

	public bool? WasStarted { get; protected set; }

	public bool IsStarted { get; protected set; }

	public RuleSchedulerUpdateFlags Flags { get; set; }

	public RuleSchedulerItem(Rule rule)
	{
		Rule = rule;
	}

	public void Update(Rule rule = null, DateTime? tmNow = null, bool includeLastReset = false)
	{
		Flags = RuleSchedulerUpdateFlags.None;
		if (!tmNow.HasValue)
		{
			tmNow = DateTime.UtcNow;
		}
		if (rule != null)
		{
			_rule = rule;
			Flags |= RuleSchedulerUpdateFlags.Updated;
		}
		WasStarted = IsStarted;
		ConditionItem prev = (Next = null);
		Prev = prev;
		prev = (NextStartStop = null);
		PrevStartStop = prev;
		foreach (TimeCondition item in Rule.Conditions.OfType<TimeCondition>())
		{
			ConditionItem conditionItem3 = new ConditionItem(item, tmNow.Value);
			bool flag = conditionItem3.Condition.Action == RuleConditionAction.Start || conditionItem3.Condition.Action == RuleConditionAction.Stop;
			if (conditionItem3.PrevTime > DateTime.MinValue)
			{
				if (Prev == null || Prev.PrevTime < conditionItem3.PrevTime)
				{
					Prev = conditionItem3;
					if (flag && (PrevStartStop == null || PrevStartStop.PrevTime < conditionItem3.PrevTime))
					{
						PrevStartStop = conditionItem3;
					}
				}
				if (conditionItem3.Condition.Action == RuleConditionAction.Delete)
				{
					OnRemoved();
				}
				else if (conditionItem3.Condition.Action == RuleConditionAction.Reset && (includeLastReset || conditionItem3.Condition.LastExecuted < conditionItem3.PrevTime))
				{
					conditionItem3.Condition.LastExecuted = tmNow.Value;
					Flags |= RuleSchedulerUpdateFlags.SaveRequired;
					Flags |= RuleSchedulerUpdateFlags.Reset;
					PrevReset = conditionItem3;
				}
			}
			if (conditionItem3.NextTime < DateTime.MaxValue)
			{
				if (Next == null || Next.NextTime > conditionItem3.NextTime)
				{
					Next = conditionItem3;
				}
				if (flag && (NextStartStop == null || NextStartStop.NextTime > conditionItem3.NextTime))
				{
					NextStartStop = conditionItem3;
				}
			}
		}
		if (PrevStartStop == null)
		{
			if (NextStartStop != null)
			{
				IsStarted = NextStartStop.Condition.Action != RuleConditionAction.Start;
			}
			else
			{
				IsStarted = true;
			}
		}
		else
		{
			IsStarted = PrevStartStop.Condition.Action == RuleConditionAction.Start;
		}
		_rule.IsActive = IsStarted && _rule.IsEnabled;
		if (WasStarted != IsStarted)
		{
			Flags |= RuleSchedulerUpdateFlags.ActionChanged;
		}
	}

	internal void OnRemoved()
	{
		Flags |= RuleSchedulerUpdateFlags.Removed;
		_rule.IsActive = false;
	}

	public static IEnumerable<QuotaInterval> GetQuotaIntervals(Rule rule, DateTime? tmNow = null)
	{
		if (rule == null)
		{
			throw new ArgumentNullException("rule");
		}
		if (!tmNow.HasValue)
		{
			tmNow = DateTime.UtcNow;
		}
		DateTime value = tmNow.Value;
		RuleSchedulerItem item = new RuleSchedulerItem(rule);
		item.Update(null, value, includeLastReset: true);
		value = ((item.PrevReset == null) ? DateTime.MinValue : item.PrevReset.PrevTime);
		while (true)
		{
			item.Update(null, value, includeLastReset: true);
			DateTime tm2 = ((item.Next != null) ? item.Next.NextTime : tmNow.Value);
			if (tm2 > tmNow.Value)
			{
				tm2 = tmNow.Value;
			}
			if (item.IsStarted)
			{
				yield return new QuotaInterval
				{
					StartTime = value,
					EndTime = tm2,
					Item = item
				};
			}
			if (!(tm2 == tmNow.Value))
			{
				value = tm2;
				continue;
			}
			break;
		}
	}
}}
